<?php

class page_model extends CI_Model
{


		public function __construct()
        {
			parent::__construct();
			// Your own constructor code
        }
		
		
		
        public function login($data) {

			$condition = "user_login.user_name =" . "'" . $data['username'] . "' AND " . "user_login.user_password =" . "'" . $data['password'] . "'";
			$this->db->select('user_login.*');
			$this->db->from('user_login');
			$this->db->where($condition);
			$this->db->limit(1);
			$query = $this->db->get();

			if ($query->num_rows() == 1) {
				return true;
			} else {
				return false;
			}
		}
		public function checkuser($user) {

			$condition = "user_name =" . "'" . $user . "'";
			$this->db->select('*');
			$this->db->from('user_login');
			$this->db->where($condition);
			$this->db->limit(1);
			$query = $this->db->get();

			if ($query->num_rows() == 1) {
				return true;
			} else {
				return false;
			}
		}

        public function get_countries()
        {
            $this->db->select('*');
            $result = $this->db->get('lenguajes');
            return $result->result();
        }

			// Read data from database to show data in admin page
		public function read_user_information($username) {
			$condition = "user_name =" . "'" . $username . "'";
			$this->db->select('user_login.*');
			$this->db->from('user_login');
			$this->db->where($condition);
			$this->db->limit(1);
			$query = $this->db->get();

			if ($query->num_rows() == 1) {
				return $query->result();
			} else {
				return false;
			}
		}
			
        
		public function get_usuarios()
        {
				$this->db->select('user_login.*');
                $query = $this->db->get('user_login');
                return $query->result();
        }

        public function get_newsletter()
        {
                $this->db->select('*');
                $query = $this->db->get('newsletter');
                return $query->result();
        }

        public function get_contactos()
        {
                $this->db->select('*');
                $query = $this->db->get('contactos');
                return $query->result();
        }
        public function get_registro_producto()
        {
                $this->db->select('*');
                $query = $this->db->get('registro_producto');
                return $query->result();
        }
		public function get_usuarios_id($id)
        {
				$this->db->where('id', $id);
                $query = $this->db->get('user_login');
                return $query->result();
        }

        public function get_centros()
        {
            $query = $this->db->get('centros');
            return $query->result();
        }

        public function get_localidades()
        {
            $query = $this->db->get('localidades');
            return $query->result();
        }

        public function get_catalogos()
        {   
            $this->db->query("SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));");
            $this->db->group_by('uniq');
            $query = $this->db->get('catalogos');
            return $query->result();
        }

        public function get_eventos()
        {   
            $this->db->query("SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));");
            $this->db->group_by('uniq');
            $query = $this->db->get('events');
            return $query->result();
        }

        public function get_sliders()
        {   
            $this->db->query("SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));");
            $this->db->group_by('uniq');
            $query = $this->db->get('slider');
            return $query->result();
        }


        public function get_paises()
        {
            $query = $this->db->get('countries');
            return $query->result();
        }

        public function insert_centro($data)
        {
            $this->db->insert('centros', $data);
        }

        public function get_localidad_pais($id)
        {
            $this->db->where('paisId',$id);
            $query = $this->db->get('localidades');
            return $query->result();
        }
        public function get_categorias()
        {
            $this->db->query("SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));");
            $this->db->group_by('categorias.uniq');
        	$this->db->select('categorias.*');
        	$this->db->select('catalogos.nombre as catalogo_nombre');
        	$this->db->join('catalogos', 'catalogos.uniq = categorias.id_catalogo');
            $query = $this->db->get('categorias');	
            return $query->result();
        }


        public function get_productos_admin()
        {   
            $this->db->query("SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));");
            $this->db->group_by('productos.uniq');
            $this->db->select('productos.*');
            $query = $this->db->get('productos');  
            return $query->result();
        }

        public function get_productos()
        {   
            $this->db->query("SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));");
            $this->db->group_by('productos.uniq');
            $this->db->select('productos.*');
            $this->db->select('categorias.slug as slug_categoria');
            $this->db->select('subcategorias.slug as slug_subcategoria');
            $this->db->select('catalogos.slug as slug_catalogo');
            $this->db->join('categorias', 'categorias.uniq = productos.id_categoria');
            $this->db->join('subcategorias', 'subcategorias.uniq = productos.id_subcategoria');
            $this->db->join('catalogos', 'catalogos.uniq = categorias.id_catalogo');
            $query = $this->db->get('productos');  
            return $query->result();
        }

        public function get_subcategorias($id_categoria)
        {
            $this->db->query("SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));");
            $this->db->group_by('subcategorias.uniq');
        	$this->db->select('subcategorias.*');
        	$this->db->select('categorias.nombre as categoria');
        	$this->db->join('categorias', 'categorias.id = subcategorias.id_categoria');
        	$this->db->where('id_categoria', $id_categoria);
            $query = $this->db->get('subcategorias');	
            return $query->result();
        }


        public function get_catalogo_id($id)
        {
        	$this->db->where('uniq', $id);
        	$query = $this->db->get('catalogos');
        	return $query->result();
        }

        public function get_evento_id($id)
        {
            $this->db->where('uniq', $id);
            $query = $this->db->get('events');
            return $query->result();
        }

        public function get_slider_id($id)
        {
            $this->db->where('uniq', $id);
            $query = $this->db->get('slider');
            return $query->result();
        }


        public function get_centro_id($id)
        {
            $this->db->where('id', $id);
            $query = $this->db->get('centros');
            return $query->result();
        }

        public function get_pais_id($id)
        {
        	$this->db->where('id', $id);
        	$query = $this->db->get('countries');
        	return $query->result();
        }

        public function get_categoria_id($id)
        {
        	$this->db->where('uniq', $id);
        	$query = $this->db->get('categorias');
        	return $query->result();
        }


        public function get_producto_id($id)
        {
            $this->db->where('uniq', $id);
            $query = $this->db->get('productos');
            return $query->result();
        }

        public function get_subcategoria_id($id)
        {   
        	$this->db->where('uniq', $id);
        	$query = $this->db->get('subcategorias');
        	return $query->result();
        }
		

		/*INSERTS*/
		
		
		public function insert_usuario($data)
        {
            $this->db->insert('user_login', $data);
        }

        public function insert_catalogo($data)
        {
            $this->db->insert('catalogos', $data);
            return $this->db->insert_id();
        }

        public function insert_evento($data)
        {
            $this->db->insert('events', $data);
        }

        public function insert_slider($data)
        {
            $this->db->insert('slider', $data);
        }

        public function insert_pais($data)
        {
            $this->db->insert('countries', $data);
        }

        public function insert_categoria($data)
        {
            $this->db->insert('categorias', $data);

        }

        public function insert_producto($data)
        {
            $this->db->insert('productos', $data);
        }

        public function insert_subcategoria($data)
        {
            $this->db->insert('subcategorias', $data);
        }
        
		
		/*UPDATES*/

		public function update_catalogo($data)
        {
			$this->db->where('id', $this->uri->segment(3));
            $this->db->update('catalogos', $data);
        }

        public function update_evento($data)
        {
            $this->db->where('id', $this->uri->segment(3));
            $this->db->update('events', $data);
        }

        public function update_centro($data)
        {
            $this->db->where('id', $this->uri->segment(3));
            $this->db->update('centros', $data);
        }

        public function update_slider($data)
        {
            $this->db->where('id', $this->uri->segment(3));
            $this->db->update('slider', $data);
        }

        public function update_pais($data)
        {
			$this->db->where('id', $this->uri->segment(3));
            $this->db->update('countries', $data);
        }

        public function update_categoria($data)
        {
			$this->db->where('id', $this->uri->segment(3));
            $this->db->update('categorias', $data);
        }

        public function update_producto($data)
        {
            $this->db->where('id', $this->uri->segment(3));
            $this->db->update('productos', $data);
        }

        public function update_subcategoria($data)
        {
			$this->db->where('id', $this->uri->segment(3));
            $this->db->update('subcategorias', $data);
        }

		public function update_usuario($data)
        {
				$this->db->set('name', $data['name']);
				$this->db->set('lastname', $data['lastname']);
				$this->db->set('user_email', $data['user_email']);
				if($data['user_password']<>''){
					$this->db->set('user_password', $data['user_password']);
				}
				$this->db->where('id', $this->uri->segment(3));
                $this->db->update('user_login');
        }
		
		
		/*REMOVES*/
			
		public function remove_catalogo()
        {
				$this->db->where('uniq', $this->uri->segment(3));
                $this->db->delete('catalogos');
        }

        public function remove_evento()
        {
                $this->db->where('uniq', $this->uri->segment(3));
                $this->db->delete('events');
        }

        public function remove_centro()
        {
            $this->db->where('id', $this->uri->segment(3));
            $this->db->delete('centros');
        }

        public function remove_slider()
        {
                $this->db->where('uniq', $this->uri->segment(3));
                $this->db->delete('slider');
        }

        public function remove_pais()
        {
				$this->db->where('id', $this->uri->segment(3));
                $this->db->delete('countries');
        }

        public function remove_categoria()
        {
				$this->db->where('uniq', $this->uri->segment(3));
                $this->db->delete('categorias');
        }

        public function remove_subcategoria()
        {
                $this->db->where('uniq', $this->uri->segment(3));
                $this->db->delete('subcategorias');
        }


        public function remove_producto()
        {
                $this->db->where('uniq', $this->uri->segment(3));
                $this->db->delete('productos');
        }
        
		public function remove_usuario()
        {
				$this->db->where('id', $this->uri->segment(3));
                $this->db->delete('user_login');
        }
        
}

?>