<?php
if ($this->session->userdata['logged_in']['administrator']==0) {
	header("location: ".base_url());
}
?>
<div class="home-main col-sm-10" id="home_main">
	<div class="home-content" style="margin-top:0px; padding-top:20px;">
		<div class="navbar-inner">
			<ul class="nav nav-tabs">
			  <li role="presentation" class="active"><a href="#tab1" data-toggle="tab">Datos</a></li>
			  <!--<li role="presentation"><a href="#tab2" data-toggle="tab">Subtabla relacionada</a></li>-->
			</ul>
		</div>
		<div class="tab-content" id="adm_form">
		  <div class="tab-pane active" id="tab1">
				
			 <form method="post" action="<?php echo base_url()?>catalogos/update/<?php echo $info[0]->{'uniq'} ?>/">
			 	<!-- Nav tabs -->
			 	<ul class="nav nav-tabs">
			 		<?php foreach($lang as $keylang => $lan): ?>
			 			<li class="nav-item <?php if($keylang == 0): ?> active <?php endif; ?>">
			 			  <a class="nav-link <?php if($keylang == 0): ?> active <?php endif; ?>" data-toggle="tab" href="#sec<?php echo $lan->{'id'} ?>">Titulo <?php echo $lan->{'titulo'} ?></a>
			 			</li>
			 		<?php endforeach; ?>
			 	</ul>
			 	<div class="tab-content">
			 		<?php foreach($lang as $keylang => $lan): ?>
			 			<div class="tab-pane lang-buttons <?php if($keylang == 0): ?> active <?php endif; ?>" id="sec<?php echo $lan->{'id'} ?>">
			 				<div class="row">
			 					<div class="col-xs-12 col-md-6">
					 				<div class="td-input">
										<b>Nombre:</b><br>
										<input type="text" name="nombre_<?php echo $lan->{'abr'} ?>" id="nombre" value="<?php if(isset($info[$keylang]->{'nombre'})) echo $info[$keylang]->{'nombre'} ?>">
									</div>
								</div>
								<div class="col-xs-12 col-md-6">
									<div class="td-input">
										<b>Descripcion:</b><br>
										<input type="text" name="descripcion_<?php echo $lan->{'abr'} ?>" id="descripcion" value="<?php if(isset($info[$keylang]->{'descripcion'})) echo $info[$keylang]->{'descripcion'} ?>">
									</div>
								</div>
							</div>
				 			<div class="td-input">
		    					<input type="hidden" name="id_idioma_<?php echo $lan->{'abr'} ?>" value="<?php echo $lan->{'id'} ?>">
		    				</div>
			 			</div>
			 		<?php endforeach; ?>
		 		</div>
				<div class="td-input">
					<b>Imagen de inicio o portada:</b><br>
					<input type="text" name="galeria1_input" id="galeria1_input" class="img-input" value="<?php echo $info[0]->imagen_portada ?>" readonly>
					<div id="main_uploader">
						<div class="uploader-id1">
							<div id="uploader1" align="left">
								<input id="uploadify1" type="file" class="uploader" />
							</div>
						</div>
						<div id="filesUploaded" style="display:none;"></div>
						<div id="thumbHeight1" style="display:none;" >800</div>
						<div id="thumbWidth1" style="display:none;" >1131</div>
					</div>
					<div id="galeria1" class="upload-galeria">
						<?php if($info[0]->{'imagen_portada'}<>''){ ?>
						<div class="list-img-gal"><div class="file-del" onclick="delFile('../../../asset/img/uploads/<?php echo $info[0]->{'imagen_portada'}?>',function(){}); $('#galeria1_input').val(''); $(this).parent().remove();"></div><img src="../../../../asset/img/uploads/<?php echo $info[0]->{'imagen_portada'}?>" width="auto" height="100"><br><input id="img_desc" type="text"></div>
						<?php } ?>
					</div>
				</div>
				<div class="td-input">
					<b>Imagen home:</b><br>
					<input type="text" name="galeria2_input" id="galeria2_input" class="img-input" value="<?php echo $info[0]->imagen_home ?>" readonly>
					<div id="main_uploader">
						<div class="uploader-id2">
							<div id="uploader2" align="left">
								<input id="uploadify2" type="file" class="uploader" />
							</div>
						</div>
						<div id="filesUploaded" style="display:none;"></div>
						<div id="thumbHeight2" style="display:none;" >800</div>
						<div id="thumbWidth2" style="display:none;" >1131</div>
					</div>
					<div id="galeria2" class="upload-galeria">
						<?php if($info[0]->{'imagen_home'}<>''){ ?>
						<div class="list-img-gal"><div class="file-del" onclick="delFile('../../../asset/img/uploads/<?php echo $info[0]->{'imagen_home'}?>',function(){}); $('#galeria2_input').val(''); $(this).parent().remove();"></div><img src="../../../../asset/img/uploads/<?php echo $info[0]->{'imagen_home'}?>" width="auto" height="100"><br><input id="img_desc" type="text"></div>
						<?php } ?>
					</div>
				</div>
			 </form>
		  </div>
		  <div class="tab-pane" id="tab2">
			 iframe listado subtabla
		  </div>
	   </div>
	   <div class="btn btn-success btn-sm pull-right bt-save" style="margin-right:8px;">GUARDAR</div>
	   <a href="<?php echo base_url()?>catalogos/"><div class="btn btn-default btn-sm pull-right" style="margin-right:8px;">CANCELAR</div></a>
	</div>
</div>
<br style="clear:both;"/>