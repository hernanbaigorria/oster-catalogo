<?php
if ($this->session->userdata['logged_in']['administrator']==0) {
	header("location: ".base_url());
}
?>
<div class="home-main col-sm-10" id="home_main">
	<div class="home-content" style="margin-top:0px; padding-top:10px;">
		<div class="listado">
			<div class="col-md-12 home-tools">
				<div class="row">
					<div class="col-xs-8 col-md-8">
						<h2>Productos</h2>
					</div>
					<div class="col-xs-4 col-md-4">
						<a href="<?php echo base_url()?>productos/add/"><div class="btn btn-success btn-sm bt-save pull-right" style="margin-right:8px;">AGREGAR NUEVO</div></a>
					</div>
				</div>
			</div>
			<table id="list" class="table table-striped table-bordered dataTable" width="100%" cellspacing="0">
				<thead>
					<tr>
						<th>Titulo</th>
						<th width="40">Link</th>
						<th width="40">Nuevo</th>
						<th width="40">Publicar</th>
						<th width="40">Editar</th>
						<th width="40">Eliminar</th>
					</tr>
				</thead>
				<tbody>
					<?php
						$html='';
						foreach ( $info as $fila ){
							$html.='<tr>
								<td>'.$fila->{'titulo'}.'</td>
								<td align="center"><a href="'.base_url()."../catalogs/".$fila->{'slug_catalogo'}."/".$fila->{'slug_categoria'}."/".$fila->{'slug_subcategoria'}."/".$fila->{'slug'}."/".'" target="_blank"><span class="glyphicon glyphicon-link" aria-hidden="true"></span></a></td>';
								if($fila->{'nuevo'} == 0):
									$html.='<td align="center"><a href="'.base_url().'productos/estado/1/?prd='.$fila->uniq.'"><span class="glyphicon glyphicon-star-empty"></span></a></td>';
								else:
									$html.='<td align="center"><a href="'.base_url().'productos/estado/0/?prd='.$fila->uniq.'" style="color:#007BD9"><span class="glyphicon glyphicon-star"></span></a></td>';
								endif;

								if($fila->{'publicar'} == 0):
									$html.='<td align="center"><a href="'.base_url().'productos/publicar/1/?prd='.$fila->uniq.'"><span class="glyphicon glyphicon-eye-close"></span></a></td>';
								else:
									$html.='<td align="center"><a href="'.base_url().'productos/publicar/0/?prd='.$fila->uniq.'" style="color:#007BD9"><span class="glyphicon glyphicon-eye-open"></span></a></td>';
								endif;

								$html.='<td align="center"><a href="'.base_url().'productos/edit/'.$fila->{'uniq'}.'/"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>
								<td align="center"><a href="#" data-href="'.base_url().'productos/remove/'.$fila->{'uniq'}.'/" data-toggle="modal" data-target="#confirm-delete"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a></td>
							</tr>';
							
						}
						echo $html;
					?>				
				</tbody>
			</table>
		</div>
	</div>
</div>
<br style="clear:both;"/>