<?php
if ($this->session->userdata['logged_in']['administrator']==0) {
	header("location: ".base_url());
}
?>
<div class="home-main col-sm-10" id="home_main">
	<div class="home-content" style="margin-top:0px; padding-top:20px;">
		<div class="navbar-inner">
			<ul class="nav nav-tabs">
			  <li role="presentation" class="active"><a href="#tab1" data-toggle="tab">Datos</a></li>
			  <!--<li role="presentation"><a href="#tab2" data-toggle="tab">Subtabla relacionada</a></li>-->
			</ul>
		</div>
		<div class="tab-content" id="adm_form">
		  <div class="tab-pane active" id="tab1">
				
			 <form method="post" action="<?php echo base_url()?>productos/save/">
			 	<!-- Nav tabs -->
			 	<ul class="nav nav-tabs">
			 		<?php foreach($lang as $keylang => $lan): ?>
			 			<li class="nav-item <?php if($keylang == 0): ?> active <?php endif; ?>">
			 			  <a class="nav-link <?php if($keylang == 0): ?> active <?php endif; ?>" data-toggle="tab" href="#sec<?php echo $lan->{'id'} ?>">Titulo <?php echo $lan->{'titulo'} ?></a>
			 			</li>
			 		<?php endforeach; ?>
			 	</ul>
			 	<!-- Tab panes -->
			 	<div class="tab-content">
			 		<?php foreach($lang as $keylang => $lan): ?>
			 			<div class="tab-pane lang-buttons <?php if($keylang == 0): ?> active <?php endif; ?>" id="sec<?php echo $lan->{'id'} ?>">
			 				<div class="row">
			 					<div class="col-xs-12 col-md-6">
			 						<div class="td-input">
			 							<b>Titulo:</b><br>
			 							<input type="text" name="titulo_<?php echo $lan->{'abr'} ?>" id="titulo" value="">
			 						</div>
			 					</div>

			 					<div class="col-xs-12 col-md-6">
			 						<div class="td-input">
			 							<b>Subtitulo:</b><br>
			 							<input type="text" name="subtitulo_<?php echo $lan->{'abr'} ?>" id="subtitulo" value="">
			 						</div>
			 					</div>
			 				</div>
			 				<div class="row">
			 					<div class="col-xs-12 col-md-6">
			 						<div class="td-input">
			 							<b>Breve descripcion:</b><br>
			 							<textarea name="breve_descripcion_<?php echo $lan->{'abr'} ?>"></textarea>
			 						</div>
			 					</div>
			 					<div class="col-xs-12 col-md-6">
			 						<div class="td-input">
			 							<b>Descripcion:</b><br>
			 							<textarea name="descripcion_<?php echo $lan->{'abr'} ?>"></textarea>
			 						</div>
			 					</div>
			 					<?php if(false): ?>
				 					<div class="col-xs-12 col-md-6">
	 						 			<div class="td-input">
	 										<b>Brochure:</b><br>
	 										<input type="text" name="galeria1<?=$keylang?>_input" id="galeria1<?=$keylang?>_input" class="img-input" value="" readonly>
	 										<div id="main_uploader">
	 											<div class="uploader-id1<?=$keylang?>">
	 												<div id="uploader1<?=$keylang?>" align="left">
	 													<input id="uploadify1<?=$keylang?>" type="file" class="uploader" />
	 												</div>
	 											</div>
	 											<div id="filesUploaded" style="display:none;"></div>
	 											<div id="thumbHeight1<?=$keylang?>" style="display:none;" >800</div>
	 											<div id="thumbWidth1<?=$keylang?>" style="display:none;" >1131</div>
	 										</div>
	 										<div id="galeria1<?=$keylang?>" class="upload-galeria">
	 										</div>
	 									</div>
	 									<div class="td-input">
	 										<b>Sell Sheet:</b><br>
	 										<input type="text" name="galeria2<?=$keylang?>_input" id="galeria2<?=$keylang?>_input" class="img-input" value="" readonly>
	 										<div id="main_uploader">
	 											<div class="uploader-id2<?=$keylang?>">
	 												<div id="uploader2<?=$keylang?>" align="left">
	 													<input id="uploadify2<?=$keylang?>" type="file" class="uploader" />
	 												</div>
	 											</div>
	 											<div id="filesUploaded" style="display:none;"></div>
	 											<div id="thumbHeight2<?=$keylang?>" style="display:none;" >800</div>
	 											<div id="thumbWidth2<?=$keylang?>" style="display:none;" >1131</div>
	 										</div>
	 										<div id="galeria2<?=$keylang?>" class="upload-galeria">
	 										</div>
	 									</div>
				 					</div>
			 					<?php endif; ?>
			 				</div>
				 			<div class="td-input">
		    					<input type="hidden" name="id_idioma_<?php echo $lan->{'abr'} ?>" value="<?php echo $lan->{'id'} ?>">
		    				</div>
			 			</div>
			 			
			 		<?php endforeach; ?>
			 	</div>
			 	<div class="row">
			 		<div class="col-xs-12 col-md-6">
						<div class="td-input">
							<b>Categoria:</b><br>
							<select id="categoriasSelect" name="id_categoria">
		 					<option value="">Seleccionar...</option>
		 					<?php foreach($categorias as $categoria): ?>
		 						<option value="<?=$categoria->uniq?>"><?=$categoria->nombre?></option>
		 					<?php endforeach; ?>
		 				</select>
		 				<div id="subcategorias" style="margin-top:10px;"></div>
						</div>
					</div>
			 		<div class="col-xs-12 col-md-12">
						<div class="td-input">
							<b>Imagen de producto:</b><br>
							<input type="text" name="galeria9_input" id="galeria9_input" class="img-input" value="" readonly>
							<div id="main_uploader">
								<div class="uploader-id9">
									<div id="uploader9" align="left">
										<input id="uploadify9" type="file" class="uploader" />
									</div>
								</div>
								<div id="filesUploaded" style="display:none;"></div>
								<div id="thumbHeight9" style="display:none;" >800</div>
								<div id="thumbWidth9" style="display:none;" >1131</div>
							</div>
							<div id="galeria9" class="upload-galeria">
							</div>
						</div>
					</div>
			 	</div>
			 </form>
		  </div>
		  <div class="tab-pane" id="tab2">
			 iframe listado subtabla
		  </div>
	   </div>
	   <div class="btn btn-success btn-sm pull-right bt-save" style="margin-right:8px;">GUARDAR</div>
	   <a href="<?php echo base_url()?>productos/"><div class="btn btn-default btn-sm pull-right" style="margin-right:8px;">CANCELAR</div></a>
	</div>
</div>
<br style="clear:both;"/>

<script type="text/javascript" src="<?php echo base_url() ?>asset/js/jquery-1.11.1.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		/*UPLOAD CONFIG*/
		var w=1440;
		var h=810;
		var path='/../../../asset/img/uploads/';
		
		
		var maxWidth=1440;
		var thWidth=1440;
		var thHeight=810;		
		var tipo='unica';
		var allowedTypes='jpg,png,gif,pdf'
		var callback=function(){console.log('upload complete');}

		<?php foreach($lang as $keylang => $lan): ?>
			uploaderNoCrop(<?='1'.$keylang?>,path,maxWidth,thHeight,thWidth,tipo,5,allowedTypes,true,callback);
		<?php endforeach; ?>
		<?php foreach($lang as $keylang => $lan): ?>
			uploaderNoCrop(<?='2'.$keylang?>,path,maxWidth,thHeight,thWidth,tipo,5,allowedTypes,true,callback);
		<?php endforeach; ?>
	});
</script>