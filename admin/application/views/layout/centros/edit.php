<style type="text/css">
	#map_canvas{height:400px; width:100%; border:1px solid #ccc;}
</style>
<div class="home-main col-sm-10" id="home_main">
	<div class="home-content" style="margin-top:0px; padding-top:20px;">
		<div class="navbar-inner">
			<ul class="nav nav-tabs">
			  <li role="presentation" class="active"><a href="#tab1" data-toggle="tab">Datos</a></li>
			  <!--<li role="presentation"><a href="#tab2" data-toggle="tab">Subtabla relacionada</a></li>-->
			</ul>
		</div>
		<div class="tab-content" id="adm_form">
		  <div class="tab-pane active" id="tab1">
			 <form method="post" action="<?php echo base_url()?>centros/update/<?php echo $info[0]->{'id'}?>/">
				<div class="td-input">
					<b>País:</b><br>
					<select name="pais" id="pais">
						<?php
							$html='';
							foreach ( $paises as $country ){
								if($info[0]->{'pais'}==$country->{'id'}){
									$html.='<option value="'.$country->{'id'}.'" selected>'.$country->{'country'}.'</option>';
								}else{
									$html.='<option value="'.$country->{'id'}.'">'.$country->{'country'}.'</option>';
								}
							}
							echo $html;
						?>
					</select>
				</div>
				<div class="td-input">
					<b>Localidad:</b><br>
					<select name="localidad" id="localidad">
					</select>
				</div>
				<div class="td-input">
					<b>Titulo:</b><br>
					<input type="text" name="titulo" id="titulo" value="<?php echo $info[0]->{'titulo'}?>">
				</div>

				<div class="td-input">
					<b>Direccion:</b><br>
					<input type="text" name="direccion" id="direccion" value="<?php echo $info[0]->{'direccion'}?>">
				</div>
				<div class="td-input">
					<b>Teléfono:</b><br>
					<input type="text" name="telefono" id="telefono" value="<?php echo $info[0]->{'telefono'}?>">
				</div>
				<div class="td-input">
					<b>Correo:</b><br>
					<input type="text" name="email" id="email" value="<?php echo $info[0]->{'email'}?>">
				</div>
				<div class="td-input">
					<b>Ubicación:</b><br>
					<input type="text" id="map_search" placeholder="Ingrese la dirección que desea buscar" style="width:50%;"><div class="btn btn-sm btn-info" style="margin-top:-4px;" id="bt_map_search">BUSCAR</div>
					<input type="hidden" name="latitud_longitud" id="latitud_longitud" placeholder="latitud,longitud"  value="<?php echo $info[0]->{'latitud_longitud'}?>">
					<script id="google_map" type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false&libraries=geometry&language=es&key=AIzaSyD4Px-mEuR54Hdl-t6Ifn4Q8H5e7ZRHTMA"></script>
					<div id="map_canvas" class="rc"></div>
				</div>
			 </form>
		  </div>
		  <div class="tab-pane" id="tab2">
			 iframe listado subtabla
		  </div>
	   </div>
	   <div class="btn btn-success btn-sm pull-right bt-save" style="margin-right:8px;">GUARDAR</div>
	   <a href="<?php echo base_url()?>centros/"><div class="btn btn-default btn-sm pull-right" style="margin-right:8px;">CANCELAR</div></a>
	</div>
</div>
<br style="clear:both;"/>