<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Centros extends CI_Controller {
	 
	function __construct()
	{
       parent::__construct();
       // testing load model
       $this->load->model('page_model');
	   // Load form helper library
	   $this->load->helper('form');
	   $this->load->helper('url');
	   // Load form validation library
	   $this->load->library('form_validation');

	   // Load session library
	   $this->load->library('session');
	} 
	 
	
	public function index()
	{
		// ----------------------------
		// testing templating method
		// ----------------------------
	
		//como hemos creado el grupo registro podemos utilizarlo
	    $this->template->set_template('template');
	    
		//añadimos los archivos css que necesitemoa
		$this->template->add_css('asset/css/concesionarios.css');
		
		//añadimos los archivos js que necesitemoa
		$this->template->add_js('asset/js/concesionarios.js');


	    
		//la sección header será el archivo views/registro/header_template
	    $this->template->write_view('header', 'layout/header');
		$this->template->write_view('nav', 'layout/nav');
	    
		//desde aquí también podemos setear el título
		$this->template->write('title', 'Administrador - Pauny', TRUE);
		$this->template->write('description', 'Administrador de contenidos', TRUE);
		$this->template->write('keywords', '', TRUE);

		$CI =& get_instance();

		$info =  $this->page_model->get_centros();
		$data['info']=$info;
		

		//el contenido de nuestro formulario estará en views/registro/formulario_registro,
		//de esta forma también podemos pasar el array data a registro/formulario_registro
	    $this->template->write_view('content', 'layout/centros/list', $data, TRUE); 
	    
		//la sección footer será el archivo views/registro/footer_template
	    //$this->template->write_view('footer', 'layout/footer');   
	    
		//con el método render podemos renderizar y hacer que se visualice la template
	    $this->template->render();
	
		 //$this->load->view('welcome_message');
	}

	public function soporte($estado)
	{
		$data = array(
			'soporte' => $estado
		);

		$this->db->where('id', $_GET['prd']);
        $this->db->update('centros', $data);

		redirect('centros/');
	}

	public function add(){
		
		//como hemos creado el grupo registro podemos utilizarlo
	    $this->template->set_template('template');
	    
		//añadimos los archivos css que necesitemoa
		//$this->template->add_css('asset/css/concesionarios.css');
		
		//añadimos los archivos js que necesitemoa
		$this->template->add_js('asset/js/centros.js?v='.time().'');
		
		//la sección header será el archivo views/registro/header_template
	    $this->template->write_view('header', 'layout/header');
		$this->template->write_view('nav', 'layout/nav');
	    
		//desde aquí también podemos setear el título
		$this->template->write('title', 'Administrador - Pauny', TRUE);
		$this->template->write('description', 'Administrador de contenidos', TRUE);
		$this->template->write('keywords', '', TRUE);

		$CI =& get_instance();

		$info =  $this->page_model->get_localidades();
		$data['provincias']=$info;
		
		$info =  $this->page_model->get_paises();
		$data['paises']=$info;


		$this->template->write_view('content', 'layout/centros/add', $data, TRUE); 
		$this->template->render();
	}
	
	public function save(){
		if (isset($this->session->userdata['logged_in'])) {
			$data = array(
				'titulo' => $_POST['titulo'],
				'direccion' => $_POST['direccion'],
				'telefono' => $_POST['telefono'],
				'email' => $_POST['email'],
				'pais' => $_POST['pais'],
				'localidad' => $_POST['localidad'],
				'latitud_longitud' => $_POST['latitud_longitud'],
			);
			$this->page_model->insert_centro($data);
			redirect('centros/');
		}else{
			redirect('login/');
		}
		
	}
	public function update(){
		if (isset($this->session->userdata['logged_in'])) {
			$data = array(
				'titulo' => $_POST['titulo'],
				'direccion' => $_POST['direccion'],
				'telefono' => $_POST['telefono'],
				'email' => $_POST['email'],
				'pais' => $_POST['pais'],
				'localidad' => $_POST['localidad'],
				'latitud_longitud' => $_POST['latitud_longitud'],
			);
			$this->page_model->update_centro($data);
			redirect('centros/');
		}else{
			redirect('login/');
		}
		
	}
	public function remove(){
		if (isset($this->session->userdata['logged_in'])) {
			$this->page_model->remove_centro();
			redirect('centros/');
		}else{
			redirect('login/');
		}
		
	}
	
	public function edit(){
		//como hemos creado el grupo registro podemos utilizarlo
	    $this->template->set_template('template');
	    
		//añadimos los archivos css que necesitemoa
		$this->template->add_css('asset/css/concesionarios.css');
		
		//añadimos los archivos js que necesitemoa
		$this->template->add_js('asset/js/centros.js');
		
		//la sección header será el archivo views/registro/header_template
	    $this->template->write_view('header', 'layout/header');
		$this->template->write_view('nav', 'layout/nav');
	    
		//desde aquí también podemos setear el título
		$this->template->write('title', 'Administrador - Pauny', TRUE);
		$this->template->write('description', 'Administrador de contenidos', TRUE);
		$this->template->write('keywords', '', TRUE);

		$CI =& get_instance();
		$info =  $this->page_model->get_centro_id($this->uri->segment(3));
		$data['info']=$info;
		
		$info =  $this->page_model->get_localidades();
		$data['provincias']=$info;
		
		$info =  $this->page_model->get_paises();
		$data['paises']=$info;
		
		$this->template->write_view('content', 'layout/centros/edit', $data, TRUE); 
		$this->template->render();
	}
	
	/*UPDATE SELECTS AJAX*/
	public function Getlocalidades()
	{
		$states =  $this->page_model->get_localidad_pais($this->input->get('id'));
		$html='';
		foreach ( $states as $fila ){
			$html.='<option value="'.$fila->{'id'}.'">'.$fila->{'localidad'}.'</option>';
		}
		if(count($states)==0){
			$html.='<option value="0">No hay provincias</option>';
		}
		print $html;
		exit;
			
	}
	public function Getprovinciasconcesionario()
	{
		$states =  $this->page_model->get_provincia_concesionario_id($this->input->get('id'));
		$html='';
		foreach ( $states as $fila ){
			$html.=$fila->{'id'};
		}
		print $html;
		exit;
			
	}

	
}
