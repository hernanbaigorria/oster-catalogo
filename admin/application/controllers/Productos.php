<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Productos extends CI_Controller {
	 
	function __construct()
	{
       parent::__construct();
       // testing load model
       $this->load->model('page_model');
	   // Load form helper library
	   $this->load->helper('form');
	   $this->load->helper('url');
	   // Load form validation library
	   $this->load->library('form_validation');

	   // Load session library
	   $this->load->library('session');
	} 
	 
	
	public function index()
	{
		// ----------------------------
		// testing templating method
		// ----------------------------
	
		//como hemos creado el grupo registro podemos utilizarlo
	    $this->template->set_template('template');
	    
		//añadimos los archivos css que necesitemoa
		$this->template->add_css('asset/css/usuarios.css');
		
		//añadimos los archivos js que necesitemoa
		$this->template->add_js('asset/js/banner.js?v='.time().'');


	    
		//la sección header será el archivo views/registro/header_template
	    $this->template->write_view('header', 'layout/header');
		$this->template->write_view('nav', 'layout/nav');
	    
		//desde aquí también podemos setear el título
		$this->template->write('title', 'Administrador', TRUE);
		$this->template->write('description', 'Administrador de contenidos', TRUE);
		$this->template->write('keywords', '', TRUE);

		$CI =& get_instance();
		$info =  $this->page_model->get_productos();
		$data['info']= $info;
		
		

		//el contenido de nuestro formulario estará en views/registro/formulario_registro,
		//de esta forma también podemos pasar el array data a registro/formulario_registro
	    $this->template->write_view('content', 'layout/productos/list', $data, TRUE); 
	    
		//la sección footer será el archivo views/registro/footer_template
	    //$this->template->write_view('footer', 'layout/footer');   
	    
		//con el método render podemos renderizar y hacer que se visualice la template
	    $this->template->render();
	
		 //$this->load->view('welcome_message');
	}

	public function estado($estado){

		$data = array(
			'nuevo' => $estado
		);

		$this->db->where('uniq', $_GET['prd']);
        $this->db->update('productos', $data);

		redirect('productos/');

	}

	public function publicar($publico){

		$data = array(
			'publicar' => $publico
		);

		$this->db->where('uniq', $_GET['prd']);
        $this->db->update('productos', $data);

		redirect('productos/');

	}

	public function add(){
		
		//como hemos creado el grupo registro podemos utilizarlo
	    $this->template->set_template('template');
	    
		//añadimos los archivos css que necesitemoa
		$this->template->add_css('asset/css/banner.css');
		
		//añadimos los archivos js que necesitemoa
		$this->template->add_js('asset/js/banner.js?v='.time().'');
		
		//la sección header será el archivo views/registro/header_template
	    $this->template->write_view('header', 'layout/header');
		$this->template->write_view('nav', 'layout/nav');
	    
		//desde aquí también podemos setear el título
		$this->template->write('title', 'Administrador', TRUE);
		$this->template->write('description', 'Administrador de contenidos', TRUE);
		$this->template->write('keywords', '', TRUE);

		$CI =& get_instance();
		
		$data['categorias']= $this->page_model->get_categorias();
		$data["lang"] =  $this->page_model->get_countries();

		$this->template->write_view('content', 'layout/productos/add', $data, TRUE); 
		$this->template->render();
	}
	
	public function save(){
		if (isset($this->session->userdata['logged_in'])) {
			if(isset($_POST['id_subcategoria'])):
				$subcateogria = $_POST['id_subcategoria'];
			else:
				$subcateogria = '';
			endif;

			$a = array('á', 'é', 'í', 'ó', 'ú', 'ñ' , 'Á', 'Ñ', 'ñ','Ú', 'Ñ');
			$b = array('a', 'e', 'i', 'o', 'u', 'n', 'A', 'Ñ', 'ñ', 'u', 'n');

			$this->db->order_by('uniq','desc');
			$this->db->limit('1');
			$result = $this->db->get('productos');
			$last_id = $result->result();
			$uniqsum = $last_id[0]->uniq+1;
			$lang = $this->page_model->get_countries();

			foreach ($lang as $keylang => $lan)
			{
				$convert = str_replace($a, $b, $_POST['titulo_es']);

				$slug = url_title(strtolower($convert));

				$data = array(
					'uniq' => $uniqsum,
					'lang' => $_POST['id_idioma_'.$lan->{"abr"}.''],
					'titulo' => $_POST['titulo_'.$lan->{"abr"}.''],
					'subtitulo' => $_POST['subtitulo_'.$lan->{"abr"}.''],
					'descripcion' => $_POST['descripcion_'.$lan->{"abr"}.''],
					'breve_descripcion' => $_POST['breve_descripcion_'.$lan->{"abr"}.''],
					'id_categoria' => $_POST['id_categoria'],
					'id_subcategoria' => $subcateogria,
					'imagen' => basename($_POST['galeria9_input']),
					//'brochure' => basename($_POST['galeria1'.$keylang.'_input']),
					//'hoja_venta' => basename($_POST['galeria2'.$keylang.'_input']),
					'slug' => $slug,
				);

				$this->page_model->insert_producto($data);
				
			}

			redirect('productos/');
			
		}
		
	}

	public function update(){
		if (isset($this->session->userdata['logged_in'])) {
			if(isset($_POST['id_subcategoria'])):
				$subcateogria = $_POST['id_subcategoria'];
			else:
				$subcateogria = '';
			endif;
			$a = array('á', 'é', 'í', 'ó', 'ú', 'ñ' , 'Á', 'Ñ', 'ñ','Ú', 'Ñ');
			$b = array('a', 'e', 'i', 'o', 'u', 'n', 'A', 'Ñ', 'ñ', 'u', 'n');

			$lang = $this->page_model->get_countries();

			foreach ($lang as $keylang => $lan)
			{
				$convert = str_replace($a, $b, $_POST['titulo_es']);

				$slug = url_title(strtolower($convert));

				//busco si tiene id de lang
				$this->db->where("lang",$lan->{'id'});
				$this->db->where("uniq",$this->uri->segment(3));
				$query = $this->db->get('productos');
				if($query->num_rows() > 0):
					$data = array(

						'titulo' => $_POST['titulo_'.$lan->{"abr"}.''],
						'subtitulo' => $_POST['subtitulo_'.$lan->{"abr"}.''],
						'descripcion' => $_POST['descripcion_'.$lan->{"abr"}.''],
						'breve_descripcion' => $_POST['breve_descripcion_'.$lan->{"abr"}.''],
						'id_categoria' => $_POST['id_categoria'],
						'id_subcategoria' => $subcateogria,
						'imagen' => basename($_POST['galeria9_input']),
						//'brochure' => basename($_POST['galeria1'.$keylang.'_input']),
						//'hoja_venta' => basename($_POST['galeria2'.$keylang.'_input']),
						'slug' => $slug,
					);

					$this->db->where('uniq', $this->uri->segment(3));
					$this->db->where('lang', $_POST['id_idioma_'.$lan->{"abr"}.'']);
	                $this->db->update('productos', $data);
				else:
					$data = array(
						'uniq' => $this->uri->segment(3),
						'lang' => $_POST['id_idioma_'.$lan->{"abr"}.''],
						'titulo' => $_POST['titulo_'.$lan->{"abr"}.''],
						'subtitulo' => $_POST['subtitulo_'.$lan->{"abr"}.''],
						'descripcion' => $_POST['descripcion_'.$lan->{"abr"}.''],
						'breve_descripcion' => $_POST['breve_descripcion_'.$lan->{"abr"}.''],
						'id_categoria' => $_POST['id_categoria'],
						'id_subcategoria' => $subcateogria,
						'imagen' => basename($_POST['galeria9_input']),
						//'brochure' => basename($_POST['galeria1'.$keylang.'_input']),
						//'hoja_venta' => basename($_POST['galeria2'.$keylang.'_input']),
						'slug' => $slug,
					);
	                $this->db->insert('productos', $data);
				endif;
			}

			redirect('productos/');
		}else{
			redirect('login/');
		}
		
	}

	public function remove(){
		if (isset($this->session->userdata['logged_in'])) {
			$this->page_model->remove_producto();
			redirect('productos/');
		}else{
			redirect('login/');
		}
		
	}
	
	public function edit(){
		//como hemos creado el grupo registro podemos utilizarlo
	    $this->template->set_template('template');
	    
		//añadimos los archivos css que necesitemoa
		$this->template->add_css('asset/css/banner.css');
		
		//añadimos los archivos js que necesitemoa
		$this->template->add_js('asset/js/banner.js?v='.time().'');
		
		//la sección header será el archivo views/registro/header_template
	    $this->template->write_view('header', 'layout/header');
		$this->template->write_view('nav', 'layout/nav');
	    
		//desde aquí también podemos setear el título
		$this->template->write('title', 'Administrador', TRUE);
		$this->template->write('description', 'Administrador de contenidos', TRUE);
		$this->template->write('keywords', '', TRUE);

		$CI =& get_instance();
		$data['categorias']= $this->page_model->get_categorias();
		$info =  $this->page_model->get_producto_id($this->uri->segment(3));		
		$data['info']=$info;
		$data["lang"] =  $this->page_model->get_countries();
		$this->template->write_view('content', 'layout/productos/edit', $data, TRUE); 
		$this->template->render();
	}
	
	// Logout from admin page
	public function logout() {
		// Removing session data
		$sess_array = array(
		'username' => ''
		);
		$this->session->unset_userdata('logged_in', $sess_array);
		$data['message_display'] = 'Successfully Logout';
		redirect('home/');
	}
	
	public function SendForm()
	{
		$parmsJSON = (isset($_POST['_p']))?$_POST['_p']:$_GET['_p'];
		$parmsJSON = urldecode(base64_decode ( $parmsJSON ));
		$JSON = new Services_JSON();
		$parmsJSON = $JSON->decode($parmsJSON);
		$asunto = $parmsJSON->{'asunto'};
		$mensaje = rawURLdecode($parmsJSON->{'mensaje'});
		$name = $parmsJSON->{'name'};
		$email = $parmsJSON->{'email'};
		$para = $parmsJSON->{'para'};
			
		$mAIL = new MAIL;
		$mAIL->From($email,$name);
        							
		$mAIL->AddTo($para);
		 $mAIL->Subject(utf8_encode($asunto));
									
	     $contact['message_body'] = $mensaje;
							        
		$mAIL->Html($contact['message_body']);
									
	 
		$cON = $mAIL->Connect("smtp.gmail.com", (int)465, "diego.mantovani@gmail.com", "p4t0f30p4t0f30", "tls") or die(print_r($mAIL->Result));
        $mAIL->Send($cON) ? $sent = true : $sent = false;
		$mAIL->Disconnect();
		
		
		if(!$sent) {
		 print '{"resultado":"NO","error":"'.$mAIL->Result.'"}';
		} else {
		  print '{"resultado":"OK"}';
		}

		exit;
			
	}
}
