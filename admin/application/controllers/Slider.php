<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Slider extends CI_Controller {
	 
	function __construct()
	{
       parent::__construct();
       // testing load model
       $this->load->model('page_model');
	   // Load form helper library
	   $this->load->helper('form');
	   $this->load->helper('url');
	   // Load form validation library
	   $this->load->library('form_validation');

	   // Load session library
	   $this->load->library('session');
	} 
	 
	
	public function index()
	{
		// ----------------------------
		// testing templating method
		// ----------------------------
	
		//como hemos creado el grupo registro podemos utilizarlo
	    $this->template->set_template('template');
	    
		//añadimos los archivos css que necesitemoa
		$this->template->add_css('asset/css/usuarios.css');
		
		//añadimos los archivos js que necesitemoa
		$this->template->add_js('asset/js/banner.js?v='.time().'');


	    
		//la sección header será el archivo views/registro/header_template
	    $this->template->write_view('header', 'layout/header');
		$this->template->write_view('nav', 'layout/nav');
	    
		//desde aquí también podemos setear el título
		$this->template->write('title', 'Administrador', TRUE);
		$this->template->write('description', 'Administrador de contenidos', TRUE);
		$this->template->write('keywords', '', TRUE);

		$CI =& get_instance();
		$info =  $this->page_model->get_sliders();
		$data['info']= $info;
		

		//el contenido de nuestro formulario estará en views/registro/formulario_registro,
		//de esta forma también podemos pasar el array data a registro/formulario_registro
	    $this->template->write_view('content', 'layout/slider/list', $data, TRUE); 
	    
		//la sección footer será el archivo views/registro/footer_template
	    //$this->template->write_view('footer', 'layout/footer');   
	    
		//con el método render podemos renderizar y hacer que se visualice la template
	    $this->template->render();
	
		 //$this->load->view('welcome_message');
	}

	public function add(){
		
		//como hemos creado el grupo registro podemos utilizarlo
	    $this->template->set_template('template');
	    
		//añadimos los archivos css que necesitemoa
		$this->template->add_css('asset/css/banner.css');
		
		//añadimos los archivos js que necesitemoa
		$this->template->add_js('asset/js/banner.js?v='.time().'');
		
		//la sección header será el archivo views/registro/header_template
	    $this->template->write_view('header', 'layout/header');
		$this->template->write_view('nav', 'layout/nav');
	    
		//desde aquí también podemos setear el título
		$this->template->write('title', 'Administrador', TRUE);
		$this->template->write('description', 'Administrador de contenidos', TRUE);
		$this->template->write('keywords', '', TRUE);

		$CI =& get_instance();
		
		$data["lang"] =  $this->page_model->get_countries();
		$this->template->write_view('content', 'layout/slider/add', $data, TRUE); 
		$this->template->render();
	}
	
	public function save(){
		if (isset($this->session->userdata['logged_in'])) {

			$this->db->order_by('uniq','desc');
			$this->db->limit('1');
			$result = $this->db->get('slider');
			$last_id = $result->result();
			$uniqsum = $last_id[0]->uniq+1;
			$lang = $this->page_model->get_countries();
			foreach ($lang as $lan)
			{

				$data = array(
					'uniq' => $uniqsum,
					'titulo' => $_POST['titulo_'.$lan->{"abr"}.''],
					'descripcion' => $_POST['descripcion_'.$lan->{"abr"}.''],
					'link' => $_POST['link_'.$lan->{"abr"}.''],
					'imagen' => basename($_POST['galeria1_input']),
					'lang' => $_POST['id_idioma_'.$lan->{"abr"}.''],
				);

				$this->page_model->insert_slider($data);
				
			}

			redirect('slider/');
			
		}
		
	}
	public function update(){
		if (isset($this->session->userdata['logged_in'])) {

			$lang = $this->page_model->get_countries();

			foreach ($lang as $lan)
			{

				//busco si tiene id de lang
				$this->db->where("lang",$lan->{'id'});
				$this->db->where("uniq",$this->uri->segment(3));
				$query = $this->db->get('slider');
				if($query->num_rows() > 0):

					$data = array(
						'titulo' => $_POST['titulo_'.$lan->{"abr"}.''],
						'descripcion' => $_POST['descripcion_'.$lan->{"abr"}.''],
						'link' => $_POST['link_'.$lan->{"abr"}.''],
						'imagen' => basename($_POST['galeria1_input']),
					);

					$this->db->where('uniq', $this->uri->segment(3));
					$this->db->where('lang', $_POST['id_idioma_'.$lan->{"abr"}.'']);
	                $this->db->update('slider', $data);
				else:
					$data = array(
						'uniq' => $this->uri->segment(3),
						'lang' => $_POST['id_idioma_'.$lan->{"abr"}.''],
						'titulo' => $_POST['titulo_'.$lan->{"abr"}.''],
						'descripcion' => $_POST['descripcion_'.$lan->{"abr"}.''],
						'link' => $_POST['link_'.$lan->{"abr"}.''],
						'imagen' => basename($_POST['galeria1_input']),
					);
	                $this->db->insert('slider', $data);
				endif;
			}
			
			redirect('slider/');
		}else{
			redirect('login/');
		}
		
	}
	public function remove(){
		if (isset($this->session->userdata['logged_in'])) {
			$this->page_model->remove_slider();
			redirect('slider/');
		}else{
			redirect('login/');
		}
		
	}
	
	public function edit(){
		//como hemos creado el grupo registro podemos utilizarlo
	    $this->template->set_template('template');
	    
		//añadimos los archivos css que necesitemoa
		$this->template->add_css('asset/css/banner.css');
		
		//añadimos los archivos js que necesitemoa
		$this->template->add_js('asset/js/banner.js?v='.time().'');
		
		//la sección header será el archivo views/registro/header_template
	    $this->template->write_view('header', 'layout/header');
		$this->template->write_view('nav', 'layout/nav');
	    
		//desde aquí también podemos setear el título
		$this->template->write('title', 'Administrador', TRUE);
		$this->template->write('description', 'Administrador de contenidos', TRUE);
		$this->template->write('keywords', '', TRUE);

		$CI =& get_instance();
		$info =  $this->page_model->get_slider_id($this->uri->segment(3));		
		$data['info']=$info;
		$data["lang"] =  $this->page_model->get_countries();
		
		$this->template->write_view('content', 'layout/slider/edit', $data, TRUE); 
		$this->template->render();
	}
	
}
