$(document).ready(function() {
    var calendar = $('#calendar').fullCalendar({
        monthNames: JSON.parse(meses),
        dayNamesShort: JSON.parse(dias),
        buttonText: {today: hoy},
        editable:true,
        events: 'events/',
        selectable:true,
        selectHelper:true,
        eventClick: function(info) {
            $('.modal-events').removeClass('d-none');
            $('.modal-events').addClass('d-flex');

            $('.title-event').html(info.title);
            $('.description-event').html(info.description);
            $('.link-modal').html('<a href="'+info.link+'" target="_blank">'+info.link+'</a>');
            if(info.image != ""){
                $('.image-events').html('<img src="'+info.base_url+'asset/img/uploads/'+info.image+'" class="img-fluid">');
            }
        },
    });
});

$( ".cierre-modal" ).click(function() {
  $('.modal-events').removeClass('d-flex');
  $('.modal-events').addClass('d-none');
});