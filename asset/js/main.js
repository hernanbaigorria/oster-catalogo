$('#HeaderHome').owlCarousel({
  loop: true,
  margin: 10,
  nav: true,
  navText: [
    "<i class='fa fa-caret-left'></i>",
    "<i class='fa fa-caret-right'></i>"
  ],
  autoplay: true,
  autoplayHoverPause: true,
  responsive: {
    0: {
      items: 1
    },
    600: {
      items: 1
    },
    1000: {
      items: 1
    }
  }
});


$('#LastViewed').owlCarousel({
  loop: true,
  margin: 10,
  nav: true,
  navText: [
    "<i class='fa fa-caret-left'></i>",
    "<i class='fa fa-caret-right'></i>"
  ],
  autoplay: true,
  autoplayHoverPause: true,
  responsive: {
    0: {
      items: 1
    },
    600: {
      items: 1
    },
    1000: {
      items: 4
    }
  }
});

$(document).ready(function() {
    $('.xzoom, .xzoom-gallery').xzoom({zoomWidth: 300, title: false, tint: '#fff', Xoffset: 15});
    $('.xzoom2, .xzoom-gallery2').xzoom({position: '#xzoom2-id', tint: '#ffa200'});
    $('.xzoom3, .xzoom-gallery3').xzoom({position: 'lens', lensShape: 'circle', sourceClass: 'xzoom-hidden'});
    $('.xzoom4, .xzoom-gallery4').xzoom({tint: '#006699', Xoffset: 15});
    $('.xzoom5, .xzoom-gallery5').xzoom({tint: '#006699', Xoffset: 15});

    //Integration with hammer.js
    var isTouchSupported = 'ontouchstart' in window;

    if (isTouchSupported) {
        //If touch device
        $('.xzoom, .xzoom2, .xzoom3, .xzoom4, .xzoom5').each(function(){
            var xzoom = $(this).data('xzoom');
            xzoom.eventunbind();
        });
        
        $('.xzoom, .xzoom2, .xzoom3').each(function() {
            var xzoom = $(this).data('xzoom');
            $(this).hammer().on("tap", function(event) {
                event.pageX = event.gesture.center.pageX;
                event.pageY = event.gesture.center.pageY;
                var s = 1, ls;

                xzoom.eventmove = function(element) {
                    element.hammer().on('drag', function(event) {
                        event.pageX = event.gesture.center.pageX;
                        event.pageY = event.gesture.center.pageY;
                        xzoom.movezoom(event);
                        event.gesture.preventDefault();
                    });
                }

                xzoom.eventleave = function(element) {
                    element.hammer().on('tap', function(event) {
                        xzoom.closezoom();
                    });
                }
                xzoom.openzoom(event);
            });
        });

    $('.xzoom4').each(function() {
        var xzoom = $(this).data('xzoom');
        $(this).hammer().on("tap", function(event) {
            event.pageX = event.gesture.center.pageX;
            event.pageY = event.gesture.center.pageY;
            var s = 1, ls;

            xzoom.eventmove = function(element) {
                element.hammer().on('drag', function(event) {
                    event.pageX = event.gesture.center.pageX;
                    event.pageY = event.gesture.center.pageY;
                    xzoom.movezoom(event);
                    event.gesture.preventDefault();
                });
            }

            var counter = 0;
            xzoom.eventclick = function(element) {
                element.hammer().on('tap', function() {
                    counter++;
                    if (counter == 1) setTimeout(openfancy,300);
                    event.gesture.preventDefault();
                });
            }

            function openfancy() {
                if (counter == 2) {
                    xzoom.closezoom();
                    $.fancybox.open(xzoom.gallery().cgallery);
                } else {
                    xzoom.closezoom();
                }
                counter = 0;
            }
        xzoom.openzoom(event);
        });
    });
    
    $('.xzoom5').each(function() {
        var xzoom = $(this).data('xzoom');
        $(this).hammer().on("tap", function(event) {
            event.pageX = event.gesture.center.pageX;
            event.pageY = event.gesture.center.pageY;
            var s = 1, ls;

            xzoom.eventmove = function(element) {
                element.hammer().on('drag', function(event) {
                    event.pageX = event.gesture.center.pageX;
                    event.pageY = event.gesture.center.pageY;
                    xzoom.movezoom(event);
                    event.gesture.preventDefault();
                });
            }

            var counter = 0;
            xzoom.eventclick = function(element) {
                element.hammer().on('tap', function() {
                    counter++;
                    if (counter == 1) setTimeout(openmagnific,300);
                    event.gesture.preventDefault();
                });
            }

            function openmagnific() {
                if (counter == 2) {
                    xzoom.closezoom();
                    var gallery = xzoom.gallery().cgallery;
                    var i, images = new Array();
                    for (i in gallery) {
                        images[i] = {src: gallery[i]};
                    }
                    $.magnificPopup.open({items: images, type:'image', gallery: {enabled: true}});
                } else {
                    xzoom.closezoom();
                }
                counter = 0;
            }
            xzoom.openzoom(event);
        });
    });

    } else {
        //If not touch device

        //Integration with fancybox plugin
        $('#xzoom-fancy').bind('click', function(event) {
            var xzoom = $(this).data('xzoom');
            xzoom.closezoom();
            $.fancybox.open(xzoom.gallery().cgallery, {padding: 0, helpers: {overlay: {locked: false}}});
            event.preventDefault();
        });
       
        //Integration with magnific popup plugin
        $('#xzoom-magnific').bind('click', function(event) {
            var xzoom = $(this).data('xzoom');
            xzoom.closezoom();
            var gallery = xzoom.gallery().cgallery;
            var i, images = new Array();
            for (i in gallery) {
                images[i] = {src: gallery[i]};
            }
            $.magnificPopup.open({items: images, type:'image', gallery: {enabled: true}});
            event.preventDefault();
        });
    }
});

$(document).ready(function(e){
  $('.custom-select').click(function(e){
    e.stopPropagation();
    $( '.select-options', this ).slideToggle();
  });
  
  $( '.select-options li' ).click(function(e){
    e.stopPropagation();
    const text_inside = $(this).text();
    $(this).parent().slideUp();
    $(this).parent().next().text( text_inside );
  });
  
  $( document ).click(function(e){
    $( '.select-options' ).slideUp();
  });


  $('.custom-select-foot').click(function(e){
    e.stopPropagation();
    $( '.select-options', this ).slideToggle();
  });
  
  $( '.select-options li' ).click(function(e){
    e.stopPropagation();
    const text_inside = $(this).text();
    $(this).parent().slideUp();
    $(this).parent().next().text( text_inside );
  });

  $('[aria-expanded="false"]').click(function(){

    var open = $(".icon-menu-toggle").hasClass("fa-times");
    var close = $(".icon-menu-toggle").hasClass("fa-bars");
    // hasClass is boolean
    if(open === true)
    {
        $('.icon-menu-toggle').removeClass('fa-times');
        $('.icon-menu-toggle').addClass('fa-bars');
    }

    if(close === true)
    {
        $('.icon-menu-toggle').removeClass('fa-bars');
        $('.icon-menu-toggle').addClass('fa-times');
    }
  });
  
  $( document ).click(function(e){
    $( '.select-options' ).slideUp();
  });
});



