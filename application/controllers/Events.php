<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Events extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 
	function __construct()
	{
       parent::__construct();
       // testing load model
       $this->load->model('page_model');
	   $this->load->helper('url');
	   $this->load->helper('cookie');
	   $this->load->helper('language');
	   $this->load->library('session');
	} 
	 
	
	public function index()
	{	
		$leng = $this->config->item('language_abbr');
		//Choose language file according to selected lanaguage
		//print_r($language);
		//exit;
		if($language == "portuguese"):
			$this->lang->load('web_lang','portuguese');
			$data['shortname'] = "pt";
			$data['language'] = $language;
		elseif ($language == "spanish"):
			$this->lang->load('web_lang','spanish');
			$data['shortname'] = "es";
			$data['language'] = $language;
		elseif ($language == "english"):
			$load_en = $this->lang->load('web_lang','english');
			$data['shortname'] = "en";
			$data['language'] = $language;
		else:
			
            if ($leng == 'ar'){
            	$this->lang->load('web_lang','spanish');
				$data['shortname'] = "es";
				$data['language'] = "spanish";
			}


			if ($leng == 'br'){
				$this->lang->load('web_lang','portuguese');
				$data['shortname'] = "pt";
				$data['language'] = "portuguese";
			}

			if ($leng == 'us'){
				$load_en = $this->lang->load('web_lang','english');
				$data['shortname'] = "en";
				$data['language'] = "english";
			}

		endif;

		$events = $this->page_model->get_events($data['shortname']);
		foreach ($events as $event) {
		    $ev[] = array(
		        'id' => $event->uniq,
		        'title' => $event->title,
		        'description' => $event->description,
		        'link' => $event->link,
		        'image' => $event->image,
		        'base_url' => base_url(),
		        'start' => $event->event_date
		    );
		}

		
		echo json_encode($ev);
	}

	
}
