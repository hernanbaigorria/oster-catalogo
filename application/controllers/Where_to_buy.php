<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Where_to_buy extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 
	function __construct()
	{
       parent::__construct();
       // testing load model
       $this->load->model('page_model');
	   $this->load->helper('url');
	   $this->load->helper('cookie');
	   $this->load->helper('language');
	   $this->load->library('session');
	} 
	 
	
	public function index()
	{	
		
		ini_set('max_execution_time', 0); 
		ini_set('memory_limit','2048M');
		// ----------------------------
		// testing templating method
		// ----------------------------
	
		//como hemos creado el grupo registro podemos utilizarlo
	    $this->template->set_template('template');

		$this->template->add_css('asset/css/home.css');

		//añadimos los archivos js que necesitemoa
		//$this->template->add_js('asset/js/maps.js');

		// --		
		// Save utm
		// --
	    if(isset($_GET["utm_medium"]) && strlen($_GET["utm_medium"]) > 1)
	    {
	    		$this->session->set_userdata("utm_medium",$_GET["utm_medium"]);	   
	    }
	   
	    if(isset($_GET["utm_source"])  && strlen($_GET["utm_source"]) > 1)
	    {
 	   	 	$this->session->set_userdata("utm_source",$_GET["utm_source"]);	   
 	    }
		
		//añadimos los archivos js que necesitemoa		
		//$this->template->add_js('asset/js/home.js');
	    
		//desde aquí también podemos setear el título
		$this->template->write('title', 'Oster Catalogo', TRUE);
		$this->template->write('description', '', TRUE);
		$this->template->write('keywords', '', TRUE);
		$this->template->write('image', '', TRUE);
		$this->template->write('ogType', 'website', TRUE);
		//obtenemos los usuarios
		//$data['users'] = array("aaa" => "bbb"); // $this->page_model->get_users();	
		$CI =& get_instance();	

		$leng = $this->config->item('language_abbr');
		//Choose language file according to selected lanaguage
		//print_r($language);
		//exit;
		if($language == "portuguese"):
			$this->lang->load('web_lang','portuguese');
			$data['shortname'] = "pt";
			$data['language'] = $language;
		elseif ($language == "spanish"):
			$this->lang->load('web_lang','spanish');
			$data['shortname'] = "es";
			$data['language'] = $language;
		elseif ($language == "english"):
			$load_en = $this->lang->load('web_lang','english');
			$data['shortname'] = "en";
			$data['language'] = $language;
		else:
			
            if ($leng == 'ar'){
            	$this->lang->load('web_lang','spanish');
				$data['shortname'] = "es";
				$data['language'] = "spanish";
			}


			if ($leng == 'br'){
				$this->lang->load('web_lang','portuguese');
				$data['shortname'] = "pt";
				$data['language'] = "portuguese";
			}

			if ($leng == 'us'){
				$load_en = $this->lang->load('web_lang','english');
				$data['shortname'] = "en";
				$data['language'] = "english";
			}

		endif;
		
		$data['paises'] = $this->page_model->get_centros_bypaises();
 		
		$this->template->write_view('content', 'layout/where-to-buy/main', $data);
		
		$this->template->write_view('header', 'layout/header', $data);
		 
	    
		$this->template->write_view('footer', 'layout/footer');   
	    
		
		//con el método render podemos renderizar y hacer que se visualice la template
	    $this->template->render();
	
		 //$this->load->view('welcome_message');
	}

	public function maps()
	{	
		
		ini_set('max_execution_time', 0); 
		ini_set('memory_limit','2048M');
		// ----------------------------
		// testing templating method
		// ----------------------------
	
		//como hemos creado el grupo registro podemos utilizarlo
	    $this->template->set_template('template');

		$this->template->add_css('asset/css/home.css');

		//añadimos los archivos js que necesitemoa
		$this->template->add_js('asset/js/maps.js');

		// --		
		// Save utm
		// --
	    if(isset($_GET["utm_medium"]) && strlen($_GET["utm_medium"]) > 1)
	    {
	    		$this->session->set_userdata("utm_medium",$_GET["utm_medium"]);	   
	    }
	   
	    if(isset($_GET["utm_source"])  && strlen($_GET["utm_source"]) > 1)
	    {
 	   	 	$this->session->set_userdata("utm_source",$_GET["utm_source"]);	   
 	    }
		
		//añadimos los archivos js que necesitemoa		
		//$this->template->add_js('asset/js/home.js');
	    
		//desde aquí también podemos setear el título
		$this->template->write('title', 'Oster Catalogo', TRUE);
		$this->template->write('description', '', TRUE);
		$this->template->write('keywords', '', TRUE);
		$this->template->write('image', '', TRUE);
		$this->template->write('ogType', 'website', TRUE);
		//obtenemos los usuarios
		//$data['users'] = array("aaa" => "bbb"); // $this->page_model->get_users();	
		$CI =& get_instance();	

		$leng = $this->config->item('language_abbr');
		//Choose language file according to selected lanaguage
		//print_r($language);
		//exit;
		if($language == "portuguese"):
			$this->lang->load('web_lang','portuguese');
			$data['shortname'] = "pt";
			$data['language'] = $language;
		elseif ($language == "spanish"):
			$this->lang->load('web_lang','spanish');
			$data['shortname'] = "es";
			$data['language'] = $language;
		elseif ($language == "english"):
			$load_en = $this->lang->load('web_lang','english');
			$data['shortname'] = "en";
			$data['language'] = $language;
		else:
			
            if ($leng == 'ar'){
            	$this->lang->load('web_lang','spanish');
				$data['shortname'] = "es";
				$data['language'] = "spanish";
			}


			if ($leng == 'br'){
				$this->lang->load('web_lang','portuguese');
				$data['shortname'] = "pt";
				$data['language'] = "portuguese";
			}

			if ($leng == 'us'){
				$load_en = $this->lang->load('web_lang','english');
				$data['shortname'] = "en";
				$data['language'] = "english";
			}

		endif;
		
		$data['map'] = $this->page_model->get_map($_GET['ctry']);
		$data['localidades'] = $this->page_model->get_localidades_map($_GET['ctry']);
 		
		$this->template->write_view('content', 'layout/where-to-buy/maps', $data);
		
		$this->template->write_view('header', 'layout/header', $data);
		 
	    
		$this->template->write_view('footer', 'layout/footer');   
	    
		
		//con el método render podemos renderizar y hacer que se visualice la template
	    $this->template->render();
	
		 //$this->load->view('welcome_message');
	}

	
}
