<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 
	function __construct()
	{
       parent::__construct();
       // testing load model
       $this->load->model('page_model');
	   $this->load->helper('url');
	   $this->load->helper('cookie');
	   $this->load->helper('language');
	   $this->load->library('session');
	} 
	 
	
	public function index()
	{	
		
		ini_set('max_execution_time', 0); 
		ini_set('memory_limit','2048M');
		// ----------------------------
		// testing templating method
		// ----------------------------
	
		//como hemos creado el grupo registro podemos utilizarlo
	    $this->template->set_template('template');

		$this->template->add_css('asset/css/contacts.css');

		//añadimos los archivos js que necesitemoa
		//$this->template->add_js('asset/js/maps.js');

		// --		
		// Save utm
		// --
	    if(isset($_GET["utm_medium"]) && strlen($_GET["utm_medium"]) > 1)
	    {
	    		$this->session->set_userdata("utm_medium",$_GET["utm_medium"]);	   
	    }
	   
	    if(isset($_GET["utm_source"])  && strlen($_GET["utm_source"]) > 1)
	    {
 	   	 	$this->session->set_userdata("utm_source",$_GET["utm_source"]);	   
 	    }
		
		//añadimos los archivos js que necesitemoa		
		//$this->template->add_js('asset/js/home.js');
	    
		//desde aquí también podemos setear el título
		$this->template->write('title', 'Oster Catalogo', TRUE);
		$this->template->write('description', '', TRUE);
		$this->template->write('keywords', '', TRUE);
		$this->template->write('image', '', TRUE);
		$this->template->write('ogType', 'website', TRUE);
		//obtenemos los usuarios
		//$data['users'] = array("aaa" => "bbb"); // $this->page_model->get_users();	
		$CI =& get_instance();

		$leng = $this->config->item('language_abbr');
		//Choose language file according to selected lanaguage
		//print_r($language);
		//exit;
		if($language == "portuguese"):
			$this->lang->load('web_lang','portuguese');
			$data['shortname'] = "pt";
			$data['language'] = $language;
		elseif ($language == "spanish"):
			$this->lang->load('web_lang','spanish');
			$data['shortname'] = "es";
			$data['language'] = $language;
		elseif ($language == "english"):
			$load_en = $this->lang->load('web_lang','english');
			$data['shortname'] = "en";
			$data['language'] = $language;
		else:
			
            if ($leng == 'ar'){
            	$this->lang->load('web_lang','spanish');
				$data['shortname'] = "es";
				$data['language'] = "spanish";
			}


			if ($leng == 'br'){
				$this->lang->load('web_lang','portuguese');
				$data['shortname'] = "pt";
				$data['language'] = "portuguese";
			}

			if ($leng == 'us'){
				$load_en = $this->lang->load('web_lang','english');
				$data['shortname'] = "en";
				$data['language'] = "english";
			}

		endif;
		
		$data['paises'] = $this->page_model->get_paises();
 		
		$this->template->write_view('content', 'layout/contact/main', $data);
		
		$this->template->write_view('header', 'layout/header', $data);
		 
	    
		$this->template->write_view('footer', 'layout/footer');   
	    
		
		//con el método render podemos renderizar y hacer que se visualice la template
	    $this->template->render();
	
		 //$this->load->view('welcome_message');
	}

	public function save_newsletter()
	{	
		date_default_timezone_set('America/Los_Angeles');
		$data = array(
			'email' => $_POST['email'],
			'fecha_registro' => date("Y-m-d H:i:s")
		);

		$this->db->insert('newsletter', $data);
	}

	public function save_product()
	{	
		date_default_timezone_set('America/Los_Angeles');
		$data = array(
			'nombre' => $_POST['name'],
			'apellido' => $_POST['lastname'],
			'email' => $_POST['email'],
			'pais' => $_POST['country'],
			'direccion' => $_POST['address'],
			'numero_modelo' => $_POST['modelnumber'],
			'fecha_compra' => $_POST['purchase'],
			'fecha_registro' => date("Y-m-d H:i:s")
		);

		$this->db->insert('registro_producto', $data);
	}

	public function save_contact()
	{	
		date_default_timezone_set('America/Los_Angeles');


		$data = array(
			'nombre' => $_POST['name'],
			'apellido' => $_POST['lastname'],
			'email' => $_POST['email'],
			'pais' => $_POST['country'],
			'asunto' => $_POST['subject'],
			'mensaje' => $_POST['message'],
			'fecha_registro' => date("Y-m-d H:i:s")
		);

		$this->db->insert('contactos', $data);
	}

	
}
