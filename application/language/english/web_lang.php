<?php 

	$lang['esp'] = 'Spanish';
	$lang['en'] = 'English';
	$lang['port'] = 'Portuguese';

  $lang['search'] = 'Search';

	$lang['last_view'] = 'Last Viewed Items';
	$lang['new_products'] = 'What’s New';
	$lang['learn_more'] = 'Learn More';

  $lang['meses'] = '["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]';
  $lang['dias'] = '["Mon", "Tue", "Wed", "Thur", "Fri", "Sat", "Sun"]';
  $lang['hoy'] = 'Today';

	$lang['privacy'] = 'Privacy Statement';
	$lang['cookie'] = 'Cookie Policy';
	$lang['terms'] = 'Terms of Use';
  $lang['menu_contact'] = 'Contact Us';


  $lang['menu_service'] = 'Service & Support';
  $lang['menu_product'] = 'Product Registration';
  $lang['menu_buy'] = 'Where to Buy';

  $lang['header_buy'] = 'header-buy.png';
  $lang['header_service'] = 'header-support.png';
  $lang['header_service_sub'] = 'CHOOSE YOUR COUNTRY';
  $lang['header_service_desc'] = '<strong>Service options during the warranty period:</strong><br>For technical support contact the nearest service center';

  $lang['social_info'] = 'Follow us on your favorite platforms.';

  $lang['back_top'] = 'Back to top';

  $lang['title_resource'] = 'Resources';
  $lang['title_overview'] = 'Overview';

  $lang['newsletter_info'] = 'By submitting, I confirm I have read your Privacy Statement and I would like to receive marketing and/or promotional emails from Oster.';
  $lang['newsletter_send'] = 'Sign Up';

  $lang['contact_title'] = 'Contact Us';
  $lang['contact_sub'] = 'All mandatory fields marked with';

  $lang['input_name'] = 'First Name';
  $lang['input_lastname'] = 'Last Name';
  $lang['input_email'] = 'Email Address';
  $lang['input_country'] = 'Country';
  $lang['input_subject'] = 'Subject';
  $lang['input_message'] = 'Message';

  $lang['input_address'] = 'Address';
  $lang['input_model'] = 'Model Number';
  $lang['input_date'] = 'Purchase Date';

  $lang['input_terms'] = 'I would like to receive marketing and/or promotional emails from Oster.';
  $lang['input_subterms'] = 'By submitting, I confirm I have read your <a href="'.base_url().'us/privacidad/" target="_blank">Privacy Statement.</a>';
  $lang['input_submit'] = 'Submit';

	$lang['privacidad_txt'] = '<div id="heading">
    <h1> NEWELL BRANDS PRIVACY STATEMENT </h1>
    <h3> Last Modification: v2.6 September 01 2020 </h3>
  </div>
  <div id="container">
      <div>TOPICS:</div>  
          <h5><a href="#1">1.	Your Privacy is Important to Us</a></h5>
          <h5><a href="#2">2.	Personal Data We Collect About You</a></h5>
          <h5><a href="#3">3.	Why We Process Your Personal Data, Legal Bases for Processing and Retention</a></h5>
          <h5><a href="#4">4.	How We Share Your Personal Data</a></h5>
          <h5><a href="#5">5.	Sensitive Personal Data</a></h5>
          <h5><a href="#6">6.	International Data Transfers</a></h5>
          <h5><a href="#7">7.	Your Rights Regarding Access to and Control Over Personal Data</a></h5>
          <h5><a href="#8">8.	Data Controller and Data Protection Contact Information</a></h5>
          <h5><a href="#9">9.	Personal Data Submitted by Children</a></h5>
          <h5><a href="#10">10.Cookies, Web Beacons and Other Tracking Technologies</a></h5>
          <h5><a href="#11">11.Security</a></h5>
          <h5><a href="#12">12.Links to Third Party Sites</a></h5>
          <h5><a href="#13">13.Do Not Track</a></h5>
          <h5><a href="#14">14.Using Mobile and Other Devices</a></h5>
          <h5><a href="#15">15.Email Marketing Communications</a></h5>
          <h5><a href="#16">16.Text Messaging</a></h5>
          <h5><a href="#17">17.Changes to Our Privacy Statement</a></h5>
          <h5><a href="#18">18.Contact Us</a></h5>
          <h5><a href="#19">19.What’s New</a></h5>     
      
      <h2 id="1">1. YOUR PRIVACY IS IMPORTANT TO US</h2>
      <div>
          At Newell Brands, we strive to honor the privacy and security of our users, customers and suppliers, as well as their representatives, in relation to all products, services, applications and websites provided by Newell Brands, Inc., or any of its affiliates (“Newell Brands”), when acting as a Controller under relevant data protection rules and regulations.
      </div>
      <br>
      <div>
          This Privacy Statement describes our privacy practices as required by the General Data Protection Regulation (“GDPR”) (EU) (2016/679) and other applicable data protection laws (collectively “Privacy Laws”). This Privacy Statement applies where Newell Brands is acting as a Data Controller with respect to the Personal Data of our customers, employees, website users, partners and service providers.  In other words, this Privacy Statement applies where we determine the purposes and means of the processing of Personal Data.
     <br></br>
          This Privacy Statement provides information regarding how we collect, process, share, transfer, retain and protect your Personal Data and the rights you may exercise, where applicable, regarding your information.  If you provide us with your Personal Data, or we obtain Personal Data about you from other sources, we treat your Personal Data according to this Privacy Policy.
          <br></br>This Privacy Policy applies to Newell Brands’ websites including, not limited to newellbrands.com, Newell Brands mobile apps, Newell Brands dedicated social media accounts, internet and wi-fi enabled devices, etc. (“Sites”).  This Privacy Policy does not apply to third-party applications, products, services, websites or social media features that may be accessed through links on our Sites that have their own privacy policies.  Accessing those links will cause you to leave our Sites and may result in the collection and/or sharing your Personal Data by a third party. We do not control, endorse or make any representations about third-party websites or their privacy practices, which may differ from ours.  We encourage you to review the privacy practices of any site you interact with before allowing the collection and use of your Personal Data. If you have any questions or comments regarding this Privacy Statement or our data privacy practices, please contact us at 
          <a href="data-privacy-enquiry-form_en.html">Data Privacy Enquiry</a> .
      </div>
    
    <h2 id="2">2. PERSONAL DATA WE COLLECT ABOUT YOU </h2>

    <div>
        When you complete a transaction, submit a request, set up an account, apply for a position, visit our Sites, etc., with any Newell Brands entity, you may be asked to provide certain information, including, but not limited to, your name, title, company, address, phone number, e-mail address, credit card information, and line of business.  You may also be providing us with data regarding your devices including, your IP address, your geolocation, your data usage, your client or supplier account, etc.  In limited circumstances, we may collect information related to you, and your family members, such as age, gender and civil status.  This information and data may be Personal Data under applicable Privacy Laws.   
    </div>
    
    <h2 id="3">3.	WHY WE PROCESS YOUR PERSONAL DATA, LEGAL BASES FOR PROCESSING AND RETENTION</h2>
    <div>Please consult the table below to see the categories of Personal Data we may collect about you, why we process (collect, use, store, etc.) your Personal Data, the legal grounds for such processing (where applicable) and the relevant maximum retention period. </div>
<br>
    <table>
      <tr>
        <th>Purpose</th>
        <th>Categories of Personal Data</th>
        <th>Legal Bases for Processing (if required by applicable Privacy Laws)</th>
        <th>Retention Period (max*)</th>
      </tr>
      <tr>
        <td>Customer Management (including provision of services, invoicing, customer support; account management, personalizing your experience on our sites, etc.)</td>
        <td><ul>
          <li>Identification data (name, address, telephone, etc.)</li>
          <li>Electronic identification data (e-mail, IP-addresses, geolocation, website usage data, cookies, etc.)</li>
          <li>Financial characteristics (bank account number, credit card details, etc.)</li>
          <li>Personal characteristics of data subjects and, in some cases family members (age, gender, civil status, etc.</li>
          </ul>
        </td>
        <td><ul>
          <li>Performance of a contract between you and us and/or taking steps, at your request, to enter into such a contract</li>
          <li>Legal obligation to process your data</li>
          <li>Our legitimate interests in the proper administration of our website and business</li>
        </ul>
        </td>
        <td>
         Up to 10 years after date of last order or end of contract, unless longer period required by law or for litigation
		</td>	
      </tr>
      <tr>
        <td>Direct Consumer Sales (including provision of services, invoicing, customer support, account management, personalizing your experience on our sites, etc.)
        </td>
        <td>
          <ul>
        <li>
            Identification data (name, address, telephone, etc.)
        </li>
        <li>Electronic identification data (e-mail, IP-addresses, geolocation, website usage data, cookies, etc.)</li>
        <li>Financial characteristics (bank account number, credit card details, etc.)</li>
        <li>Personal characteristics of data subjects and, in some cases family members (age, gender, civil status, etc.)</li>
      </ul>
        </td>
        <td>
          <ul>
            <li>Performance of a contract between you and us and/or taking steps, at your request, to enter into such a contract </li>
            <li>Legal obligation to process your data</li>
            <li>Our legitimate interests in the proper administration of our website and business</li>
          </ul>
        </td>
        <td>
            Up to 10 years after date of last order or end of contract, unless longer period required by law or for litigation
		</td>	
      </tr>
      <tr>
        <td>Know your customer (including anti-fraud,  anti-money-laundering, etc.</td>
        <td>
          <ul>
            <li>Identification data (name, address, telephone, etc.)</li>
            <li>Electronic identification data (e-mail, IP-addresses, geolocation, website usage data, cookies, etc.) Financial characteristics (bank account number, credit card details, etc.)</li>
            <li>Personal characteristics of data subjects and, in some cases family members (age, gender, civil status, etc.)</li>
          </ul>
        </td>
        <td> <ul>
          <li>Performance of a contract between you and us and/or taking steps, at your request, to enter into such a contract </li>
          <li>Our legitimate interests in the proper administration of our website and business</li>
        </ul>
        </td>
        <td>
            Up to 10 years after date of last order or end of contract, unless longer period required by law or for litigation
		</td>
      </tr>
      <tr>
        <td>Direct marketing (including email marketing communications including promotions, marketing campaigns, surveys, etc.)**</td>
        <td><ul>
          <li>Identification data (name, address, telephone, etc.)</li>
          <li>Electronic identification data (e-mail, IP-addresses, geolocation, website usage data, cookies, etc.)</li>
        </ul>
        </td>
        <td>
            <ul>
                <li>With your consent/opt-in where required</li>
                <li>Performance of a contract between you and us and/or taking steps, at your request, to enter into such a contract </li>
                <li>Our legitimate interests in the proper administration of our website and business</li>
              </ul>
        </td>
        <td>
            Until you withdraw consent to direct marketing communications or your information is no longer needed for marketing purposes. 
        </td>
      </tr>
      <tr>
        <td>Supplier management (including accounting, etc.) </td>
        <td>
            <ul>
                <li>Identification data (name, address, telephone, etc.)</li>
                <li>Financial characteristics (bank account number, credit card details, etc.)</li>
                <li>Electronic identification data (e-mail, IP-addresses, geolocation, website usage data, cookies, etc.)</li>
                <li>Personal characteristics of data subjects and, in some cases family members (age, gender, civil status, etc.</li>
              </ul>
        </td>
        <td>
          <ul>
            <li>Performance of a contract between you and us and/or taking steps, at your request, to enter into such a contract</li>
            <li>Our legitimate interests in the proper administration of our website and business</li>
            <li>Legal obligation to process your data</li>
          </ul>
        </td>
        <td>
            Up to 10 years after date of last order or end of contract, unless longer period required by law or for litigation
        </td>
      </tr>
      <tr>
        <td>Maintaining the security of our people and assets  </td>
        <td>
            <ul>
                <li>Identification data (name, address, telephone, etc.)</li>
                <li>Electronic identification data (e-mail, IP-addresses, geolocation, website usage data, cookies, etc.)</li>
                <li>Financial characteristics (bank account number, credit card details, etc.)</li>
                <li>Personal characteristics of data subjects and, in some cases family members (age, gender, civil status, etc.)</li>
              </ul>
        </td>
        <td><ul>
            <li>Performance of a contract between you and us and/or taking steps, at your request, to enter into such a contract </li>
            <li>Legal obligation to process data securely</li>
            <li>Our legitimate interests in the proper administration of our website and business</li>
          </ul>
        </td>
        <td>
            Up to 10 years after date of last order or end of contract, unless longer period required by law or for litigation 
			</td>
      </tr>
      <tr>
        <td>Legal Compliance and Litigation management</td>
        <td>
          <ul>
            <li>Identification data (name, address, telephone, etc.)</li>
            <li>Electronic identification data (e-mail, IP-addresses, geolocation, website usage data, cookies, etc.)</li>
            <li>Financial characteristics (bank account number, credit card details, etc.)</li>
            <li>Personal characteristics of data subjects and, in some cases family members (age, gender, civil status, etc.)</li>
          </ul>
        </td>
        <td> <ul>
            <li>Processing necessary for the establishment, exercise or defense of legal claims or obligations.</li>
            <li>Performance of a contract between you and us and/or taking steps, at your request, to enter into such a contract </li>
            <li>Legal obligation to process data</li>
            <li>Our legitimate interests in the Proper administration of our website and business</li>
          </ul>
        </td>
        <td>Up to 10 years after legal compliance or other obligation ends or after final resolution of litigation  </td>
      </tr>
      <tr>
        <td>Product/service improvement & business intelligence</td>
        <td>
         <ul>
           <li>Identification data (name, address, telephone, etc.)</li>
           <li>Electronic identification data (e-mail, IP-addresses, geolocation, website usage data, cookies, etc.)</li>
           <li>Financial characteristics (bank account number, credit card details, etc.)</li>
           <li>Personal characteristics of data subjects and, in some cases family members (age, gender, civil status, etc.)</li>
         </ul>
        </td>
        <td> <ul>
            <li>Your consent</li>
            <li>Our legitimate interests in the proper administration of our website and business</li>            
          </ul>
        </td>
        <td>Up to 10 years from product discontinuation</td>
      </tr>
    </table>
    <br>
    <div id="asterisks">  * Retention periods vary depending on the type of document containing the Personal Data. For more information regarding the retention periods, please contact us at
      <a href="data-privacy-enquiry-form_en.html">Data Privacy Enquiry</a>.
    <br></br> ** If you wish to object to the use of your Personal Data for these purposes, see the “Your Rights Regarding Access To and Control Over Personal Data” section below.</div>
    <br>
    <div>We will keep your Personal Data while we are providing our products and services to you. Thereafter, we will keep your Personal Data for one of these reasons: (a) to respond to any questions, complaints or claims made by you or on your behalf; (b) to show that we treated you fairly; (c) for the proper administration of our business; and/or (d) to keep records as required by law.
        <br></br>
      We will not retain your Personal Data for longer than necessary for the purposes for which it was collected as set out in this Privacy Statement. In determining data retention periods, we take into consideration local laws, contractual obligations, our reasonable business requirements, and your expectations and requirements. When it is no longer necessary to retain your Personal Data, we will securely delete it or anonymize it.
    </div>

    <h2 id="4">4.	HOW WE SHARE YOUR PERSONAL DATA</h2>
	<div>Your Personal Data may be shared with others as described below: 
      <ul>
        <li>We may disclose your Personal Data to any member of our group of companies, affiliates, resellers, suppliers, and service providers, including advertising networks, as reasonably necessary for the purposes, and on the legal bases, set out in this Privacy Statement.</li>
        <li>We may disclose your Personal Data to our insurers and professional advisers as reasonably necessary for the purposes of obtaining or maintaining insurance coverage, managing risks, obtaining professional advice, or the establishment, exercise or defense of legal claims, whether in court proceedings or other legal proceedings.</li>
        <li>We may disclose your Personal Data with our payment services providers to complete financial transactions. We will share transaction data with our payment services providers only to the extent necessary for the purposes of processing your payments, refunding such payments and dealing with complaints and queries relating to such payments and refunds.</li>
        <li>In addition to the specific disclosures of Personal Data set out in this section, we may disclose your Personal Data: (a) where such disclosure is necessary for compliance with a legal obligation to which we are subject, including a subpoena, court order or search warrant; (b) enforce our Terms of Service, this Privacy Statement, or other contracts with 
            you, including investigation of potential violations thereof; (c) respond to claims regarding violations of third party rights; (d) respond to your requests for customer service; and/or (e) protect the rights, property or personal safety of Newell Brands and its employees, its agents and affiliates, its users and/or the public.  This includes exchanging information with other companies and organizations for fraud protection, spam/malware prevention, or in order to protect your vital interests or the vital interests of another natural person.
        </li>
      </ul>
    </div>
    <div>We reserve the right to transfer your Personal Data in the event of bankruptcy or a sale or transfer of all or a portion of our business or assets, an acquisition, merger or divestiture.</div>
    <br></br>    
   <div>Except as otherwise set forth herein, we do not sell, trade, rent or otherwise share your Personal Data with any third parties outside of Newell Brands or with our affiliated companies for monetary or other valuable consideration.  
   <br></br>
   <div>To California Residents: As of January 1, 2020, we do not sell Personal Data; however, prior to that date, some Personal Data of California consumers may have been sold, as that term is now construed under the California Consumer Privacy Act. If you are a California consumer who wishes to know if your Personal Data was sold, please submit a Request to Know as outlined in Section 7, below. Please note that Requests to Know under the California Consumer Privacy Act apply to the 12-month period preceding the date of the Request.</div>
  </div>  
    <h2 id="5">5. SENSITIVE PERSONAL DATA </h2>

    <div>Unless specifically requested, we ask that you not send us, and you not disclose, on or through the Sites or otherwise to us, Sensitive Personal Data (e.g., religion, ethnicity, political opinions, ideological or other beliefs, health, biometrics or genetic characteristics, criminal background, trade union membership, or administrative or criminal proceedings and sanctions) unless specifically requested by us or required by law.</div>
  
    <h2 id="6">6. INTERNATIONAL DATA TRANSFERS </h2>

    <div>Newell Brands is a global business based in the United States. Newell Brands, and our affiliated companies, have offices and facilities in worldwide locations including, the European Union/EEA, Latin America, Asia, Africa, etc.  Your Personal Data may be transferred to other Newell Brands entities, as well as to third party service providers who may process your Personal Data on our behalf, including e-marketing service providers, hosting providers, etc. </div>
<br>
    <di>Our customers and the users of our Sites may be located anywhere in the world.  The privacy laws in some countries may not provide protections equivalent to those in your country of residence, and your government may or may not deem such privacy protections adequate.  We may process your information in, or transfer your information to, the United States, third parties in the United States, or other countries. </di>
    <br></br>
    <div>For residents of EU/EEA countries, in the event of transfer of your Personal Data to a country which has been deemed to have an inadequate level of protection for Personal Data, we will implement safeguards, including selecting “Privacy Shield” certified processors or putting in place standard contractual clauses, in accordance with the EU requirements.  You can request more information about such measures by contacting us at
      <a href="data-privacy-enquiry-form_en.html">Data Privacy Enquiry</a>. 
  </div>

    <h2 id="7">7.	YOUR RIGHTS REGARDING ACCESS TO AND CONTROL OVER PERSONAL DATA</h2>
      <div>Your rights regarding your Personal Data depend on local law in the jurisdiction where you reside. </div> <br>
      <div> If you reside in the EU/EEA, you may have the right, after submitting a verified request to:</div>
      <ul>
        <li><u><i>Information and Access</i></u> - You may request to access your Personal Data, receive supplemental information about your Personal Data, and/or be provided with a copy of your Personal Data.</li>
        <li><u><i>Rectification</i></u> - You may request to rectify and/or update your inaccurate or out-of-date Personal Data.</li>
        <li><u><i>Erasure</i></u> - You may have the right to have your Personal Data erased. </li>
        <li><u><i>Restriction</i></u> - You may have the right to restrict the processing of your Personal Data. </li>
        <li><u><i>Object to Processing</i></u> - You may have the right to object to specific types of processing of your Personal Data.  </li>
        <li><u><i>Data Portability</i></u> - You may have the right to request a portable copy of your Personal Data. For example, Data Portability does not apply to paper records, and must not prejudice the rights of others, or sensitive company information.</li>
        <li><u><i>Right not to be subject to decisions based solely Automated Decision Making</i></u> -  You may have the right not to be subject to decisions based solely on automated processing (i.e., without human intervention).</li>
        <li><u><i>Right to lodge a complaint</i></u> - You may have the right to lodge a complaint with a supervisory authority in the country in which you reside, details of which can be provided on request by the Data Privacy Committee at Online Webform, or the lead supervisory authority for Newell Brands, namely The Information Commissioner’s Office of Wycliff House, Water Lane, Wilmslow, Cheshire SK9 6AF, United Kingdom, 
          <a href="https://ico.org.uk/">www.ico.org.uk</a>.
        </li>
      </ul>
    <div>If you wish to exercise any of these rights please <a href="data-subject-request-form_en.html">Data Subject Request</a>. </div><br>
    <div>Newell Brands may charge a reasonable fee if your request is not valid under applicable law, repetitive or excessive. We may also request information from you in order to verify your identify before you receive a response. </div>
    <br><div>If you reside in California, you may have the right, after submitting a verified request to:</div>
    <div>
    <ul>
      <li>Know the categories of personal information we collect about you; </li>
      <li>Know whether your personal information was sold or disclosed to third parties for a business purpose, including for direct marketing purposes by third parties, during the immediately preceding calendar year;</li>
      <li>Know the identity of the third parties that received your personal information for their direct marketing purposes during that calendar year;</li>
      <li>Deletion of your personal information maintained by us;</li>
      <li>Opt-out of the sale of your personal information;</li>
      <li>Access your personal information and to a portable copy of your data; </li>
      <li>Not be discriminated against for exercising your rights; and</li>
      <li>Notification of your rights.</li>
    </ul>
  </div>

  <div>
     If you wish to exercise any of these rights, please submit your request to <a href="data-subject-request-form_en.html">Data Subject Request </a>
     <br></br>(Please note, California consumers may exercise these verified rights effective January 1, 2020). 
     <br></br>To opt-out of the sale of your personal information click here: <a href="data-subject-request-form_en.html">DO NOT SELL MY PERSONAL INFORMATION</a> .
     <br></br><b><i>This Privacy Statement does not create, extend or modify any EU Data Subject rights, California consumer rights, or Newell Brands obligations, except as provided by the GDPR and CCPA.</i></b>
    </div>

    <h2 id="8">8.	EU/EEA DATA CONTROLLER AND DATA PROTECTION CONTACT INFORMATION </h2>
    <div>You can find a list and contact details of Newell Brands Data Controller entities and more information related to Supervisory Authorities, on the following page <a href="controllers_en.html">Newell Brands Data Controllers and Supervisory Authorities</a>. 
        <br></br>Where the Newell Brands Data Controller is located outside of the EU/EEA, the Controller’s EU Representative is specified on the <a href="controllers_en.html">Newell Brands Data Controllers and Supervisory Authorities page</a>.
        <br></br>Newell’s Data Protection Director may be contacted at:<br>
    </br><U><a href="data-privacy-enquiry-form_en.html">Data Privacy Enquiry </a></U><br></br>
      Attn: Data Protection Director
      <br>6655 Peachtree Dunwoody Road
      <br>Atlanta, Georgia 30328
    </div>
    <br>
    <div>For more information on the full extent of your rights or to exercise your rights, please contact our Data Protection Director using the contact information above or <a href="data-privacy-enquiry-form_en.html">Data Privacy Enquiry</a>, or the lead supervisory authority for Newell Brands, namely The Information Commissioner’s Office of Wycliff House, Water Lane, Wilmslow, Cheshire SK9 6AF, United Kingdom, <a href="https://ico.org.uk/">www.ico.org.uk</a>.</div>

    <h2 id="9">9.	PERSONAL DATA SUBMITTED BY CHILDREN </h2>

    <div>Some of the content on our Sites may be directed toward children under age 13. However, we do not knowingly collect or solicit personal information from children under age 13 without parental consent, unless permitted by law. If we become aware that we have collected personal information from a child under age 13 without parental consent or unless otherwise permitted by law, we will delete it in accordance with applicable law. If you believe that a child may have provided us with Personal Data without parental consent or otherwise not permitted by law, please contact us as detailed below in the “Contact Us” section.
    <br></br><u><i>California consumers </i></u>– We will not sell the information of California consumers who are 16 years old or younger without prior authorization. Our sites and services directed toward children will verify a user’s age and offer choices regarding the sale of personal information related to children aged 16 or younger.  If a California consumer is under 13 years old, a parent or guardian must consent to the sale of the minor’s personal information.  However, if the California consumer is between the ages of 13 and 16 years old, the minor may provide opt-in consent to the sale of their personal information.  Our sites directed toward children will ask for age verification and require appropriate consent.
    <br></br><u><i>EU Residents</i></u>– We do not collect or process personal data related to EU Data Subjects under the age of 16 without explicit consent from a parent or guardian.   Our sites directed toward children will ask for age verification and require consent as appropriate.   
    </div>
    
    <h2 id="10">10.	COOKIES, WEB BEACONS AND OTHER TRACKING TECHNOLOGIES </h2>
    <div>Our Sites use Cookies. Cookies are text files containing small amounts of information which are downloaded to your device when you visit a website.  Cookies are useful because they allow a website to recognize a user’s device.  Cookies help with a variety of website functions including allowing users to “log in” to websites (making your navigation of websites more efficient), remembering your preferences (such as language), and generally improving your experience on our Sites.  Cookies can also help ensure that the advertisements and content you see online are more relevant to you and your interests.  We may also include web beacons in marketing e-mail messages or our newsletters in order to determine whether messages have been opened and links contained within clicked on.
    </div><br>
    <div>Some of our business partners set web beacons and Cookies on our site. In addition, third-party social media buttons may log certain information such as your IP address, browser type and language, access time, and referring website addresses.  Further, if you are logged in to those social media sites, they may also link such collected information with your profile information on that site.
        <br></br>If you would like to opt-out of cookies, visit www.aboutcookies.org or www.allaboutcookies.org for further information.  These websites also tell you how to set your browser not to accept cookies and how to remove cookies from your browser.  However, in a few cases, some of the features on our Sites may not function as a result.  More information on our use of cookies can be found in our 
        <a href="cookie_en.html">Cookies Policy</a>.
    </div>

    <h2 id="11">11.	SECURITY</h2>
    <div>
        There is no perfect security.  We have implemented appropriate and commercially reasonable organizational, technical and administrative safeguards to protect Personal Data.  You are responsible for maintaining the secrecy of any credentials that can be used to access any account or service with Newell. If you should suspect any unauthorized activity please report this to us immediately.  You can report suspicious activity or other concerns by contacting us at:
        <br></br><a href="data-privacy-enquiry-form_en.html">Data Privacy Enquiry</a>
    </div>

    <h2 id="12">12.	LINKS TO THIRD PARTY SITES</h2>
    <div>
        We may collect your information from third parties sites.  This includes when you log in through a third party site such as Facebook, Snapchat, Instagram, LinkedIn, Twitter, etc. We may also disclose information collected from third parties to customize ads and to manage and facilitate messaging on third party sites.  Please note that when you unsubscribe from our marketing email there may be a delay in the discontinuation of our ads directed to you on third party sites.  
    </div>
    <h2 id="13">13.	DO NOT TRACK</h2>
    <div>
        Newell does not track its customers over time and across third party websites to provide targeted advertising and therefore does not respond to Do Not Track (DNT) signals.  However, some third party sites do keep track of your browsing activities when they serve you content, which enables them to tailor what they present to you.  If you are visiting such sites, you may be able to set the DNT signal on your browser so that third parties (particularly advertisers) know you do not want to be tracked.  
    </div>

    <h2 id="14">14.	USING MOBILE AND OTHER DEVICES</h2>
    <div>
        Newell, our affiliates and third parties that we have engaged, may collect and store unique identifiers matched to your mobile device in order to deliver customized ads or content while you use our Sites, navigate the internet, to deliver location-based services and advertising, or to identify you in a unique manner across devices or browsers.  In order to customize these ads or content, we, or third parties, may collect Personal Data, e.g., your email address, or data passively collected from you such as your device identifier, location, or IP address.  Most mobile devices allow you to turn off location services.  
        <br>By adjusting the privacy and security settings on your device, you can manage how your mobile device and mobile browser share information, as well as how your mobile browser handles Cookies and other tracking identifiers.  Please refer to instructions provided by your mobile service provider or the manufacturer of your device to learn how to adjust your settings.
    </div>

    <h2 id="15">15.	EMAIL MARKETING COMMUNICATIONS </h2>
    <div>
        If you are an existing customer or, if required by applicable law, you have provided consent or have not otherwise directed us not to send you marketing messages, we may send you marketing messages via email and/or mail to keep you updated on our products and services.
        <br></br>You can stop receiving marketing messages from us at any time through the following methods:
        <ul>
          <li>Through your customer account settings/preferences within “My Account”;</li>
          <li>By clicking on the ‘unsubscribe’ link in any email communication we send you</li>
          <li>By contacting us at <a href="data-subject-request-form_en.html">Data Subject Request </a> </li>
        </ul>
        Once one of these methods is completed, we will honor your request and update your profile to ensure that you don’t receive further marketing messages.
        <br></br>We will process opt-out/unsubscribe requests as quickly as possible, and in most cases, within 10 days.  However, Newell Brands is made up of a complex web of many inter-connecting companies and service providers so it may take time for all our systems to reflect these updates. We ask for your understanding that during this period you may still get messages from us while your request is being processed.  
        <br></br>Please note that the discontinuation of marketing messages will not stop any service communications such as order updates.
    </div>

    <h2 id="16">16.	TEXT MESSAGING </h2>
    <div>
        Our Sites may give you the choice to opt-in to receiving text messages and alerts on the mobile phone number(s) you have shared with us.  Once you have opted-in, we may send you text messages regarding your account, to investigate or prevent fraud, to alert you in the event of an emergency, etc.  We may send you text messages and alerts using autodialed technology.  Also, from time to time, we may run certain campaigns that provide you with the ability to send a text message to a specific short code and provide an opt-in to receive future text messages.  Your opt-in request provides us with your mobile phone number.  If you choose to opt-in, you agree that we may, from time to time, send messages to the mobile device associated with that number about new products, promotions, services, or offers. 
        <br></br>You may choose to opt-out from our text messages and alerts at any time by replying “STOP” to the message you received.  Once you opt-out, you will not receive any additional text messages via your mobile phone.  Please keep in mind that if you opt-out of receiving text messages and alerts we may not be able to contact you with important messages regarding your account or transactions with us.  However, if there is an emergency or account question, we will make every attempt to contact you in other ways such as by email or on a landline phone.  You do not have to opt-in to text messages and alerts to use our Sites and services.  If you opt-in, standard text messaging charges may apply.  For more information regarding our text messaging and alerts, please contact us at <a href="data-privacy-enquiry-form_en.html">Data Privacy Enquiry</a>.
    </div>

    <h2 id="17">17.	CHANGES TO OUR PRIVACY STATEMENT  </h2>
    <div>
        We reserve the right, at our sole discretion, to change, modify, add, or remove portions from this privacy policy at any time.  We shall notify any such changes in according with applicable data protection legislation.  
        <br></br>Any material changes to this Privacy Policy will be posted on this webpage. It is your responsibility to review our Privacy Policy each time you provide Personal Data to us as the Privacy Policy may have changed since the last time you used our Sites or services.
    </div>

    <h2 id="18">18.	CONTACT US</h2>
      <div>
          If you have questions about this Privacy Policy or Newell’s and practices, please contact us at:<a href="data-privacy-enquiry-form_en.html">Data Privacy Enquiry</a>.
      </div>
    <h2 id="19">19. WHAT’S NEW? </h2>
      <div>Effective November 15, 2019, we updated and harmonized portions of our Privacy Statement.  Here is a summary of some of the changes:  
        <ul>
          <li>We will now use the term “Sites” to describe our website, mobile apps, etc</li>
          <li>We clarify the types of Personal Data we collect about you and the ways we collect this information.</li>
          <li>We describe your rights of access to and control over your Personal Data.  </li>
          <li>We provide updated contact information.</li>
          <li>We describe how we may use and share your Personal Data</li>
          <li>We clarify the types of Personal Data we collect about you and the ways we collect this Personal Data. We collect Personal Data in many ways, including from third party sites, social media sites, and advertising networks.</li>
          <li>We describe how we share and use your Personal Data. </li>
          <li>We describe our use of email communications and text messaging and how to opt out and/or control these communications.</li>
          <li>We notify users as to their rights under the GDPR and California Consumer Privacy Act where applicable.</li>
          <li>We clarify that your Personal Data may be transferred across international borders for processing and other uses.</li>
          <li>We clarify how we share your Personal Data with our suppliers, third party vendors and referring sites.</li>
          <li>We include additional information on Do Not Track signals.</li>
          <li>For California Residents: Disclosure that some personal data may have been sold prior to Jan. 1 2020.</li>     
        </ul>
      </div>  ';

      $lang['cookie_txt'] = '<div id="heading">
    <h1> NEWELL BRANDS COOKIES POLICY </h1>
    <h3> Last modification: v2.1 December 1 2019 </h3>
  </div>
  <div id="container">
    <div>
      We strive to honor the privacy and security of our users, customers and suppliers, as well as their representatives, in relation to all products, services, applications and websites provided by Newell Brands Inc. and our affiliates (“Newell Brands”). This Cookies Policy explains what Cookies are, how we use Cookies, how third-parties we may partner with may use Cookies on our Site, your choices regarding Cookies and further information about Cookies.
    </div>
  <br>
    <div>
      Newell Brands websites and digital media, including apps, mobile apps, social media accounts, internet and wifi-enabled devices, etc. (“Sites”) may use cookies and similar technologies (such as pixel tags, web beacons, etc.) to allow us and third parties to obtain information about your visits to the Sites, including to analyze your visiting patterns.  We may use this information to process your transactions or requests, and, where applicable, deliver online and mobile advertisements, messages and content from us, as well as third parties, that are specific to your interests.
    </div>
  <br>
    <div>
      <b>What Are Cookies?</b> Cookies are small text files stored on your computer or other devices.  They are widely used to make websites work or improve the efficiency of a website. Cookies help with website functionality and user experience because websites can read and write to these text files, enabling the site to recognize you and remember important information that will make your use of the site more convenient (i.e., by remembering your language or font preference settings). 
    </div>
  <br>
    <div>
      Functionality tags/files do not require your consent. For analytical and other tags/files, however, where applicable and required by law we request your consent before placing them on your device or allow you to opt out of our use of these types of Cookies. You can give your consent by clicking on the appropriate button on the Cookies banner displayed to you when you visit our Sites or using the preference center in our cookie management tool.
  </div>

  <h2> Cookies Use By Our Sites </h2>
    <div>Our Sites may use the following Cookies: </div>
    <br>
    <div>
      <b><u>Essential Cookies</u>.</b> These are Cookies required for the operation of our Sites. Essential Cookies may enable you to, among other things, log in to secure areas of our Sites, navigate our Sites, add products to your bag, checkout, make use of our e-billing services, etc.
    </div>
<br>
    <div>
      <b><u>Analytical/Performance Cookies</u>.</b> These Cookies allow us to recognize and count the number of visitors and to see how visitors navigate our Sites. We may use these Cookies to, among other things, measure how our customers are using the site to improve functionality and optimize the shopping experience for you. This helps us improve the way our Sites work, for example, by ensuring that users are finding what they are looking for easily.
  </div>
<br>
    <div>
      <b><u>Functionality/User Preference Cookies</u>:</b> These Cookies are used to recognize you when you return to our Sites. We may use these to remember the language and currency that you use onsite and to help streamline your shopping experience. These Cookies enable us to personalize our content for you, greet you by name and remember your preferences.
  </div>
<br>
    <div>
      <b><u>Marketing/Targeting/Social Sharing Cookies</u>.</b> These Cookies record your visits to our Sites, the pages you have visited and the links you have followed. We will use this information to make our Sites, and the advertising displayed on them, more relevant to your interests.  Where applicable and as allowed by law, we may also share this information with third parties for this purpose.  Please note that third parties (including, for example, advertising networks and providers of external services like web traffic analysis services) may also use Cookies, over which we have no control.
    </div>
<br>

  <h2> How to Manage, Disable or Opt-Out of Cookies Used By Our Sites </h2> 
  <div>
    Internet browsers allow you to change your Cookies settings for various reasons, including to block certain kinds of Cookies or files. You can therefore block certain Cookies by activating the applicable settings on your browser. However, if you use your browser settings to block all Cookies, you may not be able to access all or parts of our Sites because some may be functionality or essential Cookies. For further information about deleting or blocking cookies, please visit:<a href="http://www.allaboutcookies.org/manage-cookies/"> http://www.allaboutcookies.org/manage-cookies/.</a> You may refuse, accept or remove Cookies from our Sites at any time by activating or accessing the Cookie preferences setting on your browser. For further information about changing your Cookies preferences or disabling Cookies altogether, refer to <br>
  <a href="http://www.allaboutcookies.org/manage-cookies">http://www.allaboutcookies.org/manage-cookies</a> for information on commonly used browsers. Please be aware that if Cookies are disabled or removed, you may not be able to access all or parts of our Sites, or features may not operate as intended. 
  </div>

<br>
  <div>
    To opt out of being tracked by Google Analytics across all websites visit <a href="http://tools.google.com/dlpage/gaoptout">http://tools.google.com/dlpage/gaoptout.</a>
  </div>

  <h2> Further Information </h2>
	
    <div>You can find more information about cookies and similar tags/files at the following addresses:
      <br> - <a href=" http://www.allaboutcookies.org/"> http://www.allaboutcookies.org/;</a>
      <br> - <a href="http://www.youronlinechoices.eu/">http://www.youronlinechoices.eu/</a> (a guide to behavioral advertising and online privacy, produced by the internet advertising industry).

      <br><br>
      If you have questions or concerns or require further information, do not hesitate to contact us at <a href="data-privacy-enquiry-form_en.html">Data Privacy Enquiry</a>.
  </div>';

  $lang['condiciones_txt'] = '<div role="main" id="maincontent">

    
    <p style="margin: 0in 0in 10pt 40px; text-align: justify;"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif"><b><span style="font-size:12.0pt"><span style="line-height:115%">Terms of Use:</span></span></b></span></span></span></p>

<p style="margin: 0in 0in 10pt 40px; text-align: justify;"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="line-height:115%">PLEASE READ THESE TERMS OF USE CAREFULLY BEFORE USING OR ACCESSING ANY PAGES IN THIS WEBSITE. By using or accessing this website you signify your acknowledgment and assent to the terms and conditions of use set forth below. &nbsp;This website is owned and operated by Newell Brands Inc. and its subsidiaries and affiliates (collectively “Newell Brands,” “we,” or “us”). &nbsp;If you do not agree to these terms of use, please do not use this website. Newell Brands is free to revise these terms of use at any time by updating this posting, and your use after such change signifies your acceptance of the changed terms. Please check these terms of use periodically for changes. Questions concerning this website or its operation should be directed to the contact points set forth at the end of these terms of use.</span></span></span></span></span></p>

<p style="margin: 0in 0in 10pt 40px; text-align: justify;"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="line-height:115%">In connection with viewing and using this website, you are permitted to temporarily download one copy of the materials posted on this website onto a single computer for only your personal, non-commercial use. Except as otherwise specifically provided elsewhere on this website, redistribution, retransmission, republication or commercial exploitation of the contents of this website are expressly prohibited without the written consent of Newell Brands and any copyright owner from whom we have obtained a license. Requests for such permission should be made to copyright@newellco.com or to the contact specified below. All rights not expressly granted herein are reserved. Downloading of any information, content or images from this website does not transfer any right or ownership of such information, content or images to you, and such information, content or images may be used solely in accordance with these terms of use. You may not mirror or archive any part of this site or any material contained on this site on any server or computer without Newell Brands written permission.</span></span></span></span></span></p>

<p style="margin: 0in 0in 10pt 40px; text-align: justify;"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif"><b><span style="font-size:12.0pt"><span style="line-height:115%">Contents and Hyperlinks</span></span></b></span></span></span></p>

<p style="margin: 0in 0in 10pt 40px; text-align: justify;"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="line-height:115%">This website may contain hyperlinks to third party websites, and those websites are the sole responsibility of such independent third parties, and use thereof is solely at your own risk. Newell Brands has no control over the content or policies of such third-party websites, and we are not responsible for (and under no circumstances shall be liable for) the contents, accuracy or reliability of any websites hyperlinked to this website. Those who choose to access information from this website (including any information obtained through any hyperlink) are solely responsible for the compliance of such information with any applicable law.&nbsp; THIS PROVISION IS VOID, INAPPLICABLE OR UNENFORCEABLE IN THE STATE OF NEW JERSEY.&nbsp; </span></span></span></span></span></p>

<p style="margin: 0in 0in 10pt 40px; text-align: justify;"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif"><b><span style="font-size:12.0pt"><span style="line-height:115%">User Content</span></span></b></span></span></span></p>

<p style="margin: 0in 0in 10pt 40px; text-align: justify;"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="line-height:115%">Newell Brands is pleased to hear from its customers; however, we do not accept or consider any creative ideas, suggestions or other materials related to products, services or marketing except in compliance with the procedures outlined for idea submissions outlined elsewhere in this website. Please do not send us any original creative materials such as product ideas or suggestions except in compliance with such procedures. Anything you disclose or offer to us by or through this website ("Communications"), including e-mails to Newell Brands or postings on interactive portions of this website, shall be deemed and shall remain the property of Newell Brands. If you send us such Communications, you are providing it to us on a NON-CONFIDENTIAL BASIS, and we will have no obligation to keep such information secret, to refrain from using such information, or to compensate you for the receipt or use of such Communications. Newell Brands is free to use, for any purpose whatsoever, any Communications, including but not limited to publishing, or developing, manufacturing, and marketing products using such Communications. By uploading or otherwise providing any Communications to this website or Newell Brands, you hereby grant Newell Brands, to the extent you retain any rights, the unlimited, perpetual right to reuse, redistribute, modify and create derivative works from such Communications for any purpose and in any media without compensation, and you warrant that all "moral rights" in uploaded Communications have been waived.&nbsp; By submitting Communications to us through this website, through e-mail, or through any means other than through the procedures outlined elsewhere in this website, you hereby RELEASE Newell Brands from any liability under any legal theory in connection with the use, modification, sale, or disclosure of any Communications. THIS PROVISION IS VOID, INAPPLICABLE OR UNENFORCEABLE IN THE STATE OF NEW JERSEY.&nbsp; </span></span></span></span></span></p>

<p style="margin: 0in 0in 10pt 40px; text-align: justify;"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif"><b><span style="font-size:12.0pt"><span style="line-height:115%">Interactive Areas and Code of Acceptable Conduct</span></span></b></span></span></span></p>

<p style="margin: 0in 0in 10pt 40px; text-align: justify;"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="line-height:115%">Newell Brands does not ordinarily filter, censor, edit or regulate information and content provided by third parties on this website, including any such information provided in interactive areas, and we neither endorse nor are responsible for (and under no circumstances shall be liable for) the contents, accuracy or reliability of such information and content.&nbsp; THIS PROVISION IS VOID, INAPPLICABLE OR UNENFORCEABLE IN THE STATE OF NEW JERSEY.&nbsp; </span></span></span></span></span></p>

<p style="margin: 0in 0in 10pt 40px; text-align: justify;"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="line-height:115%">When participating in interactive portions of this website, you represent that you have proper right and authorization to use any information or content you upload or post and agree to abide by the following code of acceptable conduct:</span></span></span></span></span></p>

<ol>
	<li style="text-align:justify; margin:0in 0in 0.0001pt 0.5in; margin-top:0in; margin-right:0in">
	<p style="margin-left: 40px;"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"></span><span style="font-size:12.0pt"><span style="line-height:115%">You will not upload or otherwise provide infringing, defamatory, obscene, pornographic, threatening, abusive, illegal or otherwise improper content. </span></span></span></span></span></p>
	</li>
	<li style="text-align:justify; margin:0in 0in 0.0001pt 0.5in; margin-top:0in; margin-right:0in">
	<p style="margin-left: 40px;"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"></span><span style="font-size:12.0pt"><span style="line-height:115%">You will not upload viruses or harmful components.</span></span></span></span></span></p>
	</li>
	<li style="text-align:justify; margin:0in 0in 0.0001pt 0.5in; margin-top:0in; margin-right:0in">
	<p style="margin-left: 40px;"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"></span><span style="font-size:12.0pt"><span style="line-height:115%">You will not use the website to further any illegal purpose or to violate the rights of any party.</span></span></span></span></span></p>
	</li>
	<li style="text-align:justify; margin:0in 0in 10pt 0.5in; margin-top:0in; margin-right:0in">
	<p style="margin-left: 40px;"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"></span><span style="font-size:12.0pt"><span style="line-height:115%">You will not upload or otherwise provide content with a commercial purpose or attempt to solicit funds or advertise goods and services.</span></span></span></span></span></p>
	</li>
</ol>

<p style="margin: 0in 0in 10pt 40px; text-align: justify;"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="line-height:115%">Newell Brands will assist law-enforcement officials investigating illegal activity or violations of these terms of use.</span></span></span></span></span></p>

<p style="margin: 0in 0in 10pt 40px; text-align: justify;"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif"><b><span style="font-size:12.0pt"><span style="line-height:115%">Products, Services and Software</span></span></b></span></span></span></p>

<p style="margin: 0in 0in 10pt 40px; text-align: justify;"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="line-height:115%">Downloading software from this website does not give you title to such software, including any files, data and images incorporated in or associated with the software. Your use of any such software shall be only in accordance with the license agreement that is included with the software or presented upon download of such software. Software available on this website is copyrighted by Newell Brands or its owner. Software may not be copied, redistributed or placed on any server for further distribution. You may not sell, modify, decompile, disassemble, or otherwise reverse engineer the software. </span></span></span></span></span></p>

<p style="margin: 0in 0in 10pt 40px; text-align: justify;"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="line-height:115%">A description or reference to a product, service or publication on this website (including any description or reference via hyperlink) does not imply endorsement by Newell Brands of that product, service or publication. Products and software offered through this website shall be warranted, if at all, through the written license or warranty provided in connection with such product or software.</span></span></span></span></span></p>

<p style="margin: 0in 0in 10pt 40px; text-align: justify;"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif"><b><span style="font-size:12.0pt"><span style="line-height:115%">Notice and Procedure for making Claims of Copyright Infringement</span></span></b></span></span></span></p>

<p style="margin: 0in 0in 10pt 40px; text-align: justify;"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="line-height:115%">Pursuant to Title 17, United States Code, Section 512(c)(2), notifications of claimed copyright infringement should be sent to our Designated Agent. See our Procedure for Copyright Infringement Claims.</span></span></span></span></span></p>

<p style="margin: 0in 0in 10pt 40px; text-align: justify;"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif"><b><span style="font-size:12.0pt"><span style="line-height:115%">No Representations or Warranties</span></span></b></span></span></span></p>

<p style="margin: 0in 0in 10pt 40px; text-align: justify;"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="line-height:115%">The pages on this website may contain technical inaccuracies, outdated information and typographical errors. To the extent permitted by applicable law, THIS WEBSITE IS PROVIDED "AS IS." NEWELL BRANDS DOES NOT MAKE ANY WARRANTY OF ANY KIND, WHETHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO ANY WARRANTY OF FITNESS FOR A PARTICULAR PURPOSE OR MERCHANTABILITY, NOR DOES IT IN ANY WAY GUARANTEE THE QUALITY, DATA CONTENT, ARTISTIC WORTH, OR LEGALITY OF INFORMATION, CONTENT, GOODS OR SERVICES THAT ARE TRANSFERRED, RECEIVED, PURCHASED, OR OTHERWISE MADE AVAILABLE OR OBTAINED BY WAY OF THIS WEBSITE. WE DO NOT WARRANT THAT THIS WEBSITE WILL BE ERROR-FREE OR THAT DEFECTS WILL BE CORRECTED. APPLICABLE LAW MAY NOT ALLOW THE EXCLUSION OF IMPLIED WARRANTIES, SO THE ABOVE EXCLUSION MAY NOT APPLY TO YOU.</span></span></span></span></span></p>

<p style="margin: 0in 0in 10pt 40px; text-align: justify;"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="line-height:115%">Newell Brands makes no representations or warranties that this website is free of defects, viruses or other harmful components. We shall not be responsible for any damages or loss that may result from the hacking or infiltration of this website or Newell Brands computer systems. YOU HAVE THE SOLE RESPONSIBILITY FOR ADEQUATE PROTECTION AND BACKUP OF DATA AND/OR EQUIPMENT USED IN CONNECTION WITH THIS WEBSITE AND YOU AGREE TO HOLD NEWELL BRANDS HARMLESS FROM, AND YOU COVENANT NOT TO SUE US FOR, ANY CLAIMS BASED ON USE OF THIS WEBSITE, INCLUDING CLAIMS FOR LOST DATA, WORK DELAYS OR LOST PROFITS RESULTING FROM USE OF MATERIALS OR CONTENT FROM THIS WEBSITE. &nbsp;THIS PROVISION IS VOID, INAPPLICABLE OR UNENFORCEABLE IN THE STATE OF NEW JERSEY. </span></span></span></span></span></p>

<p style="margin: 0in 0in 10pt 40px; text-align: justify;"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif"><b><span style="font-size:12.0pt"><span style="line-height:115%">Limitations of Liability</span></span></b></span></span></span></p>

<p style="margin: 0in 0in 10pt 40px; text-align: justify;"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="line-height:115%">UNDER NO CIRCUMSTANCES, INCLUDING NEGLIGENCE, SHALL NEWELL BRANDS BE LIABLE FOR ANY SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES OR LOST PROFITS THAT RESULT FROM THE DISTRIBUTION OR USE OF, OR THE INABILITY TO USE, THE CONTENT OR MATERIALS ON THIS WEBSITE, EVEN IF WE HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. APPLICABLE LAW MAY NOT ALLOW THE LIMITATION OR EXCLUSION OF LIABILITY OR INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION MAY NOT APPLY TO YOU. IN NO EVENT SHALL NEWELL BRANDS’ TOTAL LIABILITY TO YOU FOR ALL DAMAGES, LOSSES, AND CAUSES OF ACTION (WHETHER IN CONTRACT, TORT OR OTHERWISE) EXCEED THE AMOUNT PAID BY YOU, IF ANY, FOR ACCESSING THIS WEBSITE.</span></span></span></span></span></p>

<p style="margin: 0in 0in 10pt 40px; text-align: justify;"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="line-height:115%">THIS PROVISION IS VOID, INAPPLICABLE OR UNENFORCEABLE IN THE STATE OF NEW JERSEY.</span></span></span></span></span></p>

<p style="margin: 0in 0in 10pt 40px; text-align: justify;"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif"><b><span style="font-size:12.0pt"><span style="line-height:115%">Choice of Law</span></span></b></span></span></span></p>

<p style="margin: 0in 0in 10pt 40px; text-align: justify;"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="line-height:115%">Newell Brands controls and manages this website from its facilities in the State of Georgia in the United States of America. Unless otherwise stated, materials and content on this website are presented solely for promoting products and services in the United States of America. Information published on this website may contain references to products, programs and services that are not announced or available in your country or region. We make no representation that such information, products, programs or services referenced on this website are legal, available or appropriate in your country or region. &nbsp;&nbsp;These terms of use shall be governed by and construed in accordance with the laws of the State of Georgia and the United States of America, without giving effect to any principles of conflicts of law. &nbsp;&nbsp;THIS PROVISION IS VOID, INAPPLICABLE OR UNENFORCEABLE IN THE STATE OF NEW JERSEY.</span></span></span></span></span></p>

<p style="margin: 0in 0in 10pt 40px; text-align: justify;"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif"><b><span style="font-size:12.0pt"><span style="line-height:115%">Jurisdiction</span></span></b></span></span></span></p>

<p style="margin: 0in 0in 10pt 40px; text-align: justify;"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="line-height:115%">You and Newell Brands irrevocably consent to the exclusive jurisdiction of the courts located in Georgia in connection with any action arising out of or related to these terms of use or their subject matter. You and Newell Brands waive any objection based on lack of personal jurisdiction, place of residence, improper venue or forum non convenient in any such action.&nbsp; THIS PROVISION IS VOID, INAPPLICABLE OR UNENFORCEABLE IN THE STATE OF NEW JERSEY.</span></span></span></span></span></p>

<p style="margin: 0in 0in 10pt 40px; text-align: justify;"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif"><b><span style="font-size:12.0pt"><span style="line-height:115%">Disputes</span></span></b></span></span></span></p>

<p style="margin: 0in 0in 10pt 40px; text-align: justify;"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="line-height:115%">PLEASE READ THIS SECTION CAREFULLY BECAUSE IT REQUIRES YOU TO ARBITRATE DISPUTES AND IT LIMITS THE MANNER IN WHICH YOU CAN SEEK RELIEF.&nbsp; </span></span><span style="font-size:12.0pt"><span style="line-height:115%">Any dispute or claim relating in any way to these to the Terms of Use, this website, or any products or services sold or distributed by or through this website, will be resolved by binding arbitration, rather than in court, except that you may assert claims in small claims court if your claims qualify. The Federal Arbitration Act and federal arbitration law apply to the Terms of Use. We each agree that any dispute resolution proceedings will be conducted only on an individual basis and not in a class, consolidated or representative action. If for any reason a claim proceeds in court rather than in arbitration we each waive any right to a jury trial. We also both agree that you or we may bring suit in court to enjoin infringement or other misuse of intellectual property rights.&nbsp; THIS PROVISION IS VOID, INAPPLICABLE OR UNENFORCEABLE IN THE STATE OF NEW JERSEY.</span></span></span></span></span></p>

<p style="margin: 0in 0in 10pt 40px; text-align: justify;"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif"><b><span style="font-size:12.0pt"><span style="line-height:115%">Trademarks and Copyrights</span></span></b></span></span></span></p>

<p style="margin: 0in 0in 10pt 40px; text-align: justify;"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="line-height:115%">Nothing on this website shall be construed as conferring any license under any intellectual property right, including any right in the nature of trademark or copyright, of Newell Brands or any third party, whether by estoppel, implication, or otherwise. All trademarks and trade names are the property of their respective owners. </span></span></span></span></span></p>

<p style="margin: 0in 0in 10pt 40px; text-align: justify;"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="line-height:115%">Except as otherwise noted, Newell Brands and/or its subsidiaries is the owner of all trademarks and service marks on this website, whether registered or not. All registered trademarks are registered in the United States of America (or other applicable jurisdictions).</span></span></span></span></span></p>

<p style="margin: 0in 0in 10pt 40px; text-align: justify;"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif"><b><span style="font-size:12.0pt"><span style="line-height:115%">Securities and Investment</span></span></b></span></span></span></p>

<p style="margin: 0in 0in 10pt 40px; text-align: justify;"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="line-height:115%">This website and the information contained or referred to herein does not constitute an offer or a solicitation of an offer for the purchase or sale of any securities. </span></span></span></span></span></p>

<p style="margin: 0in 0in 10pt 40px; text-align: justify;"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="line-height:115%">This website may contain information and press releases about and by Newell Brands. While information prepared by us was believed to be accurate as of the date so prepared, we disclaim any duty or obligation to update such information or to verify the accuracy of information prepared by others. Any statements in this site that are not historical facts, including but not limited to plans, projections, objectives, goals, strategies, future events or performance and underlying assumptions, are forward-looking statements as provided in the rules and regulations of the Securities Act of 1933, Securities Exchange Act of 1934, and the Private Securities Litigation Reform Act of 1995. Such statements are intended to fit within the "safe harbor" for forward-looking information and are subject to material risk factors which may or may not be disclosed herein. Statements or phrases that use such words as "believes," "anticipates," "plans," "may," "hopes," "can," "will," "expects," "estimates," "predicts," "is designed to," "with the intent," "potential," and similar expressions commonly indicate forward-looking statements, but in their absence do not mean that a statement is not forward-looking. Any forward-looking statements contained herein involve risks and uncertainties, including but not limited to general economic and currency conditions, various conditions specific to Newell Brands business and industry, market demand, competitive factors, supply constraints, technology factors, government and regulatory actions, Newell Brands’ accounting policies, future trends, and other risks which are detailed in Newell Brands Securities and Exchange Commission filings.</span></span></span></span></span></p>

<p style="margin: 0in 0in 10pt 40px; text-align: justify;"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif"><b><span style="font-size:12.0pt"><span style="line-height:115%">Severability</span></span></b></span></span></span></p>

<p style="margin: 0in 0in 10pt 40px; text-align: justify;"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="line-height:115%">If any provision of these terms of use shall be deemed unlawful, void, or for any reason unenforceable, then that provision shall be deemed severable from the remaining terms of use and shall not affect the validity and enforceability of any remaining provisions.</span></span></span></span></span></p>

<p style="margin: 0in 0in 10pt 40px; text-align: justify;"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif"><b><span style="font-size:12.0pt"><span style="line-height:115%">Privacy</span></span></b></span></span></span></p>

<p style="margin: 0in 0in 10pt 40px; text-align: justify;"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="line-height:115%">Please see our Privacy Policy for information regarding the collection and use of personal information from this website. </span></span></span></span></span></p>

<p style="margin: 0in 0in 10pt 40px; text-align: justify;"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="line-height:115%">Despite any representations concerning privacy, Newell Brands reserves the right to disclose without notice to you any information in its possession if required to do so by law or upon a good-faith belief that such action is necessary to comply with the law, to protect or defend our rights or property, or to respond to an emergency situation. Specific areas or pages of this website may include additional or different terms relating to the use of personal information collected from such areas or pages. </span></span></span></span></span></p>

<p style="margin: 0in 0in 10pt 40px; text-align: justify;"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif"><b><span style="font-size:12.0pt"><span style="line-height:115%">General</span></span></b></span></span></span></p>

<p style="margin: 0in 0in 10pt 40px; text-align: justify;"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="line-height:115%">These terms of use represent the entire understanding relating to the use of this website and prevail over any prior or contemporaneous, conflicting or additional communications. </span></span></span></span></span></p>

<p style="margin: 0in 0in 10pt 40px; text-align: justify;"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="line-height:115%">Any unauthorized access, modification or change of any information, or any interference with the availability of or access to this website is strictly prohibited. Newell Brands reserves all legal rights and remedies available to it and this disclaimer shall in no way be deemed a limitation or waiver of any other rights Newell Brands may have.</span></span></span></span></span></p>

<p style="margin: 0in 0in 10pt 40px; text-align: justify;"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="line-height:115%">Specific areas or pages of this website may include additional or different terms relating to the use of this website. In the event of a conflict between such terms and these terms of use such specific terms shall control.</span></span></span></span></span></p>

<p style="margin: 0in 0in 10pt 40px; text-align: justify;"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="line-height:115%">Unless otherwise indicated, all material on this site © 2016 Newell Brands Inc. </span></span></span></span></span></p>

<p style="margin: 0in 0in 10pt 40px; text-align: justify;"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="line-height:115%">Material on this site may be subject to one or more U.S. Patents used under license from Applied Interact LLC:&nbsp; Numbers 5,128,752, 5,227,874, 5,249,044, 5,283,734, 5,508,731. </span></span></span></span></span></p>

<p style="margin: 0in 0in 10pt 40px; text-align: justify;"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="line-height:115%">All rights reserved.</span></span></span></span></span></p>

<p style="margin: 0in 0in 10pt 40px; text-align: justify;"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="line-height:115%">Contact Information:</span></span></span></span></span></p>

<p style="margin: 0in 0in 10pt 40px; text-align: justify;"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="line-height:115%">Questions about these terms and conditions may be directed to:</span></span></span></span></span></p>

<p style="margin: 0in 0in 10pt 40px; text-align: justify;"><span style="font-size:11pt"><span style="line-height:normal"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt">E-Business Team</span></span></span></span></p>

<p style="margin: 0in 0in 10pt 40px; text-align: justify;"><span style="font-size:11pt"><span style="line-height:normal"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt">Newell Brands Inc.</span></span></span></span></p>

<p style="margin: 0in 0in 10pt 40px; text-align: justify;"><span style="font-size:11pt"><span style="line-height:normal"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt">6655 Peachtree Dunwoody Road</span></span></span></span></p>

<p style="margin: 0in 0in 10pt 40px; text-align: justify;"><span style="font-size:11pt"><span style="line-height:normal"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt">Atlanta, GA 30328</span></span></span></span></p>

<p style="margin: 0in 0in 10pt 40px; text-align: justify;"><span style="font-size:11pt"><span style="line-height:normal"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt">ebusiness@newellco.com </span></span></span></span></p>

<p style="margin: 0in 0in 10pt 40px; text-align: justify;"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="line-height:115%">Questions about our products should not be directed to this address and will not be replied to.</span></span></span></span></span></p>

<p style="margin: 0in 0in 10pt 40px; text-align: justify;"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif"><b><span style="font-size:12.0pt"><span style="line-height:115%">Procedure for Copyright Infringement Claims</span></span></b></span></span></span></p>

<p style="margin: 0in 0in 10pt 40px; text-align: justify;"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif"><span lang="EN" style="color:#4f81bd">Newell Brands Inc. and its affiliated companies respect the intellectual property of others. If you believe that material accessible on this website or through this service infringes one or more of your copyrighted works, you can request that the Service Provider remove the allegedly infringing material by providing a Notification of Claimed Infringement (“Notification”) to our designated Copyright Agent in accordance with the Digital Millennium Copyright Act of 1998 (“DMCA”).</span><span style="font-size:12.0pt"><span style="line-height:115%"></span></span></span></span></span></p>

<p style="margin: 0in 0in 10pt 40px; text-align: justify;"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="line-height:115%">Notification must be submitted to the following Designated Agent: </span></span></span></span></span></p>

<p style="margin: 0in 0in 10pt 40px; text-align: justify;"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="line-height:115%">Service Provider(s): Newell Brands Inc. </span></span></span></span></span></p>

<p style="margin: 0in 0in 10pt 40px; text-align: justify;"><span style="font-size:11pt"><span style="line-height:normal"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt">Name of Agent Designated to Receive Notification of Claimed Infringement: General Counsel Full Address of Designated Agent to Which Notification Should Be Sent: 6655 Peachtree Dunwoody Road, Atlanta, GA 30328 </span></span></span></span></p>

<p style="margin: 0in 0in 10pt 40px; text-align: justify;"><span style="font-size:11pt"><span style="line-height:normal"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt">Telephone Number of Designated Agent: (770) 418-7000</span></span></span></span></p>

<p style="margin: 0in 0in 10pt 40px; text-align: justify;"><span style="font-size:11pt"><span style="line-height:normal"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt">Facsimile Number of Designated Agent: (770) 677-8000 </span></span></span></span></p>

<p style="margin: 0in 0in 10pt 40px; text-align: justify;"><span style="font-size:11pt"><span style="line-height:normal"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt">E-mail Address of Designated Agent: copyright@newellco.com </span></span></span></span></p>

<p style="margin: 0in 0in 10pt 40px; text-align: justify;"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif">&nbsp;</span></span></span></p>

<p style="margin: 0in 0in 10pt 40px; text-align: justify;"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="line-height:115%">To be effective, the Notification must be a written communication that includes the following: </span></span></span></span></span></p>

<p style="margin: 0in 0in 10pt 40px; text-align: justify;"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="line-height:115%">1. A physical or electronic signature of a person authorized to act on behalf of the owner of an exclusive right that allegedly infringed; 2. Identification of the copyrighted work claimed to have been infringed, or, if multiple copyrighted works at a single online site are covered by a single notification, a representative list of such works at that site; 3. Identification of the material that is claimed to be infringing or to be the subject of infringing activity and that is to be removed or access to which is to be disabled, and information reasonably sufficient to permit the service provider to locate the material; 4. Information reasonably sufficient to permit the service provider to contact the complaining party, such as an address, telephone number, and, if available, an electronic mail address at which the complaining party may be contacted; 5. A statement that the complaining party has a good-faith belief that use of the material in the manner complained of is not authorized by the copyright owner, its agent, or the law; 6. A statement that the information in the notification is accurate, and under penalty of perjury, that the complaining party is authorized to act on behalf of the owner of an exclusive right that is allegedly infringed.</span></span></span></span></span></p>

<p style="margin-top: 0in; margin-right: 0in; margin-left: 40px; text-align: justify;"><span style="font-size:12pt"><span style="font-family:&quot;Times New Roman&quot;,serif"><span lang="EN" style="font-family:&quot;Calibri&quot;,sans-serif"><span style="color:#4f81bd">Upon receipt of a valid Notification, the Service Provider will promptly remove or disable access to the allegedly infringing material, shall forward the Notification to the alleged infringer, and will take reasonable steps to notify the alleged infringer that it has removed or disabled access to the material. Service Provider will comply with any valid counter-notification submitted to it as provided under the DMCA.</span></span></span></span></p>

<p style="margin-top: 0in; margin-right: 0in; margin-left: 40px; text-align: justify;"><span style="font-size:12pt"><span style="font-family:&quot;Times New Roman&quot;,serif"><span lang="EN" style="font-family:&quot;Calibri&quot;,sans-serif"><span style="color:#4f81bd">WE CAUTION YOU THAT KNOWINGLY MISREPRESENTING THAT MATERIAL IS INFRINGING MAY SUBJECT YOU TO CIVIL PENALTIES, CIVIL LITIGATION, AND/OR CRIMINAL PROSECUTION FOR PERJURY.</span></span></span></span></p>

<p style="margin-top: 0in; margin-right: 0in; margin-left: 40px; text-align: justify;"><span style="font-size:12pt"><span style="font-family:&quot;Times New Roman&quot;,serif"><span lang="EN" style="font-family:&quot;Calibri&quot;,sans-serif"><span style="color:#4f81bd">NOTE: This information is provided exclusively for providing Service Provider with Notifications of Claimed Infringement under the DMCA. All other inquiries will not receive a response through this process or through the email address listed above. The information on this page should not be construed as legal advice.</span></span></span></span></p>

<p style="margin: 0in 0in 10pt 40px; text-align: justify;"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="line-height:115%"></span></span></span></span></span></p>

<p style="margin: 0in 0in 10pt 40px; text-align: justify;"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="line-height:115%">© 2020 Newell Brands Inc.&nbsp;&nbsp; All rights reserved.</span></span></span></span></span></p>

<p style="margin:0in 0in 10pt; margin-top:0in; margin-right:0in; margin-left:0in"><span style="font-size:11pt"><span style="line-height:115%"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="line-height:115%"></span></span></span></span></span></p>
    

</div>';


?>
