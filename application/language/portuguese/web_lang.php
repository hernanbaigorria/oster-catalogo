<?php 
	
	$lang['esp'] = 'Espanhol';
	$lang['en'] = 'Inglês';
	$lang['port'] = 'Português';

  $lang['search'] = 'Procurar';

  $lang['meses'] = '["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"]';
  $lang['dias'] = '["Seg", "Ter", "Qua", "Qui", "Sex", "Sab", "Dom"]';
  $lang['hoy'] = 'Hoje';

	$lang['last_view'] = 'Última visita';
	$lang['new_products'] = 'O novo';
	$lang['learn_more'] = 'Ver mais';

	$lang['privacy'] = 'Declaração de privacidade';
	$lang['cookie'] = 'Política de Cookies';
	$lang['terms'] = 'Termos de uso';

  $lang['menu_service'] = 'Assistência Técnica';
  $lang['menu_product'] = 'Registro de Produto';
  $lang['menu_buy'] = 'Onde comprar';
  $lang['menu_contact'] = 'Contáctanos';

  $lang['header_buy'] = 'header-buy-port.png';
  $lang['header_service'] = 'header-support-port.png';
  $lang['header_service_sub'] = 'ESCOLHA SEU PAÍS';
  $lang['header_service_desc'] = '<strong>Opções de serviço durante o período de garantia:</strong><br>Para assistência técnica, entre em contato com o centro de atendimento mais próximo';

  $lang['social_info'] = 'Nos acompanhe nas Redes Sociais!';

  $lang['newsletter_info'] = 'Ao enviar, confirmo que li e aceito a Declaração de Privacidade e gostaria de receber e-mails marketing e/ou promocionais da Oster';
  $lang['newsletter_send'] = 'Cadastre-se aqui';

  $lang['back_top'] = 'De volta ao topo';

  $lang['title_resource'] = 'Recursos';
  $lang['title_overview'] = 'Descrição';

  $lang['contact_title'] = 'Contato';
  $lang['contact_sub'] = 'Todos os campos obrigatórios marcados com';

  $lang['input_name'] = 'Nome';
  $lang['input_lastname'] = 'Sobrenome';
  $lang['input_email'] = 'E-mail';
  $lang['input_country'] = 'País';
  $lang['input_subject'] = 'Assunto';
  $lang['input_message'] = 'Mensagem';

  $lang['input_address'] = 'Endereço';
  $lang['input_model'] = 'Número do modelo';
  $lang['input_date'] = 'Data de compra';

  $lang['input_terms'] = 'Gostaria de receber e-mails de marketing e/ou promocionais da Oster.';
  $lang['input_subterms'] = 'Ao enviar, confirmo que li a <a href="'.base_url().'br/privacidad/" target="_blank">Declaração de Privacidade.</a>';
  $lang['input_submit'] = 'Enviar';

	$lang['cookie_txt'] = '<div id="heading">
    <h1> POLÍTICA DE COOKIES DA NEWELL BRANDS </h1>
    <h3> Última modificação: v2.1, 1° de dezembro de 2019 </h3>
  </div>
  <div id="container">
      <div>
          Estamos empenhados em honrar a privacidade e a segurança de nossos usuários, clientes e fornecedores, e também de seus representantes, no que diz respeito a todos os produtos, serviços, aplicativos e sites fornecidos pela Newell Brands Inc. e nossas afiliadas ("Newell Brands"). Esta Política de Cookies explica o que são Cookies, como usamos Cookies, como terceiros com os quais podemos fazer parcerias podem usar Cookies em nosso site, suas opções com relação aos Cookies e outras informações sobre Cookies.
      </div>
    <br>
      <div>
          Os sites e mídias digitais da Newell Brands, incluindo aplicativos, aplicativos móveis, contas da rede social, dispositivos habilitados para internet e Wi-Fi etc. ("Sites") podem usar cookies e tecnologias semelhantes (como tags de pixels, web beacon etc.) para permitir que nossa empresa e terceiros obtenhamos informações sobre suas visitas aos Sites, inclusive para analisar seus padrões de visitas. Poderemos usar essas informações para processar suas transações ou solicitações e, quando aplicável, divulgar anúncios, mensagens e conteúdo enviados on-line e no celular, por nós e também por terceiros, que sejam específicos para os seus interesses.
      </div>
    <br>
      <div>
        <b></b>O que são cookies?</b> Cookies são pequenos arquivos de texto armazenados em seu computador ou outros dispositivos. São amplamente usados para fazer os sites funcionarem bem ou aumentar a eficiência de um site. Os cookies ajudam a aprimorar a funcionalidade de um site e a experiência do usuário porque os sites podem ler e escrever para esses arquivos de texto, permitindo que o site consiga reconhecer você e se lembrar de informações importantes que tornarão seu uso do site mais conveniente (por exemplo, lembrar suas configurações de idioma ou preferência de fonte).
      </div>
    <br>
      <div>
          As tags/arquivos de funcionalidade não requerem seu consentimento. No entanto, sempre que for aplicável e exigido por lei, solicitaremos seu consentimento antes de colocar esses tipos de Cookies em seu dispositivo e permitiremos que você opte por não aceitá-los. Você pode dar seu consentimento clicando no botão apropriado no banner de Cookies que é exibido quando você acessa nossos Sites ou usando a central de preferências em nossa ferramenta de gerenciamento de cookies.
    </div>
  
    <h2>Uso de Cookies por nossos Sites</h2>
      <div>Nossos Sites poderão usar os seguintes Cookies:</div>
      <br>
      <div>
        <b><u>Cookies essenciais.</u>.</b>  Trata-se dos Cookies necessários para o funcionamento dos nossos Sites. Os Cookies essenciais podem permitir que você, entre outras coisas, faça login em áreas seguras de nossos Sites, navegue em nossos Sites, adicione produtos ao seu carrinho, passe pelo caixa, use nossos serviços de faturamento eletrônico etc.
      </div>
  <br>
      <div>
        <b><u>Cookies analíticos/de desempenho</u>.</b> Esses Cookies nos permitem reconhecer e contar o número de visitantes e ver como os visitantes navegam em nossos Sites. Podemos usar esses Cookies para, entre outras coisas, medir como nossos clientes estão usando o site de modo a aprimorar a funcionalidade e otimizar sua experiência de compra. Isso nos ajuda a aprimorar a forma como nossos Sites funcionam ao, por exemplo, garantir que os usuários encontrem facilmente o que estão procurando.
    </div>
  <br>
      <div>
        <b><u>Cookies de funcionalidade/preferências do usuário</u>:</b> Esses Cookies são usados para reconhecê-lo quando você volta aos nossos Sites. Podemos usá-los para lembrar o idioma e a moeda que você usa quando está no site e ajudar a simplificar sua experiência de compra. Esses Cookies nos permitem personalizar nosso conteúdo para você, cumprimentá-lo chamando-o pelo nome e lembrar suas preferências. 
    </div>
  <br>
      <div>
        <b><u>Cookies de marketing/segmentação/compartilhamento social</u>.</b>Esses Cookies registram suas visitas aos nossos Sites, as páginas que você visitou e os links que você seguiu. Usaremos essas informações para tornar nossos Sites e a publicidade que divulgamos mais relevantes para seus interesses. Quando aplicável e conforme permitido por lei, também poderemos compartilhar essas informações com terceiros para este fim. Observe que terceiros (incluindo, por exemplo, redes de publicidade e provedores de serviços externos, como análises do tráfego de internet) também podem usar Cookies, sobre os quais não temos controle. 
      </div>
  <br>
  
    <h2> 
        Como gerenciar, desativar ou optar por não aceitar os Cookies usados por nossos Sites 
        </h2> 
    <div>
        Os navegadores da internet permitem que você altere as configurações de seus Cookies por várias razões, entre elas bloquear certos tipos de Cookies ou arquivos. Assim, você pode bloquear certos Cookies ativando as configurações aplicáveis no seu navegador. No entanto, se usar as configurações do seu navegador para bloquear todos os cookies, você poderá não ser capaz de acessar nossos Sites, no todo ou em parte, porque alguns desses cookies poderão ser Cookies essenciais ou de funcionalidades. Para obter mais informações sobre como excluir ou bloquear Cookies, acesse o site: <br>
    <a href="http://www.allaboutcookies.org/manage-cookies">http://www.allaboutcookies.org/manage-cookies</a> Você pode recusar, aceitar ou remover Cookies de nossos Sites a qualquer momento ao ativar ou acessar a configuração de preferências de Cookies em seu navegador. Para obter mais informações sobre como alterar as preferências de Cookies ou desativar os Cookies completamente, consulte o artigo
    <a href="http://www.allaboutcookies.org/manage-cookies">http://www.allaboutcookies.org/manage-cookies</a> e saiba mais sobre os navegadores mais usados. Fique ciente de que, se os Cookies forem desativados ou removidos, você poderá não ser capaz de acessar nossos Sites, no todo ou em parte, ou os recursos poderão não funcionar conforme o pretendido.
    </div>
  
  <br>
    <div>
        Para não ser rastreado pelo Google Analytics em todos os sites, acesse <a href="http://tools.google.com/dlpage/gaoptout">http://tools.google.com/dlpage/gaoptout.</a>
    </div>
  
    <h2> Mais informações</h2>
    
      <div>Você pode encontrar mais informações sobre Cookies e tags/arquivos semelhantes nos seguintes endereços:
        <br> - <a href=" http://www.allaboutcookies.org/"> http://www.allaboutcookies.org/;</a>
        <br> - <a href="http://www.youronlinechoices.eu/">http://www.youronlinechoices.eu/</a> (um guia de publicidade comportamental e privacidade on-line produzido pelo setor de publicidade na internet).
  
        <br><br>
        Se tiver dúvidas ou preocupações ou precisar de mais informações, não hesite em entrar em contato conosco no link <a href="data-privacy-enquiry_pt.html">Consulta sobre Privacidade de Dados</a>.
    </div>';
	$lang['privacidad_txt'] = '<div id="heading">
    <h1> DECLARAÇÃO DE PRIVACIDADE DA NEWELL BRANDS  </h1>
    <h3> Última modificação: v2.5, 17 de novembro de 2020</h3>
  </div>
  <div id="container">
	  
	  <div>TÓPICOS:</div>  
          <h5><a href="#1">1.Sua privacidade é importante para nós</a></h5>
          <h5><a href="#2">2.Dados pessoais que coletamos sobre você</a></h5>
          <h5><a href="#3">3.Por que processamos seus dados pessoais e bases legais para seu processamento e retenção</a></h5>
          <h5><a href="#4">4.Como compartilhamos seus dados pessoais</a></h5>
          <h5><a href="#5">5.Dados Pessoais Sensíveis</a></h5>
          <h5><a href="#6">6.Transferências internacionais de dados</a></h5>
          <h5><a href="#7">7.Seus direitos relativos ao acesso aos dados pessoais e seu controle sobre eles</a></h5>
          <h5><a href="#8">8.Dados de contato do controlador de dados e da proteção de dados</a></h5>
          <h5><a href="#9">9.Dados pessoais enviados por crianças</a></h5>
          <h5><a href="#10">10.Cookies, web beacons e outras tecnologias de rastreamento</a></h5>
          <h5><a href="#11">11.Segurança</a></h5>
          <h5><a href="#12">12.Links para sites de terceiros</a></h5>
          <h5><a href="#13">13.Não rastrear</a></h5>
          <h5><a href="#14">14.Como usar dispositivos móveis e outros dispositivos</a></h5>
          <h5><a href="#15">15.Comunicados de e-mail marketing</a></h5>
          <h5><a href="#16">16.Mensagens de texto</a></h5>
          <h5><a href="#17">17.Alterações em nossa declaração de privacidade</a></h5>
          <h5><a href="#18">18.Fale conosco</a></h5>
          <h5><a href="#19">19.O que há de novo</a></h5>
          
      <h2 id="1">1.SUA PRIVACIDADE É IMPORTANTE PARA NÓS</h2>
      <div>
          Na Newell Brands, estamos empenhados em honrar a privacidade e a segurança de nossos usuários, clientes e fornecedores, e também de nossos representantes, no que diz respeito a todos os produtos, serviços, aplicativos e sites fornecidos pela Newell Brands Inc. e nossas afiliadas ("Newell Brands"), quando estiverem atuando na função de Controladoras ao abrigo das regras e regulamentos relevantes de proteção de dados.  
      </div>
      <br>
      <div>
          Esta Declaração de Privacidade descreve nossas práticas de privacidade conforme exigido pelo Regulamento Geral sobre a Proteção de Dados ("RGPD") (UE) (2016/679) e outras leis aplicáveis de proteção de dados (em conjunto, "Leis de Privacidade"). Esta Declaração de Privacidade se aplica sempre que a Newell Brands estiver atuando como Controladora de Dados no que se refere aos Dados Pessoais de nossos clientes, funcionários, usuários de sites, parceiros e provedores de serviços.  Em outras palavras, esta Declaração de Privacidade se aplica sempre que determinarmos os fins e os meios de processamento de Dados Pessoais.  
     <br></br>
     Esta Declaração de Privacidade fornece informações sobre como coletamos, processamos, compartilhamos, transferimos, retemos e protegemos seus Dados Pessoais e os direitos que você poderá exercer, sempre que aplicável, referentes às suas informações.  Se você nos fornecer seus Dados Pessoais, ou se viermos a obter Dados Pessoais seus junto a outras fontes, trataremos seus Dados Pessoais de acordo com esta Política de Privacidade.
          <br></br>Esta Política de Privacidade se aplica aos sites da Newell Brands, incluindo, mas não se limitando ao newellbrands.com, aos aplicativos móveis da Newell Brands, às contas de rede social dedicadas à Newell Brands, aos dispositivos habilitados para internet e Wi-Fi, etc. ("Sites").  Esta Política de Privacidade não se aplica aos aplicativos, produtos, serviços, sites ou recursos de rede social de terceiros que podem ser acessados através de links em nossos Sites e que têm suas próprias políticas de privacidade.  Acessar esses links fará com que você saia de nossos Sites e pode resultar na coleta e/ou compartilhamento de seus Dados Pessoais por um terceiro. Não controlamos, endossamos, nem fazemos quaisquer afirmações sobre sites de terceiros ou suas práticas de privacidade, que podem ser diferentes das nossas.  Incentivamos que você leia as práticas de privacidade de qualquer site com o qual interage antes de permitir a coleta e o uso de seus Dados Pessoais. Se tiver alguma dúvida ou comentário referente a esta Declaração de Privacidade ou às nossas práticas de privacidade de dados, entre em contato conosco no link 
          <a href="data-privacy-enquiry-form_pt.html">Consulta sobre Privacidade de Dados</a> .
      </div>
    
    <h2 id="2">2.DADOS PESSOAIS QUE COLETAMOS SOBRE VOCÊ  </h2>

    <div>
        Quando conclui uma transação, envia uma solicitação, configura uma conta, se candidata a um emprego, visita nossos Sites etc, junto a qualquer entidade da Newell Brands, você poderá ser solicitado a fornecer determinadas informações, incluindo, mas não se limitando ao seu nome, cargo, empresa, endereço, número de telefone, endereço de e-mail, dados de cartão de crédito e linha de negócio.  Você também pode estar nos fornecendo dados sobre seus dispositivos, incluindo seu endereço IP, sua geolocalização, seu uso de dados, sua conta de cliente ou de fornecedor etc.  Em circunstâncias limitadas, poderemos coletar informações relacionadas a você e aos seus familiares, como idade, sexo e estado civil.  Essas informações e dados podem ser Dados Pessoais no âmbito das Leis de Privacidade aplicáveis. 
    </div>
    
    <h2 id="3">3.POR QUE PROCESSAMOS SEUS DADOS PESSOAIS E BASES LEGAIS PARA SEU PROCESSAMENTO E RETENÇÃO  </h2>
    <div>Consulte a tabela abaixo para ver as categorias de Dados Pessoais que poderemos coletar sobre você, por que processamos (coletamos, usamos, armazenamos etc.) seus Dados Pessoais, os fundamentos jurídicos para tal processamento (sempre que aplicável) e o período máximo de retenção relevante.
    </div>
<br>
    <table>
      <tr>
        <th>Finalidade </th>
        <th>Categorias de Dados Pessoais</th>
        <th>Bases legais para o processamento (caso sejam exigidas pelas Leis de Privacidade aplicáveis)</th>
        <th>Período de retenção (máx.*)</th>
      </tr>
      <tr>
        <td>Gestão de clientes (incluindo a prestação de serviços, faturamento, atendimento ao cliente, gerenciamento de contas, personalização de sua experiência em nossos Sites etc.)

        </td>
        <td><ul>
          <li>Dados de identificação (nome, endereço, telefone etc.)</li>
          <li>Dados de identificação eletrônica (e-mail, endereços IP, geolocalização, dados de uso de sites, cookies etc.)</li>
          <li>Características financeiras (número de conta bancária, dados de cartão de crédito etc.)</li>
          <li>Características pessoais dos titulares de dados e, em alguns casos, de membros da família (idade, sexo, estado civil etc.)   </li>
          </ul>
        </td>
        <td><ul>
          <li>Execução de um contrato entre nós e você e/ou providências a serem tomadas, mediante sua solicitação, para celebrar esse contrato   </li>
          <li>Obrigação legal de processar seus dados</li>
          <li>Nossos interesses legítimos relativos à administração adequada de nosso site e de nossa empresa </li>
        </ul>
        </td>
        <td>
            Até 10 anos após a data do último pedido ou término do contrato, a menos que um período maior seja exigido por lei ou em decorrência de um litígio
		</td>	
      </tr>
      <tr>
        <td>Vendas diretas ao consumidor (incluindo prestação de serviços, faturamento, atendimento ao cliente, gerenciamento de contas, personalização de sua experiência em nossos Sites etc.)
        </td>
        <td>
          <ul>
        <li>Dados de identificação (nome, endereço, telefone etc.)</li>
        <li>Dados de identificação eletrônica (e-mail, endereços IP, geolocalização, dados de uso de sites, cookies etc.)</li>
        <li>Características financeiras (número de conta bancária, dados de cartão de crédito etc.)</li>
        <li>Características pessoais dos titulares de dados e, em alguns casos, de membros da família (idade, sexo, estado civil etc.)</li>
      </ul>
        </td>
        <td>
          <ul>
            <li>Execução de um contrato entre nós e você e/ou providências a serem tomadas, mediante sua solicitação, para celebrar esse contrato </li>
            <li>Obrigação legal de processar seus dados</li>
            <li>Nossos interesses legítimos relativos à administração adequada de nosso site e de nossa empresa </li>
          </ul>
        </td>
        <td>
            Até 10 anos após a data do último pedido ou término do contrato, a menos que um período maior seja exigido por lei ou em decorrência de um litígio
		</td>	
      </tr>
      <tr>
        <td>Conheça o seu cliente (incluindo medidas antifraude, antilavagem de dinheiro etc.</td>
        <td>
          <ul>
            <li>Dados de identificação (nome, endereço, telefone etc.)</li>
            <li>Dados de identificação eletrônica (e-mail, endereços IP, geolocalização, dados de uso de sites, cookies etc.) Características financeiras (número de conta bancária, dados de cartão de crédito etc.)</li>
            <li>Características pessoais dos titulares de dados e, em alguns casos, de membros da família (idade, sexo, estado civil etc.)</li>
          </ul>
        </td>
        <td> <ul>
          <li>Execução de um contrato entre nós e você e/ou providências a serem tomadas, mediante sua solicitação, para celebrar esse contrato  </li>
          <li>Nossos interesses legítimos relativos à administração adequada de nosso site e de nossa empresa </li>
        </ul>
        </td>
        <td>
            Até 10 anos após a data do último pedido ou término do contrato, a menos que um período maior seja exigido por lei ou em decorrência de um litígio
		</td>
      </tr>
      <tr>
        <td>Marketing direto (incluindo comunicados de e-mail marketing, como promoções, campanhas de marketing, pesquisas etc.)**</td>
        <td><ul>
          <li>Dados de identificação (nome, endereço, telefone etc.)</li>
          <li>Dados de identificação eletrônica (e-mail, endereços IP, geolocalização, dados de uso de sites, cookies etc.)</li>
        </ul>
        </td>
        <td>
            <ul>
                <li>Com seu consentimento/optar por aceitá-los quando necessário</li>
                <li>Execução de um contrato entre nós e você e/ou providências a serem tomadas, mediante sua solicitação, para celebrar esse contrato  </li>
                <li>Nossos interesses legítimos relativos à administração adequada de nosso site e de nossa empresa</li>
              </ul>
        </td>
        <td>
            Até que você cancele o seu consentimento para comunicações de marketing direto ou suas informações não sejam mais necessárias para fins de marketing 
        </td>
      </tr>
      <tr>
        <td>Gestão de fornecedores (incluindo contabilidade etc.) </td>
        <td>
            <ul>
                <li>Dados de identificação (nome, endereço, telefone etc.)</li>
                <li>Características financeiras (número de conta bancária, dados de cartão de crédito etc.)</li>
                <li>Dados de identificação eletrônica (e-mail, endereços IP, geolocalização, dados de uso de sites, cookies etc.)</li>
                <li>Características pessoais dos titulares de dados e, em alguns casos, de membros da família (idade, sexo, estado civil etc.)</li>
              </ul>
        </td>
        <td>
          <ul>
            <li>Execução de um contrato entre nós e você e/ou providências a serem tomadas, mediante sua solicitação, para celebrar esse contrato  </li>
            <li>Nossos interesses legítimos relativos à administração adequada de nosso site e de nossa empresa </li>
            <li>Obrigação legal de processar seus dados</li>
          </ul>
        </td>
        <td>
            Até 10 anos após a data do último pedido ou término do contrato, a menos que um período maior seja exigido por lei ou em decorrência de um litígio
        </td>
      </tr>
      <tr>
        <td>Manter a segurança do nosso pessoal e de nossos ativos  </td>
        <td>
            <ul>
                <li>Dados de identificação (nome, endereço, telefone etc.)</li>
                <li>Dados de identificação eletrônica (e-mail, endereços IP, geolocalização, dados de uso de sites, cookies etc.)</li>
                <li>Características financeiras (número de conta bancária, dados de cartão de crédito etc.)</li>
                <li>Características pessoais dos titulares de dados e, em alguns casos, de membros da família (idade, sexo, estado civil etc.)</li>
              </ul>
        </td>
        <td><ul>
            <li>Execução de um contrato entre nós e você e/ou providências a serem tomadas, mediante sua solicitação, para celebrar esse contrato </li>
            <li>Obrigação legal de processar dados com segurança</li>
            <li>Nossos interesses legítimos relativos à administração adequada de nosso site e de nossa empresa</li>
          </ul>
        </td>
        <td>
            Até 10 anos após a data do último pedido ou término do contrato, a menos que um período maior seja exigido por lei ou em decorrência de um litígio 
			</td>
      </tr>
      <tr>
        <td>Gestão de Litígios, Compliance e Departamento Jurídico</td>
        <td>
          <ul>
            <li>Dados de identificação (nome, endereço, telefone etc.)</li>
            <li>Dados de identificação eletrônica (e-mail, endereços IP, geolocalização, dados de uso de sites, cookies etc.)</li>
            <li>Características financeiras (número de conta bancária, dados de cartão de crédito etc.)</li>
            <li>Características pessoais dos titulares de dados e, em alguns casos, de membros da família (idade, sexo, estado civil etc.)</li>
          </ul>
        </td>
        <td> <ul>
            <li>Processamento necessário para o estabelecimento, exercício ou defesa de reivindicações ou obrigações legais. </li>
            <li>Execução de um contrato entre nós e você e/ou providências a serem tomadas, mediante sua solicitação, para celebrar esse contrato  </li>
            <li>Obrigação legal de processar dados</li>
            <li>Nossos interesses legítimos relativos à administração adequada de nosso site e de nossa empresa</li>
          </ul>
        </td>
        <td>Até 10 anos após o término de uma obrigação de conformidade jurídica ou outras obrigações legais ou após a resolução final de um litígio </td>
      </tr>
      <tr>
        <td>Aprimoramento de produtos/serviços e inteligência de negócios</td>
        <td>
         <ul>
           <li>Dados de identificação (nome, endereço, telefone etc.)</li>
           <li>Dados de identificação eletrônica (e-mail, endereços IP, geolocalização, dados de uso de sites, cookies etc.)</li>
           <li>Características financeiras (número de conta bancária, dados de cartão de crédito etc.)</li>
           <li>Características pessoais dos titulares de dados e, em alguns casos, de membros da família (idade, sexo, estado civil etc.)</li>
         </ul>
        </td>
        <td> <ul>
            <li>Seu consentimento</li>
            <li>Nossos interesses legítimos relativos à administração adequada de nosso site e de nossa empresa</li>            
          </ul>
        </td>
        <td>Até 10 anos após a descontinuação do produto</td>
      </tr>
    </table>
    <br>
    <div id="asterisks">
        * Os períodos de retenção variam dependendo do tipo de documento que contém os Dados Pessoais. Para obter mais informações sobre os períodos de retenção, entre em contato conosco no link 
      <a href="data-privacy-enquiry-form_pt.html">Consulta sobre Privacidade de Dados</a>.
    <br></br>** Se quiser se opor ao uso de seus Dados Pessoais para esses fins, consulte a seção "Seus direitos relativos ao acesso aos seus dados pessoais e controle sobre eles" abaixo.</div>
    <br>
    <div>Manteremos seus Dados Pessoais enquanto estivermos fornecendo nossos produtos e serviços para você. Posteriormente, manteremos seus Dados Pessoais por uma dessas razões: (a) responder a quaisquer perguntas, reclamações ou reivindicações feitas por você ou em seu nome; (b) mostrar que o tratamos de forma justa; (c) para a administração adequada da nossa empresa; e/ou (d) manter registros na medida do exigido por lei.
        <br></br>
        Não iremos reter seus Dados Pessoais por mais tempo do que o necessário para os fins para os quais foram coletados, conforme estabelecido nesta Declaração de Privacidade. Ao determinar os períodos de retenção de dados, levamos em consideração a legislação local, as obrigações contratuais, nossos requisitos comerciais na medida do razoável e suas próprias expectativas e exigências. Quando não for mais necessário reter seus Dados Pessoais, iremos excluí-los ou anonimizá-los com segurança.
    </div>

    <h2 id="4">4.COMO COMPARTILHAMOS SEUS DADOS PESSOAIS</h2>
	<div>Seus Dados Pessoais poderão ser compartilhados com outras pessoas conforme descrito abaixo: 
      <ul>
        <li>Poderemos divulgar seus Dados Pessoais para qualquer membro do nosso grupo de empresas, afiliadas, revendedores, fornecedores e provedores de serviços, incluindo redes de publicidade, conforme necessário na medida do razoável para as respectivas finalidades e de acordo com as bases jurídicas estabelecidas nesta Declaração de Privacidade.  Embora não consideremos tal divulgação aos provedores de serviços para esses fins como uma "venda" de informações, entendemos e revelamos a você que ela poderá ser interpretada como tal ao abrigo de determinadas leis aplicáveis.</li>
        <li>Poderemos divulgar seus Dados Pessoais às nossas seguradoras e consultores profissionais conforme necessário, na medida do razoável, para fins de obtenção ou manutenção de uma cobertura de seguros, gestão de riscos, obtenção de assessoria profissional ou o estabelecimento, exercício ou defesa de reivindicações legais, seja em processos judiciais ou outros processos jurídicos.</li>
        <li>Poderemos divulgar seus Dados Pessoais junto a nossos provedores de serviços de pagamento para concluir transações financeiras. Compartilharemos dados de transações com nossos provedores de serviços de pagamento apenas na medida do necessário para fins de processamento de seus pagamentos, reembolso desses pagamentos e para lidar com reclamações e consultas relacionadas a tais pagamentos e reembolsos.</li>
        <li>Além das divulgações específicas de Dados Pessoais estabelecidas nesta seção, poderemos divulgar seus Dados Pessoais: (a) quando tal divulgação for necessária para o cumprimento de uma obrigação legal à qual estejamos sujeitos, incluindo uma intimação, ordem judicial ou mandado de busca; (b) implementar nossos Termos de Serviço, esta Declaração de Privacidade ou outros contratos celebrados com você, incluindo a investigação de possíveis violações dos mesmos; (c) responder a reivindicações relativas a violações de direitos de terceiros; (d) responder às suas solicitações de atendimento ao cliente; e/ou (e) proteger os direitos, as propriedades ou a segurança pessoal da Newell Brands, por si e por seus funcionários, agentes e afiliadas, usuários e/ou do público em geral.  Isso inclui a troca de informações com outras empresas e organizações para fins de proteção contra fraudes, prevenção de spam/malware ou para proteger seus interesses vitais ou os interesses vitais de outra pessoa natural.
        </li>
      </ul>
    </div>
    <div>Reservamo-nos o direito de transferir seus Dados Pessoais na eventualidade de uma falência, de venda ou transferência de nossos negócios ou ativos no todo em parte, ou de uma aquisição, fusão ou alienação.  </div>
    <br>
   <div>Exceto conforme disposições em contrário estabelecidas neste instrumento, não vendemos, negociamos, alugamos nem compartilhamos seus Dados Pessoais com terceiros fora da Newell Brands ou com nossas empresas afiliadas em troca de remuneração em dinheiro ou outros meios de valor não pecuniários.  
  </div>  
    <h2 id="5">5.DADOS PESSOAIS SENSÍVEIS </h2>

    <div>Salvo solicitação específica nesse sentido, pedimos que você não nos envie e tampouco divulgue, seja nos Sites, por meio destes ou para nós de qualquer outra forma, Dados Pessoais Sensíveis (como, por exemplo, sua religião, etnia, opiniões políticas, ideologias ou outras crenças, saúde, dados biométricos ou características genéticas, antecedentes criminais, filiação sindical ou processos e sanções administrativas ou criminais), a menos que especificamente solicitado por nós ou exigido por lei.</div>
  
    <h2 id="6">6.TRANSFERÊNCIAS INTERNACIONAIS DE DADOS</h2>

    <div>A Newell Brands é uma empresa global sediada nos Estados Unidos. A Newell Brands e nossas empresas afiliadas, mantêm escritórios e instalações em locais ao redor do mundo, incluindo a União Europeia/EEE, América Latina, Ásia, África etc.  Seus Dados Pessoais poderão ser transferidos para outras entidades da Newell Brands e também para provedores de serviços terceirizados que poderão processar seus Dados Pessoais em nosso nome, incluindo provedores de serviços de marketing eletrônico, provedores de hospedagem etc.
     </div>
<br>
    <di>Nossos clientes e usuários de nossos Sites podem estar localizados em qualquer lugar do mundo.  As leis de privacidade em alguns países poderão não fornecer proteções equivalentes às do seu país de residência, e seu governo poderá ou não considerar essas proteções de privacidade adequadas.  Poderemos processar suas informações ou transferir suas informações para os Estados Unidos, para terceiros localizados nos Estados Unidos ou em outros países.  </di>
    <br></br>
    <div>Para os residentes em países da UE/EEE, no caso da transferência de seus Dados Pessoais para um país que foi considerado como tendo um nível inadequado de proteção para Dados Pessoais, implementaremos salvaguardas, incluindo a implementação de cláusulas contratuais padrão, de acordo com os requisitos da UE.  Você pode solicitar mais informações sobre tais medidas entrando em contato conosco no link
      <a href="data-privacy-enquiry-form_pt.html"> Consulta sobre Privacidade de Dados  </a>. 
  </div>

    <h2 id="7">7.SEUS DIREITOS RELATIVOS AO ACESSO AOS DADOS PESSOAIS E SEU CONTROLE SOBRE ELES</h2>
      <div>Seus direitos relativos aos seus Dados Pessoais dependem da legislação local na jurisdição onde você reside. </div> <br>
      <div> Se você for residente da UE/EEE , poderá ter o direito, após enviar uma solicitação verificada, de obter:</div>
      <ul>
        <li><u><i>Informações e acesso </i></u> - você pode solicitar acesso aos seus Dados Pessoais, receber informações complementares sobre seus Dados Pessoais e/ou obter uma cópia de seus Dados Pessoais.</li>
        <li><u><i>Retificação </i></u> - você pode solicitar a correção e/ou atualização de seus Dados Pessoais que estiverem incorretos ou desatualizados.</li>
        <li><u><i>Eliminação    </i></u> - você pode ter o direito de ter seus Dados Pessoais apagados. </li>
        <li><u><i>Restrição  </i></u> - você pode ter o direito de restringir o processamento de seus Dados Pessoais.</li>
        <li><u><i>Objeção ao processamento </i></u> - você pode ter o direito de se opor a tipos específicos de processamento de seus Dados Pessoais. </li>
        <li><u><i>Portabilidade de dados  </i></u> - você pode ter o direito de solicitar uma cópia de seus Dados Pessoais com direito à portabilidade. Por exemplo, a Portabilidade de Dados não se aplica aos registros impressos e não deve prejudicar os direitos de terceiros ou informações sigilosas da empresa.  </li>
        <li><u><i>Direito de não estar sujeito a decisões baseadas somente em decisões automatizadas </i></u> - você pode ter o direito de não estar sujeito a decisões baseadas apenas em um processamento automatizado (ou seja, sem intervenção humana).</li>
        <li><u><i>Direito de apresentar uma reclamação</i></u> - você poderá ter o direito de apresentar uma reclamação a uma autoridade de supervisão no país de sua residência, reclamação esta cujos detalhes poderão ser fornecidos mediante solicitação ao Comitê de Privacidade de Dados por meio de um formulário on-line, ou, se for localizado na Europa, à autoridade principal de supervisão da Newell Brands, a saber, o Escritório do Comissário de Informações localizado na Wycliff House, Water Lane, Wilmslow, Cheshire SK9 6AF, Reino Unido,  
          <a href="https://ico.org.uk/">www.ico.org.uk </a>.
        </li>
      </ul>
    <div>Se desejar exercer qualquer um desses direitos, envie uma <a href="data-subject-request-form_pt.html">Solicitação do Titular de Dados</a>. </div><br>
    <div>A Newell Brands poderá cobrar uma taxa razoável se sua solicitação for repetitiva, excessiva ou não for válida nos termos da legislação aplicável. Também poderemos solicitar que nos envie mais informações para verificar sua identidade antes de lhe enviar uma resposta.  </div>
    <br><div>Se você reside na Califórnia, poderá ter o direito, após enviar uma solicitação verificada, de:</div>
    <div>
    <ul>
      <li>Saber quais categorias de informações pessoais coletamos sobre você, </li>
      <li>Saber se suas informações pessoais foram vendidas ou divulgadas a terceiros para fins comerciais, inclusive para fins de marketing direto por terceiros, durante o ano civil imediatamente anterior,</li>
      <li>Conhecer a identidade dos terceiros que receberam suas informações pessoais para fins de marketing direto durante o ano civil em questão,</li>
      <li>Excluir suas informações pessoais mantidas por nós,</li>
      <li>Optar por não aceitar a venda de suas informações pessoais,</li>
      <li>Acessar suas informações pessoais e obter uma cópia de seus dados com direito à portabilidade, </li>
      <li>Não ser discriminado por exercer seus direitos; e</li>
      <li>Receber uma notificação de seus direitos.</li>
    </ul>
  </div>

  <div>
      Se desejar exercer qualquer um desses direitos, envie sua solicitação para  <a href="data-subject-request-form_pt.html">Solicitação do Titular de Dados</a>.
     <br></br>(Observe que os consumidores do estado da Califórnia passaram a poder exercer esses direitos verificados a partir de 1º de janeiro de 2020).  
     <br></br>Para optar por não aceitar a venda de suas informações pessoais, clique aqui:
     <a href="data-subject-request-form_pt.html">NÃO VENDA MINHAS INFORMAÇÕES PESSOAIS</a>.
     <br></br><b><i>Esta Declaração de Privacidade não cria, amplia ou modifica quaisquer direitos do Titular de Dados da UE, os direitos do consumidor do estado da Califórnia ou as obrigações da Newell Brands, exceto conforme disposto pelo RGPD e pelo CCPA </i></b>
    </div>

    <h2 id="8">8.DADOS DE CONTATO DA PROTEÇÃO DE DADOS E DO CONTROLADOR DE DADOS DA UE/EEE </h2>
    <div>Você pode encontrar uma lista e dados de contato das entidades Controladoras de Dados da Newell Brands, e mais informações relacionadas às Autoridades de Supervisão, na página: <a href="controllers_pt.html"> Controladores de Dados e Autoridades de Supervisão da Newell Brands.</a>. 
        <br></br>Sempre que a entidade Controladora de Dados da Newell Brands estiver localizada fora da UE/EEE, o Representante da Controladora na UE estará especificado na página <a href="controllers_pt.html">Controladores de Dados e Autoridades de Supervisão da Newell Brands.</a>.
        <br></br>Você pode entrar em contato com o diretor de Proteção de Dados da Newell no link:<br>
    </br><a href="data-privacy-enquiry-form_pt.html">Consulta de Privacidade de Dados</a><br>
    ou A/C Diretor de Proteção de Dados
      <br>6655 Peachtree Dunwoody Road
      <br>Atlanta, Georgia 30328 EUA
    </div>
    <br>
    <div>Para obter mais informações sobre a extensão total de seus direitos, ou para exercer seus direitos, entre em contato com nosso diretor de Proteção de Dados usando as informações de contato acima ou no link  <a href="data-privacy-enquiry-form_pt.html">Consulta sobre Privacidade de Dados</a> , ou, ainda com a autoridade principal de supervisão da Newell Brands, a saber, o Escritório do Comissário de Informações localizado na Wycliff House, Water Lane, Wilmslow, Cheshire SK9 6AF, Reino Unido, <a href="https://ico.org.uk/">www.ico.org.uk</a>.</div>

    <h2 id="9">9.DADOS PESSOAIS ENVIADOS POR CRIANÇAS </h2>

    <div>Alguns dos conteúdos em nossos Sites podem ser direcionados para crianças menores de 13 anos. No entanto, não coletamos nem solicitamos intencionalmente dados pessoais de crianças menores de 13 anos sem o consentimento dos pais, a menos que seja permitido por lei. Se tomarmos conhecimento de que coletamos dados pessoais de uma criança menor de 13 anos sem o consentimento dos pais — a menos que seja permitido por lei — iremos excluí-los de acordo com a legislação aplicável. Se você acredita que uma criança possa ter nos fornecido dados pessoais sem o consentimento dos pais ou de outra forma não permitida por lei, entre em contato conosco conforme detalhado abaixo na seção "Fale Conosco".
    <br></br><u><i>Consumidores do estado da Califórnia </i></u>– não venderemos as informações dos consumidores da Califórnia com 16 anos de idade ou menos sem prévia autorização. Nossos Sites e serviços direcionados às crianças irão verificar a idade do usuário e oferecer opções relativas à venda de informações pessoais relacionadas a crianças menores de 16 anos.  Se um consumidor da Califórnia tiver menos de 13 anos, um pai ou responsável deverá dar seu consentimento para a venda de dados pessoais do menor.  No entanto, se o consumidor do estado da Califórnia tiver entre 13 e 16 anos, o menor de idade poderá fornecer seu consentimento optando por aceitar a venda de suas informações pessoais.  Nossos Sites voltados para crianças irão solicitar uma confirmação da idade e exigir o consentimento adequado. 
    <br></br><u><i>Residentes da UE </i></u>– não coletamos nem processamos dados pessoais relacionados a Titulares de Dados da UE menores de 16 anos sem o consentimento explícito de um pai ou responsável.   Nossos Sites voltados para crianças irão solicitar uma confirmação da idade e exigir o consentimento conforme adequado.  
    </div>
    
    <h2 id="10">10.COOKIES, WEB BEACONS E OUTRAS TECNOLOGIAS DE RASTREAMENTO </h2>
    <div>Nossos Sites usam Cookies. Cookies são arquivos de texto contendo pequenas quantidades de informação que são baixadas no seu dispositivo quando você visita um site.  Os Cookies são úteis porque permitem que um site reconheça o dispositivo de um usuário.  Os Cookies ajudam a realizar uma variedade de funções de um site, incluindo permitir que os usuários "façam login" nos sites (tornando sua navegação de mais eficiente), lembrar suas preferências (como o idioma) e aprimorando sua experiência em nossos Sites de modo geral.  Os Cookies também podem ajudar a garantir que os anúncios e conteúdos que você vê on-line sejam mais relevantes para você e seus interesses.  Também poderemos incluir web beacons em mensagens de e-mail marketing ou em nossos boletins informativos, de modo a determinar se as mensagens foram abertas e se os links informados foram clicados.
    </div><br>
    <div>Alguns de nossos parceiros de negócios configuram web beacons e Cookies no nosso site. Além disso, os botões de rede social de terceiros poderão registrar determinadas informações, como seu endereço IP, tipo de navegador e idioma, tempo de acesso e endereços dos sites que nos recomendaram.  Mais ainda, se você estiver logado nesses sites de rede social, eles também poderão vincular essas informações coletadas às informações do seu perfil no site em questão.
        <br></br>Se quiser optar por não aceitar os Cookies, visite o site www.aboutcookies.org ou www.allaboutcookies.org para obter mais informações.  Esses sites também informam como configurar seu navegador para não aceitar Cookies e como remover Cookies do seu navegador.  No entanto, em alguns casos, alguns dos recursos em nossos Sites poderão não funcionar em decorrência disso.  Mais informações sobre o uso de Cookies podem ser encontradas em nossa 
        <a href="cookie_pt.html">Política de Cookies</a>.
    </div>

    <h2 id="11">11.SEGURANÇA</h2>
    <div>
        Uma segurança perfeita é algo que não existe.  Podemos afirmar que implementamos salvaguardas organizacionais, técnicas e administrativas adequadas e na medida do comercialmente razoável para proteger Dados Pessoais.  Mas você é responsável por manter o sigilo de todas as credenciais que possam eventualmente ser usadas para acessar qualquer conta ou serviço junto à Newell. Caso suspeite de qualquer atividade não autorizada, certifique-se de nos notificar imediatamente.  Você pode notificar atividades suspeitas ou outras preocupações entrando em contato conosco no link:
        <br></br><a href="data-privacy-enquiry-form_pt.html">Consulta de Privacidade de Dados</a>
    </div>

    <h2 id="12">12.LINKS PARA SITES DE TERCEIROS</h2>
    <div>
        Poderemos coletar suas informações em sites de terceiros.  Isso inclui as situações em que você faz login por meio de um site de terceiros, como Facebook, Snapchat, Instagram, LinkedIn, Twitter, etc. Também poderemos divulgar informações coletadas de terceiros para personalizar anúncios e gerenciar e facilitar o envio de mensagens em sites de terceiros.  Vale observar que, quando você cancela sua assinatura de nossas mensagens de e-mail marketing, pode ocorrer um atraso na interrupção de nossos anúncios direcionados a você em sites de terceiros. 
    </div>
    <h2 id="13">13.NÃO RASTREAR</h2>
    <div>
        A Newell não rastreia seus clientes ao longo do tempo nem em sites de terceiros para fornecer publicidade direcionada e, portanto, não responde aos sinais de Não Rastrear (DNT, Do Not Track).  No entanto, alguns sites de terceiros rastreiam suas atividades de navegação ao distribuir conteúdo, o que lhes permite adaptar o que apresentam a você.  Se estiver visitando um desses sites, você poderá ser capaz de configurar o sinal DNT em seu navegador para que terceiros (particularmente anunciantes) saibam que você não quer ser rastreado.
    </div>

    <h2 id="14">14.COMO USAR DISPOSITIVOS MÓVEIS E OUTROS DISPOSITIVOS</h2>
    <div>
        A Newell, por si e por suas afiliadas e terceiros que contratamos, poderá coletar e armazenar identificadores únicos associados ao seu dispositivo móvel para fornecer anúncios ou conteúdos personalizados enquanto você usa os nossos Sites e navega pela internet, de modo a fornecer serviços e publicidade baseados em localização ou identificá-lo de forma única em todos os dispositivos ou navegadores.  Para personalizar esses anúncios ou conteúdos, nós, ou os terceiros que contratamos, poderemos coletar Dados Pessoais, como, por exemplo, seu endereço de e-mail ou dados seus coletados passivamente, como o identificador, localização ou endereço IP do seu dispositivo.  A maioria dos dispositivos móveis permite que você desative os serviços de localização. 
        <br><br>Ao ajustar as configurações de privacidade e segurança do seu dispositivo, você pode gerenciar como seu dispositivo móvel e navegador móvel compartilham informações, e também como seu navegador móvel lida com Cookies e outros identificadores de rastreamento.  Consulte as instruções fornecidas por seu provedor de serviços móveis ou pelo fabricante do seu dispositivo para aprender como ajustar suas configurações.
    </div>

    <h2 id="15">15.COMUNICADOS DE E-MAIL MARKETING  </h2>
    <div>
        Se for um cliente existente ou, se assim for exigido pela legislação aplicável, tiver nos fornecido seu consentimento ou não tiver nos orientado de alguma outra forma a não enviar mensagens de marketing, poderemos lhe enviar por e-mail mensagens de marketing ou e-mails para mantê-lo atualizado sobre nossos produtos e serviços. 
        <br></br>Você poderá solicitar que pare de receber mensagens de marketing enviadas por nós a qualquer momento, por meio dos seguintes métodos:
        <ul>
          <li>Por meio das configurações/preferências da sua conta de cliente na aba "Minha Conta";</li>
          <li>Clicando no link “cancelar minha assinatura” em qualquer comunicado que enviamos por e-mail;</li>
          <li>Entrando em contato conosco no link<a href="data-subject-request-form_pt.html">Solicitação do Titular de Dados</a> </li>
        </ul>
        Assim que um desses métodos tenha sido executado, honraremos sua solicitação e atualizaremos seu perfil para garantir que você não receba mais mensagens de marketing.
        <br></br>Iremos processar as solicitações de optar por não aceitar/cancelar a assinatura o mais rápido possível, na maioria dos casos no prazo de 10 dias.  No entanto, a Newell Brands é composta de uma complexa rede de muitas empresas e provedores de serviços interligados, por isso pode levar algum tempo para que todos os nossos sistemas reflitam essas atualizações. Pedimos sua compreensão para o fato de que, durante esse período, você ainda venha a receber mensagens enviadas por nós enquanto sua solicitação estiver sendo processada.  
        <br></br>Observe que a interrupção de envio das mensagens de marketing não impedirá o envio de comunicados referentes a serviços, como atualizações de pedidos.
    </div>

    <h2 id="16">16.MENSAGENS DE TEXTO </h2>
    <div>
        Nossos Sites poderão lhe dar a opção de optar por receber mensagens de texto e alertas nos números de celular que compartilhou conosco.  Após você ter optado por aceitá-las, poderemos enviar mensagens de texto relativas à sua conta para investigar ou impedir fraudes, alertá-lo em caso de emergência etc.  Poderemos lhe enviar mensagens de texto e alertas usando tecnologia de autodiscagem.  Além disso, poderemos divulgar periodicamente determinadas campanhas que lhe oferecem a capacidade de enviar uma mensagem de texto para um código curto específico e conceder sua permissão para receber mensagens de texto no futuro.  Sua solicitação de optar por aceitá-las irá nos fornecer seu número de celular.  Se decidir fazer essa opção, você declara concordar que poderemos, periodicamente, enviar mensagens para o dispositivo móvel associado a esse número envolvendo novos produtos, promoções, serviços ou ofertas. 
        <br></br>Você pode optar por deixar de aceitar nossas mensagens de texto e alertas a qualquer momento, bastando para isso responder "PARE" à mensagem que recebeu.  Após ter optado por deixar de recebê-las, você não receberá mais nenhuma mensagem de texto no seu celular.  Tenha em mente que, se você optar por não receber mensagens de texto e alertas, poderemos não ser capazes de entrar em contato com mensagens importantes sobre sua conta ou transações conosco.  No entanto, se houver uma emergência ou uma pergunta relativa à sua conta, faremos o possível para entrar em contato com você de outras maneiras, como enviar um e-mail ou ligar para um número de telefone fixo.  Você não precisa optar por aceitar mensagens de texto e alertas para usar nossos Sites e serviços.  Se você optar por aceitá-las, o envio de mensagens de texto padrão poderá ser cobrado.  Para obter mais informações sobre nossas mensagens de texto e alertas, entre em contato conosco no link  <a href="data-privacy-enquiry-form_pt.html">Consulta sobre Privacidade de Dados</a>.
    </div>

    <h2 id="17">17.ALTERAÇÕES EM NOSSA DECLARAÇÃO DE PRIVACIDADE </h2>
    <div>
        Reservamo-nos o direito, a nosso exclusivo critério, de alterar, modificar, adicionar ou remover partes desta Política de Privacidade a qualquer momento.  Notificaremos tais alterações de acordo com a legislação de proteção de dados aplicável. 
        <br></br>Quaisquer alterações substanciais desta Política de Privacidade serão publicadas nesta página da web. É sua responsabilidade rever nossa Política de Privacidade a cada vez que você nos fornece Dados Pessoais, já que a Política de Privacidade pode ter sido alterada desde a última vez que você usou nossos Sites ou serviços.
    </div>

    <h2 id="18">18.FALE CONOSCO</h2>
      <div>
          Se tiver dúvidas sobre esta Política de Privacidade ou práticas da Newell Brands, entre em contato conosco pelo link: <a href="data-privacy-enquiry-form_pt.html">Consulta sobre Privacidade de Dados.</a>.
      </div>

      <h2 id="19">19.O QUE HÁ DE NOVO</h2>
      <div>
          Com entrada em vigor em 15 de novembro de 2019, atualizamos e harmonizamos algumas partes da nossa Declaração de Privacidade.  Aqui está um resumo de algumas das alterações:  
          <br>
          <ul>
			<li>Agora passaremos a usar o termo "Sites" para descrever nosso site, aplicativos móveis etc.</li> 
			<li>Esclarecemos os tipos de Dados Pessoais que coletamos sobre você e as formas como coletamos essas informações.</li>
			<li>Descrevemos seus direitos de acesso aos seus Dados Pessoais e seu controle sobre eles.</li>  
			<li>Fornecemos dados de contato atualizados.</li>
			<li>Descrevemos como poderemos usar e compartilhar seus Dados Pessoais.</li>
			<li>Esclarecemos os tipos de Dados Pessoais que coletamos sobre você e as formas como coletamos esses Dados Pessoais. Coletamos Dados Pessoais de muitas maneiras, inclusive em sites de terceiros, sites de rede social e redes de publicidade.</li>
			<li>Descrevemos como compartilhamos e usamos seus Dados Pessoais. </li>
			<li>Descrevemos nosso uso de comunicados por e-mail e mensagens de texto e como optar por não aceitá-los e/ou controlar esses comunicados.</li>
			<li>Notificamos os usuários quanto aos seus direitos ao abrigo do RGPD e da Lei de Privacidade do Consumidor da Califórnia (CCPA), sempre que aplicável.</li>
			<li>Esclarecemos que seus Dados Pessoais poderão ser transferidos através de fronteiras internacionais para processamento e outros usos.</li>
			<li>Esclarecemos como compartilhamos seus Dados Pessoais com nossos fornecedores, fornecedores de terceiros e sites que nos recomendam.</li>
			<li>Incluímos informações adicionais sobre sinais de Não Rastrear.</li>
          </ul>

      </div>
      <div id="heading">
        <h1> DECLARAÇÃO DE PRIVACIDADE DA NEWELL BRANDS   </h1>
        <h3> ANEXO A</h3>
      </div>
      <div id="container">
		<h2>20.	SUA PRIVACIDADE É IMPORTANTE PARA NÓS</h2>
      <div>
          Este anexo, juntamente com a Declaração de Privacidade da Newell Brands, descreve as nossas práticas de privacidade conforme exigido pela <a href="http://www.planalto.gov.br/ccivil_03/_ato2015-2018/2018/lei/L13709.htm">Lei Geral de Proteção de Dados Pessoais ("LGPD") do Brasil (Lei n.º 13.709/2018)</a>.  Para os titulares de dados localizados no Brasil, ou cobertos de qualquer outra forma pela LGPD, os tópicos abaixo devem ser lidos em conjunto com as disposições numeradas constantes na Declaração de Privacidade da Newell Brands para melhor entendimento de nossas práticas de privacidade conforme exigido pela LGPD.  Uma lista abrangente de tópicos pode ser encontrada na Declaração de Privacidade da Newell Brands.
      </div>
      <h2>3. Por que processamos seus dados pessoais e bases legais para seu processamento e retenção</h2>
      <div>
         Observe que, ao abrigo da LGPD, a base legal para processar seus dados pessoais é o nosso interesse legítimo na administração adequada do nosso site e de nossos negócios.  Além disso, será utilizada uma única base legal aplicável para processar os dados pessoais dos titulares de dados que estão no Brasil.
      </div>
      <h2>6.Transferências internacionais de dados</h2>
      <div>
         Para os titulares de dados que estão no Brasil, no caso de uma transferência de seus Dados Pessoais para um país que não proporcione um grau de proteção de dados pessoais adequado às disposições da LGPD, implementaremos as salvaguardas necessárias para a conformidade com os direitos dos titulares de dados e com os princípios estabelecidos na LGPD. Essas salvaguardas poderão ser fornecidas na forma de: cláusulas contratuais específicas ou cláusulas padrão de proteção de dados; regras societárias vinculativas; e/ou certificados ou códigos de conduta aprovados.
      <br></br>Seus Dados Pessoais também poderão ser transferidos para outros países: quando necessário para a proteção da sua vida e da sua segurança física ou da vida e segurança física de terceiros; quando você nos der seu consentimento específico para tal transferência e sempre que for necessário para o cumprimento das obrigações legais da Newell Brands; para a implementação de um contrato celebrado com você; ou para assegurar o exercício regular dos direitos da Newell Brands em processos judiciais, administrativos ou arbitrais.
      </div>
      <h2>7.	Seus Direitos Relativos ao Acesso aos Dados Pessoais e seu Controle sobre eles</h2>
      <div>
		  Se você for residente do Brasil, poderá ter o direito, após enviar uma solicitação verificada, de obter:
		  <br>
          <ul>
			<li><u><i>Confirmação da existência de tratamento e acesso </i></u> - você pode solicitar a confirmação de que seus Dados Pessoais são objeto de tratamento e requerer acesso aos seus Dados Pessoais, receber informações complementares sobre seus Dados Pessoais e/ou obter uma cópia de seus Dados Pessoais.</li>
			<li><u><i>Retificação </i></u> - você pode solicitar a correção e/ou atualização de seus Dados Pessoais que estiverem incorretos, incompletos ou desatualizados.</li>
			<li><u><i>Anonimização, Bloqueio ou Eliminação    </i></u> - você pode ter o direito de ter seus Dados Pessoais anonimizados, bloqueados ou apagados, caso se tratem de Dados Pessoais desnecessários, excessivos ou tratados em desconformidade com a Lei Geral de Proteção de Dados Brasileira. Você também pode requerer a eliminação dos seus Dados Pessoais tratados com base no consentimento, nos limites previstos na Lei Geral de Proteção de Dados Brasileira; </li>
			<li><u><i>Portabilidade de dados  </i></u> - você pode ter o direito de solicitar a portabilidade dos seus Dados Pessoais para outro fornecedor de serviços e produtos. A Portabilidade de Dados não inclui dados anonimizados e não deve prejudicar os direitos de terceiros ou informações sigilosas da empresa.</li>
			<li><u><i>Informações sobre o compartilhamento dos seus Dados Pessoais </i></u> - você pode ter o direito de receber informações sobre as entidades públicas e privadas com as quais compartilhamos os seus Dados Pessoais; </li>
			<li><u><i>Possibilidade de não fornecer consentimento e revogação do consentimento:  </i></u> - você tem o direito de ser informado sobre a possibilidade de não fornecer consentimento e sobre as consequências da negativa, bem como sobre a possibilidade de revogação do consentimento a qualquer tempo;  </li>
			<li><u><i>Direito de revisão de decisões baseadas somente em decisões automatizadas </i></u> - você pode ter o direito de solicitar a revisão de decisões tomadas unicamente com base em tratamento automatizado de dados pessoais (ou seja, sem intervenção humana).</li>
			<li><u><i>Direito de apresentar uma reclamação</i></u> - você poderá ter o direito de apresentar uma reclamação à Autoridade Nacional de Proteção de Dados.
        </li></ul>
        <br></br>Se desejar exercer seus direitos enquanto titular de dados no Brasil ou desejar alterar, excluir ou solicitar uma cópia de seus dados pessoais, envie-nos uma Solicitação de Acesso do Titular de Dados, que pode ser encontrada <a href="data-subject-request-form_pt.html">aqui</a>.  Se tiver dúvidas sobre a Declaração de Privacidade da Newell Brands, sobre este anexo ou sobre as práticas da Newell, entre em contato conosco por meio de uma: <a href="data-privacy-enquiry-form_pt.html">Consulta sobre Privacidade de Dados</a>.
       <br></br><b><i>Esta Declaração de Privacidade não cria, amplia ou modifica quaisquer direitos do Titular de Dados localizado no Brasil ou obrigações da Newell Brands, exceto conforme disposto pelo LGPD. </i></b>
      </div>
      <h2>8.	Dados de contato do controlador de dados e da proteção de dados</h2>
      <div>
          Você pode entrar em contato com o Encarregado de Dados, Juliana Arroyo, através do envio de uma solicitação através do(s) formulário(s) disponível(is), ou através do email <a href = "mailto: data.privacy@newellco.com">data.privacy@newellco.com</a>
      </div>
      <h2>9.	Dados pessoais enviados por crianças</h2>
      <div>
          Não coletamos nem processamos dados pessoais relacionados a titulares de dados que estejam no Brasil e sejam menores de 12 anos sem o consentimento explícito de um pai ou responsável. Nossos Sites voltados para crianças irão solicitar uma confirmação da idade e exigir o consentimento conforme adequado.
      </div>  
	  </div> ';
?>