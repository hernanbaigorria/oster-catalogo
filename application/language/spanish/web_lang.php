<?php 
	
	$lang['esp'] = 'Español';
	$lang['en'] = 'Ingles';
	$lang['port'] = 'Portugues';

	$lang['search'] = 'Buscar';

	$lang['meses'] = '["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"]';
	$lang['dias'] = '["Lun", "Mar", "Mie", "Jue", "Vie", "Sab", "Dom"]';
	$lang['hoy'] = 'Hoy';

	$lang['last_view'] = 'Últimos visitados';
	$lang['new_products'] = 'Lo nuevo';
	$lang['learn_more'] = 'Ver Más';

	$lang['menu_service'] = 'Servicio y soporte';
	$lang['menu_product'] = 'Registro de producto';
	$lang['menu_buy'] = 'Dónde comprar';
	$lang['menu_contact'] = 'Contáctanos';

	$lang['header_buy'] = 'header-buy-spa.png';
	$lang['header_service'] = 'header-support-spa.png';
	$lang['header_service_sub'] = 'SELECCIONA TU PAÍS';
	$lang['header_service_desc'] = '<strong>Opciones de servicio durante el periodo de garantía:</strong><br>Para apoyo técnico, contacta al centro de servicio más cercano';

	$lang['social_info'] = 'Síguenos en tus plataformas favoritas.';

	$lang['contact_title'] = 'Contáctanos';
	$lang['contact_sub'] = 'Campo obligatorio';

	$lang['input_name'] = 'Nombre';
  $lang['input_lastname'] = 'Apellido';
  $lang['input_email'] = 'Correo electrónico';
  $lang['input_country'] = 'País';
  $lang['input_subject'] = '¿En qué podemos aydarte hoy?';
  $lang['input_message'] = 'Favor escribir tu mensaje aquí';

  $lang['input_address'] = 'Dirección';
  $lang['input_model'] = 'Número de modelo';
  $lang['input_date'] = 'Fecha de compra';

  $lang['input_terms'] = 'Me gustaría recibir correos electrónicos de marketing y / o promocionales de Oster®';
  $lang['input_subterms'] = 'Al enviar, confirmo que he leído y acepto sus <a href="'.base_url().'ar/privacidad/" target="_blank">Términos y condiciones y la Declaración de privacidad.</a>';
  $lang['input_submit'] = 'Enviar';

	$lang['newsletter_info'] = 'Al inscribirme, declaro que soy mayor de edad y acepto la <a href="https://privacy.newellbrands.com/index_es.html" target="_blank">Declaración de privacidad</a> y los <a href="https://www.osterlatinamerica.com/servicio-y-soporte/informacion-legal/terminos-y-condiciones" target="_blank">Términos y Condiciones</a> de Sunbeam Latin America, LLC y entiendo que puedo cancelar mi consentimiento en cualquier momento.© 2022 Sunbeam Latin America, LLC., haciendo negocios como Newell Brands. Todos los derechos reservados.';

	$lang['newsletter_send'] = 'Inscribirse';

	$lang['back_top'] = 'Volver arriba';

	$lang['title_resource'] = 'Recursos';
	$lang['title_overview'] = 'Descripción';

	$lang['privacy'] = 'Declaracion de privacidad';
	$lang['cookie'] = 'Política de cookies';
	$lang['terms'] = 'Condiciones de uso';

	$lang['cookie_txt'] = '<div id="heading">
		    <h1> POLÍTICA DE COOKIES DE NEWELL BRANDS </h1>
		    <h3> Última modificación: v2.1 December 1 2019 </h3>
		  </div>
		  <div id="container">
		      <div>
		          Nos esforzamos por respetar la privacidad y seguridad de nuestros usuarios, clientes y proveedores, así como sus representantes, en relación con todos los productos, servicios, aplicaciones y sitios web proporcionados por Newell Brands y nuestras filiales (“Newell Brands”). Esta Política de cookies explica qué son las cookies, cómo usamos las cookies, cómo los terceros con los que podemos asociarnos pueden utilizar cookies en nuestro Sitio, sus opciones en relación con las cookies y otra información sobre estas.
		      </div>
		    <br>
		      <div>
		          Los sitios web y medios digitales de Newell Brands, incluidas las aplicaciones, aplicaciones móviles, cuentas de redes sociales, Internet y dispositivos habilitados para wifi, etc. (“Sitios”), pueden utilizar cookies y tecnologías similares (como etiquetas de píxeles, balizas web, etc.) para que nosotros y terceros obtengamos información sobre sus visitas a los Sitios, lo que incluye analizar sus patrones de visita. Podemos utilizar esta información para procesar sus transacciones o solicitudes y, cuando proceda, mostrar anuncios, mensajes y contenidos en línea y en el móvil, tanto nuestros como de terceros, que sean específicos para sus intereses. 
		      </div>
		    <br>
		      <div>
		        <b>¿Qué son las cookies?</b> Las cookies son pequeños archivos de texto almacenados en su ordenador u otros dispositivos. Se utilizan mucho para hacer que los sitios web funcionen o para mejorar la eficiencia de un sitio web. Las cookies ayudan a la funcionalidad del sitio web y a la experiencia del usuario porque los sitios web pueden leer y escribir en estos archivos de texto, lo que permite al sitio reconocerlo y recordar información importante que hará que su uso del sitio le resulte más cómodo (por ejemplo, al recordar su idioma o configuración de preferencias para la fuente de los textos). 
		      </div>
		    <br>
		      <div>
		          Las etiquetas/archivos de funcionalidad no requieren su consentimiento. Sin embargo, en el caso de las etiquetas analíticas y otras etiquetas/archivos, cuando sea aplicable y exigido por ley, solicitaremos su consentimiento antes de colocarlas en su dispositivo o le permitiremos optar por no utilizar estos tipos de cookies. Puede dar su consentimiento al hacer clic en el botón correspondiente del banner de cookies que se muestra al visitar nuestros Sitios o en el centro de preferencias de nuestra herramienta de gestión de cookies.
		    </div>
		  
		    <h2>Uso de cookies en nuestros sitios</h2>
		      <div>Nuestros Sitios pueden utilizar las siguientes cookies:</div>
		      <br>
		      <div>
		        <b><u>Cookies esenciales</u>.</b> Son cookies necesarias para el funcionamiento de nuestros Sitios. Las cookies esenciales pueden permitirle, entre otras cosas, iniciar sesión en áreas seguras de nuestros Sitios, navegar por nuestros Sitios, añadir productos al carrito, pagar, utilizar nuestros servicios de facturación electrónica, etc
		      </div>
		  <br>
		      <div>
		        <b><u>Cookies analíticas/de rendimiento</u>.</b> Estas cookies nos permiten reconocer y contar el número de visitantes y ver cómo estos navegan por nuestros Sitios. Podemos utilizar estas cookies para, entre otras cosas, medir cómo nuestros clientes utilizan el sitio y poder mejorar la funcionalidad y optimizar la experiencia de compra para usted. Esto nos ayuda a mejorar la forma en que nuestros Sitios funcionan, por ejemplo, al asegurarnos de que los usuarios encuentran lo que buscan fácilmente.
		    </div>
		  <br>
		      <div>
		        <b><u>Cookies de preferencias de usuario/funcionalidad</u>:</b> Estas cookies se utilizan para reconocerlo cuando vuelve a nuestros Sitios. Podemos usarlas para recordar el idioma y la moneda que utiliza en el sitio y para ayudar a agilizar su experiencia de compra. Estas cookies nos permiten personalizar nuestro contenido, saludarlo por su nombre y recordar sus preferencias. 
		    </div>
		  <br>
		      <div>
		        <b><u>Cookies de marketing/segmentación/redes sociales</u></b>. Estas cookies registran sus visitas a nuestros Sitios, las páginas que ha visitado y los enlaces en los que ha hecho clic. Utilizaremos esta información para hacer que nuestros Sitios y la publicidad que se muestra en ellos sean más relevantes para sus intereses. Cuando sea aplicable y según lo permita la ley, también podremos compartir esta información con terceros para este fin. Tenga en cuenta que los terceros (incluidos, por ejemplo, redes publicitarias y proveedores de servicios externos como servicios de análisis de tráfico web) también pueden utilizar cookies, sobre las que no tenemos control.
		      </div>
		  <br>
		  
		    <h2> 
		        Cómo gestionar, deshabilitar o rechazar las cookies utilizadas en nuestros sitios 
		        </h2> 
		    <div>
		        Los navegadores de Internet le permiten cambiar la configuración de cookies por varios motivos, como por ejemplo, para bloquear determinados tipos de cookies o archivos. Por lo tanto, puede bloquear determinadas cookies si activa la configuración correspondiente en su navegador. Sin embargo, si utiliza la configuración del navegador para bloquear todas las cookies, es posible que no pueda acceder a todos o a parte de nuestros Sitios porque puede que algunas de estas cookies sean de funcionalidad o esenciales. Para obtener más información sobre la eliminación o bloqueo de cookies visite: 
		    <a href="http://www.allaboutcookies.org/manage-cookies">http://www.allaboutcookies.org/manage-cookies</a> Puede rechazar, aceptar o eliminar cookies de nuestros Sitios en cualquier momento si activa o accede a la configuración de preferencias de cookies en su navegador. Para obtener más información sobre cómo cambiar sus preferencias de cookies o deshabilitar todas las cookies consulte 
		    <a href="http://www.allaboutcookies.org/manage-cookies">http://www.allaboutcookies.org/manage-cookies</a> para los navegadores más utilizados. Tenga en cuenta que si las cookies están deshabilitadas o se eliminan, es posible que no pueda acceder a todos o a parte de nuestros Sitios, o que las funciones no puedan usarse según lo previsto. 
		    </div>
		  
		  <br>
		    <div>
		        Para optar por que Google Analytics no haga un seguimiento en ningún sitio web visite  <a href="http://tools.google.com/dlpage/gaoptout">http://tools.google.com/dlpage/gaoptout.</a>
		    </div>
		  
		    <h2> Información adicional </h2>
		    
		      <div>Puede encontrar más información sobre cookies y etiquetas/archivos similares en las siguientes direcciones:
		        <br> - <a href=" http://www.allaboutcookies.org/"> http://www.allaboutcookies.org/;</a>
		        <br> - <a href="http://www.youronlinechoices.eu/">http://www.youronlinechoices.eu/</a> (una guía sobre publicidad conductual y privacidad en línea, creada por la industria publicitaria de Internet).
		  
		        <br><br>
		        Si tiene preguntas o inquietudes o necesita más información, no dude en ponerse en contacto con nosotros en <a href="data-privacy-enquiry_es.html">Consulta de privacidad de datos</a>.
		    </div>';
    
    $lang['privacidad_txt'] = '<div id="heading">
		    <h1> DECLARACIÓN DE PRIVACIDAD DE NEWELL BRANDS</h1>
		    <h3> Última modificación: v2.4 septiembre 2020 </h3>
		  </div>
		  <div id="container">
			   <div>TEMAS:</div>  
		          <h5><a href="#1">1.Su privacidad es importante para nosotros</a></h5>
		          <h5><a href="#2">2.Datos personales que recogemos sobre usted</a></h5>
		          <h5><a href="#3">3.Por qué tratamos sus datos personales, bases legales para el tratamiento y retención </a></h5>
		          <h5><a href="#4">4.Cómo compartimos sus datos personales</a></h5>
		          <h5><a href="#5">5.Datos personales sensibles</a></h5>
		          <h5><a href="#6">6.Transferencias de datos internacionales</a></h5>
		          <h5><a href="#7">7.Sus derechos sobre el acceso y el control de los datos personales</a></h5>
		          <h5><a href="#8">8.Información de contacto del responsable del tratamiento y la protección de datos</a></h5>
		          <h5><a href="#9">9.Datos personales enviados por niños</a></h5>
		          <h5><a href="#10">10.Cookies, faros web y otras tecnologías de rastreo</a></h5>
		          <h5><a href="#11">11.Seguridad</a></h5>
		          <h5><a href="#12">12.Enlaces a sitios web de terceros</a></h5>
		          <h5><a href="#13">13.No rastreo</a></h5>
		          <h5><a href="#14">14.Uso de móviles y otros dispositivos</a></h5>
		          <h5><a href="#15">15.Comunicaciones de marketing por correo electrónico</a></h5>
		          <h5><a href="#16">16.Mensajes de texto</a></h5>
		          <h5><a href="#17">17.Cambios en nuestra declaración de privacidad</a></h5>
		          <h5><a href="#18">18.Póngase en contacto con nosotros</a></h5>
		          <h5><a href="#19">19.Novedades</a></h5>
		      <h2 id="1">1.SU PRIVACIDAD ES IMPORTANTE PARA NOSOTROS</h2>
		      <div>
		        En Newell Brands, nos esforzamos por respetar la privacidad y seguridad de nuestros usuarios, clientes y proveedores, así como sus representantes, en relación con todos los productos, servicios, aplicaciones y sitios web proporcionados por Newell Brands, Inc. o cualquiera de sus filiales (“Newell Brands”), al actuar como Responsable según las normas y reglamentos de protección de datos pertinentes. 
		      </div>
		      <br>
		      <div>
		        Esta Declaración de privacidad describe nuestras prácticas de privacidad, tal y como exige el Reglamento general de protección de datos (“RGPD”) (2016/679) y otras leyes de protección de datos aplicables (colectivamente “Leyes de privacidad”). Esta Declaración de privacidad se aplica cuando Newell Brands actúa como Responsable del tratamiento de datos con respecto a los Datos personales de nuestros clientes, empleados, usuarios del sitio web, socios y proveedores de servicios. En otras palabras, esta Declaración de privacidad se aplica cuando determinamos los fines y medios del tratamiento de Datos personales. 
		     <br></br>
		     Esta Declaración de privacidad proporciona información sobre cómo recogemos, tratamos, compartimos, transferimos, retenemos y protegemos sus Datos personales y los derechos que puede ejercer, cuando proceda, en relación con su información. Si nos proporciona sus Datos personales u obtenemos Datos personales sobre usted de otras fuentes, los tratamos de acuerdo con esta Política de privacidad.
		          <br></br>Esta Política de privacidad se aplica a los sitios web de Newell Brands, incluidos, entre otros, newellbrands.com, las aplicaciones móviles de Newell Brands, las cuentas para redes sociales de Newell Brands, Internet y dispositivos habilitados para wifi, etc. (“Sitios web”). Esta Política de privacidad no se aplica a aplicaciones, productos, servicios, sitios web o funciones de redes sociales de terceros a los que pueda accederse a través de enlaces en nuestros Sitios web que tengan sus propias políticas de privacidad. Acceder a esos enlaces le hará salir de nuestros Sitios web y puede dar lugar a que un tercero recoja o comparta sus Datos personales. No controlamos, avalamos ni realizamos ninguna declaración sobre sitios web de terceros ni sobre sus prácticas de privacidad, que pueden diferir de las nuestras. Le aconsejamos que revise las prácticas de privacidad de cualquier sitio web con el que interactúe antes de permitir que recojan y usen sus Datos personales. Si tiene preguntas o comentarios sobre esta Declaración de privacidad o nuestras prácticas de privacidad de datos, póngase en contacto con nosotros en
		          <a href="data-privacy-enquiry-form_es.html">Consulta de privacidad de datos</a>.
		      </div>
		    
		    <h2 id="2">2.DATOS PERSONALES QUE RECOGEMOS SOBRE USTED  </h2>

		    <div>
		      Cuando complete una transacción, envíe una solicitud, configure una cuenta, solicite un puesto, visite nuestros Sitios web, etc., con cualquier entidad de Newell Brands, se le puede pedir que proporcione cierta información, incluidos, entre otros, su nombre, cargo, empresa, dirección, número de teléfono, dirección de correo electrónico, información de tarjeta de crédito y línea de negocio. También puede proporcionarnos datos sobre sus dispositivos, como su dirección IP, su geolocalización, su uso de datos, su cuenta de cliente o de proveedor, etc. En circunstancias limitadas, podemos recoger información relacionada con usted y sus familiares, como edad, sexo y estado civil. Esta información y datos pueden ser Datos personales según las Leyes de privacidad aplicables.
		    </div>
		    
		    <h2 id="3">3.POR QUÉ TRATAMOS SUS DATOS PERSONALES, BASES LEGALES PARA EL TRATAMIENTO Y RETENCIÓN</h2>
		    <div>Consulte la tabla a continuación para ver las categorías de Datos personales que podemos recoger sobre usted, por qué tratamos (recogemos, usamos, almacenamos, etc.) sus Datos personales, los fundamentos legales para dicho tratamiento (cuando proceda) y el período de retención máximo pertinente.
		    </div>
		<br>
		    <table>
		      <tr>
		        <th>Fines </th>
		        <th>Categorías de datos personales</th>
		        <th>Bases legales para el tratamiento (si así lo exigen las leyes de privacidad aplicables)</th>
		        <th>Período de retención (máx.*)</th>
		      </tr>
		      <tr>
		        <td>Gestión de clientes (incluida la prestación de servicios, facturación, atención al cliente; gestión de cuentas, personalización de su experiencia en nuestros sitios web, etc.)

		        </td>
		        <td><ul>
		          <li>Datos de identificación (nombre, dirección, teléfono, etc.)</li>
		          <li>Datos de identificación electrónica (correo electrónico, direcciones IP, geolocalización, datos de uso del sitio web, cookies, etc.)</li>
		          <li>Características financieras (número de cuenta bancaria, detalles de la tarjeta de crédito, etc.)</li>
		          <li>Características personales de los interesados y, en algunos casos, familiares (edad, sexo, estado civil, etc.)  </li>
		          </ul>
		        </td>
		        <td><ul>
		          <li>Celebración de un contrato entre usted y nosotros o toma de medidas, a petición suya, para celebrar dicho contrato </li>
		          <li>Obligación legal de tratar sus datos</li>
		          <li>Nuestros intereses legítimos en la gestión adecuada de nuestro sitio web y nuestro negocio </li>
		        </ul>
		        </td>
		        <td>
		          Hasta 10 años después de la fecha del último pedido o de la finalización del contrato, a menos que se requiera un período más largo por ley o por litigio.
				</td>	
		      </tr>
		      <tr>
		        <td>Ventas directas al consumidor (incluida la prestación de servicios, facturación, atención al cliente, gestión de cuentas, personalizar su experiencia en nuestros sitios web, etc.)
		        </td>
		        <td>
		          <ul>
		        <li>
		          Datos de identificación (nombre, dirección, teléfono, etc.)
		        </li>
		        <li>Datos de identificación electrónica (correo electrónico, direcciones IP, geolocalización, datos de uso del sitio web, cookies, etc.)</li>
		        <li>Características financieras (número de cuenta bancaria, detalles de la tarjeta de crédito, etc.)</li>
		        <li>Características personales de los interesados y, en algunos casos, familiares (edad, sexo, estado civil, etc.)</li>
		      </ul>
		        </td>
		        <td>
		          <ul>
		            <li>Celebración de un contrato entre usted y nosotros o toma de medidas, a petición suya, para celebrar dicho contrato</li>
		            <li>Obligación legal de tratar sus datos</li>
		            <li>Nuestros intereses legítimos en la gestión adecuada de nuestro sitio web y nuestro negocio </li>
		          </ul>
		        </td>
		        <td>
		          Hasta 10 años después de la fecha del último pedido o de la finalización del contrato, a menos que se requiera un período más largo por ley o por litigio
				</td>	
		      </tr>
		      <tr>
		        <td>Conocer a su cliente (incluyendo antifraude, antiblanqueo de capitales, etc.)</td>
		        <td>
		          <ul>
		            <li>Datos de identificación (nombre, dirección, teléfono, etc.)</li>
		            <li>Datos de identificación electrónica (correo electrónico, direcciones IP, geolocalización, datos de uso del sitio web, cookies, etc.) Características financieras (número de cuenta bancaria, detalles de la tarjeta de crédito, etc.)</li>
		            <li>Características personales de los interesados y, en algunos casos, familiares (edad, sexo, estado civil, etc.)</li>
		          </ul>
		        </td>
		        <td> <ul>
		          <li>Celebración de un contrato entre usted y nosotros o toma de medidas, a petición suya, para celebrar dicho contrato </li>
		          <li>Nuestros intereses legítimos en la gestión adecuada de nuestro sitio web y nuestro negocio </li>
		        </ul>
		        </td>
		        <td>
		          Hasta 10 años después de la fecha del último pedido o de la finalización del contrato, a menos que se requiera un período más largo por ley o por litigio
				</td>
		      </tr>
		      <tr>
		        <td>Marketing directo (incluidas comunicaciones de marketing por correo electrónico, promociones, campañas de marketing, encuestas, etc.)**</td>
		        <td><ul>
		          <li>Datos de identificación (nombre, dirección, teléfono, etc.)</li>
		          <li>Datos de identificación electrónica (correo electrónico, direcciones IP, geolocalización, datos de uso del sitio web, cookies, etc.)</li>
		        </ul>
		        </td>
		        <td>
		            <ul>
		                <li>Con su consentimiento/inclusión en el lugar necesario</li>
		                <li>Celebración de un contrato entre usted y nosotros o toma de medidas, a petición suya, para celebrar dicho contrato </li>
		                <li>Nuestros intereses legítimos en la gestión adecuada de nuestro sitio web y nuestro negocio </li>
		              </ul>
		        </td>
		        <td>
		          Hasta que retire su consentimiento para comunicaciones de marketing directo, su información ya no será necesaria para fines de marketing. 
		        </td>
		      </tr>
		      <tr>
		        <td>Gestión de proveedores (incluyendo contabilidad, etc.) </td>
		        <td>
		            <ul>
		                <li>Datos de identificación (nombre, dirección, teléfono, etc.)</li>
		                <li>Características financieras (número de cuenta bancaria, detalles de la tarjeta de crédito, etc.)</li>
		                <li>Datos de identificación electrónica (correo electrónico, direcciones IP, geolocalización, datos de uso del sitio web, cookies, etc.)</li>
		                <li>Características personales de los interesados y, en algunos casos, familiares (edad, sexo, estado civil, etc.)</li>
		              </ul>
		        </td>
		        <td>
		          <ul>
		            <li>Celebración de un contrato entre usted y nosotros o toma de medidas, a petición suya, para celebrar dicho contrato </li>
		            <li>Nuestros intereses legítimos en la gestión adecuada de nuestro sitio web y nuestro negocio </li>
		            <li>Obligación legal de tratar sus datos</li>
		          </ul>
		        </td>
		        <td>
		          Hasta 10 años después de la fecha del último pedido o de la finalización del contrato, a menos que se requiera un período más largo por ley o por litigio
		        </td>
		      </tr>
		      <tr>
		        <td>Mantener la seguridad de nuestros empleados y activos  </td>
		        <td>
		            <ul>
		                <li>Datos de identificación (nombre, dirección, teléfono, etc.)</li>
		                <li>Datos de identificación electrónica (correo electrónico, direcciones IP, geolocalización, datos de uso del sitio web, cookies, etc.)</li>
		                <li>Características financieras (número de cuenta bancaria, detalles de la tarjeta de crédito, etc.)</li>
		                <li>Características personales de los interesados y, en algunos casos, familiares (edad, sexo, estado civil, etc.)</li>
		              </ul>
		        </td>
		        <td><ul>
		            <li>Celebración de un contrato entre usted y nosotros o toma de medidas, a petición suya, para celebrar dicho contrato </li>
		            <li>Obligación legal de tratar los datos de forma segura</li>
		            <li>Nuestros intereses legítimos en la gestión adecuada de nuestro sitio web y nuestro negocio</li>
		          </ul>
		        </td>
		        <td>
		          Hasta 10 años después de la fecha del último pedido o de la finalización del contrato, a menos que se requiera un período más largo por ley o por litigio 
					</td>
		      </tr>
		      <tr>
		        <td>Cumplimiento legal y gestión de litigios</td>
		        <td>
		          <ul>
		            <li>Datos de identificación (nombre, dirección, teléfono, etc.)</li>
		            <li>Datos de identificación electrónica (correo electrónico, direcciones IP, geolocalización, datos de uso del sitio web, cookies, etc.)</li>
		            <li>Características financieras (número de cuenta bancaria, detalles de la tarjeta de crédito, etc.)</li>
		            <li>Características personales de los interesados y, en algunos casos, familiares (edad, sexo, estado civil, etc.)</li>
		          </ul>
		        </td>
		        <td> <ul>
		            <li>Tratamiento necesario para el establecimiento, ejercicio o defensa de reclamaciones u obligaciones legales.   </li>
		            <li>Celebración de un contrato entre usted y nosotros o toma de medidas, a petición suya, para celebrar dicho contrato </li>
		            <li>Obligación legal de tratar los datos</li>
		            <li>Nuestros intereses legítimos en la gestión adecuada de nuestro sitio web y nuestro negocio </li>
		          </ul>
		        </td>
		        <td>Hasta 10 años después de que termine el cumplimiento legal u otra obligación o después de la resolución final del litigio  </td>
		      </tr>
		      <tr>
		        <td>Mejora de productos/servicios e inteligencia empresarial</td>
		        <td>
		         <ul>
		           <li>Datos de identificación (nombre, dirección, teléfono, etc.)</li>
		           <li>Datos de identificación electrónica (correo electrónico, direcciones IP, geolocalización, datos de uso del sitio web, cookies, etc.)</li>
		           <li>Características financieras (número de cuenta bancaria, detalles de la tarjeta de crédito, etc.)</li>
		           <li>Características personales de los interesados y, en algunos casos, familiares (edad, sexo, estado civil, etc.)</li>
		         </ul>
		        </td>
		        <td> <ul>
		            <li>Su consentimiento</li>
		            <li>Nuestros intereses legítimos en la gestión adecuada de nuestro sitio web y nuestro negocio </li>            
		          </ul>
		        </td>
		        <td>Hasta 10 años a partir de la interrupción del producto</td>
		      </tr>
		    </table>
		    <br>
		    <div id="asterisks">* Los períodos de retención varían según el tipo de documento que contenga los Datos personales. Para obtener más información sobre los períodos de retención, póngase en contacto con nosotros en 
		      <a href="data-privacy-enquiry-form_es.html">Consulta de privacidad de datos</a>.
		    <br></br>** Si quiere oponerse al uso de sus Datos personales para estos fines, consulte la sección “Sus derechos sobre acceso y control de datos personales” a continuación.</div>
		    <br>
		    <div>Conservaremos sus Datos personales mientras le proporcionamos nuestros productos y servicios. A partir de entonces, conservaremos sus Datos personales por uno de estos motivos: (a) para responder a cualquier pregunta, queja o reclamación hecha por usted o en su nombre; (b) para demostrar que le tratamos de forma justa; (c) para la gestión adecuada de nuestro negocio; o (d) para mantener registros según lo exija la ley.
		        <br></br>
		        No conservaremos sus Datos personales durante más tiempo del necesario para los fines para los que se recogieron tal como se establece en esta Declaración de privacidad. A la hora de determinar los períodos de retención de datos, tenemos en cuenta las leyes locales, las obligaciones contractuales, nuestros requisitos empresariales razonables y sus expectativas y requisitos. Cuando ya no sea necesario conservar sus Datos personales, los eliminaremos de forma segura o los anonimizaremos.
		    </div>

		    <h2 id="4">4.CÓMO COMPARTIMOS SUS DATOS PERSONALES</h2>
			<div>Sus Datos personales pueden compartirse con otros como se describe a continuación: 
		      <ul>
		        <li>Podemos divulgar sus Datos personales a cualquier miembro de nuestro grupo de empresas, filiales, revendedores, distribuidores y proveedores de servicios, incluidas las redes de publicidad, según sea razonablemente necesario para los fines y las bases legales establecidas en esta Declaración de privacidad. Aunque no consideramos dicha divulgación a los proveedores de servicios para estos fines una “venta” de información, entendemos y revelamos que puede interpretarse como tal según ciertas leyes aplicables.</li>
		        <li>Podemos divulgar sus Datos personales a nuestros aseguradores y asesores profesionales, según sea razonablemente necesario con el fin de obtener o mantener cobertura de seguros, gestionar riesgos, obtener asesoramiento profesional o para el establecimiento, ejercicio o defensa de reclamaciones legales, ya sea en procedimientos judiciales o en otros procedimientos legales.</li>
		        <li>Podemos divulgar sus Datos personales a nuestros proveedores de servicios de pago para realizar transacciones financieras. Compartiremos datos de transacciones con nuestros proveedores de servicios de pago únicamente en la medida necesaria para el procesamiento de sus pagos, el reembolso de los mismos y la gestión de reclamaciones y consultas relacionadas con dichos pagos y reembolsos.</li>
		        <li>Además de las divulgaciones específicas de los Datos personales establecidos en esta sección, podemos divulgar sus Datos personales: (a) cuando dicha divulgación sea necesaria para el cumplimiento de una obligación legal a la que estemos sujetos, incluida una citación, orden judicial u orden de búsqueda; (b) para exigir el cumplimiento de nuestros Términos de servicio, esta Declaración de privacidad u otros contratos con usted, incluyendo la investigación de posibles infracciones de los mismos; (c) para responder a las reclamaciones relativas a violaciones de los derechos de terceros; (d) responder a sus solicitudes de atención al cliente; o (e) para proteger los derechos, la propiedad o la seguridad personal de Newell Brands y sus empleados, sus agentes y filiales, sus usuarios o el público. Esto incluye intercambiar información con otras empresas y organizaciones para la protección contra fraudes, la prevención de programas dañinos/de reenvío de mensajes basura o para proteger sus intereses vitales o los intereses vitales de otra persona física.
		        </li>
		      </ul>
		    </div>
		    <div>Nos reservamos el derecho a transferir sus Datos personales en caso de quiebra o una venta o transferencia de todo o parte de nuestro negocio o activos, una adquisición, fusión o desinversión. </div>
		    <br>
		   <div>Salvo que se establezca lo contrario en el presente documento, no vendemos, comerciamos, alquilamos ni compartimos de ningún otro modo sus Datos personales con terceros ajenos a Newell Brands o con nuestras empresas afiliadas a cambio de una contraprestación económica o de otro tipo.  
		  </div>  
		    <h2 id="5">5.DATOS PERSONALES SENSIBLES  </h2>

		    <div>A menos que se le solicite específicamente, le pedimos que no nos envíe, y que no divulgue, en o a través de los Sitios web, ni a nosotros, Datos personales sensibles (p. ej., religión, etnia, opiniones políticas, creencias ideológicas u otras creencias, datos sobre salud, biometría o características genéticas, antecedentes penales, afiliación sindical, procedimientos administrativos o penales y sanciones) a menos que nosotros lo solicitemos específicamente o lo exija la ley.
		      </div>
		  
		    <h2 id="6">6.TRANSFERENCIAS DE DATOS INTERNACIONALES</h2>

		    <div>Newell Brands es un negocio mundial con sede en Estados Unidos. Newell Brands, y nuestras empresas afiliadas, cuentan con oficinas e instalaciones en ubicaciones de todo el mundo, como la Unión Europea/EEE, Latinoamérica, Asia, África, etc. Sus Datos personales podrán transferirse a otras entidades de Newell Brands, así como a proveedores de servicios externos que puedan tratar sus Datos personales en nuestro nombre, incluidos proveedores de servicios de marketing electrónico, proveedores de alojamiento, etc. </div>
		<br>
		    <div>Nuestros clientes y usuarios de nuestros Sitios web pueden estar ubicados en cualquier parte del mundo. Las leyes de privacidad de algunos países pueden no proporcionar protecciones equivalentes a las de su país de residencia, y su Estado puede considerar o no que dichas protecciones de privacidad son adecuadas. Podemos tratar su información en, o transferir su información a, los Estados Unidos, terceros en los Estados Unidos u otros países.  </div>
		    <br></br>
		    <div>Para los residentes de países de la UE/EEE, en el caso de transferencia de sus Datos personales a un país que se haya considerado que tiene un nivel inadecuado de protección para los Datos personales, implementaremos medidas de seguridad, incluida la selección de encargados del tratamiento certificados de “Escudo de privacidad” o poniendo en práctica cláusulas contractuales tipo, de conformidad con los requisitos de la UE. Puede solicitar más información sobre dichas medidas poniéndose en contacto con nosotros en
		      <a href="data-privacy-enquiry-form_es.html">Consulta de privacidad de datos</a>.
		  </div>

		    <h2 id="7">7.SUS DERECHOS SOBRE EL ACCESO Y EL CONTROL DE LOS DATOS PERSONALES</h2>
		      <div>Sus derechos sobre sus Datos personales dependen de la legislación local en la jurisdicción donde resida.  </div> <br>
		      <div> Si reside en la UE/EEE, puede tener derecho, después de enviar una solicitud validada, para:</div>
		      <ul>
		        <li><u><i>•	Información y acceso:  </i></u> puede solicitar acceso a sus Datos personales, recibir información complementaria sobre sus Datos personales o recibir una copia de sus Datos personales.</li>
		        <li><u><i>•	Rectificación: </i></u> puede solicitar rectificar o modificar sus Datos personales que sean inexactos o que no estén actualizados.</li>
		        <li><u><i>•	Supresión: </i></u> puede tener derecho a suprimir sus Datos personales.   </li>
		        <li><u><i>•	Limitación: </i></u> puede tener derecho a limitar el tratamiento de sus Datos personales. </li>
		        <li><u><i>•	Oposición al tratamiento: </i></u> puede tener derecho a oponerse a tipos específicos de tratamiento de sus Datos personales.  </li>
		        <li><u><i>•	Portabilidad de datos: </i></u> puede tener derecho a solicitar una copia portátil de sus Datos personales. Por ejemplo, la Portabilidad de los datos no se aplica a los registros en papel, y no debe perjudicar los derechos de los demás o información sensible de la empresa.   </li>
		        <li><u><i>•	Derecho a no estar sujeto/a a decisiones basadas únicamente en la toma de decisiones automatizada:</i></u> puede tener derecho a no estar sujeto/a a decisiones basadas únicamente en el tratamiento automatizado (es decir, sin intervención humana).</li>
		        <li><u><i>•	Derecho a presentar una queja:</i></u>puede tener derecho a presentar una queja ante una autoridad de control en el país en el que reside, cuyos detalles pueden ser proporcionados a petición del Comité de privacidad de datos en el Formulario web en línea, o la autoridad de control principal de Newell Brands, a saber Oficina del Delegado de Información de Wycliff House, Water Lane, Wilmslow, Cheshire SK9 6AF, Reino Unido,
		          <a href="https://ico.org.uk/">www.ico.org.uk</a>.
		        </li>
		      </ul>
		    <div>Si desea ejercer cualquiera de estos derechos, formule una <a href="data-subject-request-form_es.html">Solicitud de acceso del interesado</a>. </div><br>
		    <div>Newell Brands puede cobrar una tarifa razonable si su solicitud no es válida según la legislación aplicable, es repetitiva o excesiva. También podemos solicitarle información para comprobar su identidad antes de que usted reciba una respuesta. </div>
		    <br><div>Si reside en California, puede tener derecho, después de enviar una solicitud validada, para:</div>
		    <div>
		    <ul>
		      <li>conocer las categorías de información personal que recogemos sobre usted;  </li>
		      <li>saber si su información personal fue vendida o divulgada a terceros con fines comerciales, incluidos los fines de marketing directo por terceros, durante el año natural inmediatamente anterior;</li>
		      <li>conocer la identidad de los terceros que recibieron su información personal con fines de marketing directo durante ese año natural;</li>
		      <li>eliminar la información personal relativa a usted que nosotros guardamos;</li>
		      <li>excluirse voluntariamente de la venta de su información personal;</li>
		      <li>acceder a su información personal y a una copia portátil de sus datos;  </li>
		      <li>no ser discriminado por ejercer sus derechos, y</li>
		      <li>ser notificado/a de sus derechos.</li>
		    </ul>
		  </div>

		  <div>
		    Si desea ejercer cualquiera de estos derechos, envíe su solicitud a<a href="data-subject-request-form_es.html">Solicitud de acceso del interesado</a>
		    <br></br>(Tenga en cuenta que los consumidores de California pueden ejercer estos derechos validados a partir del 1 de
		    enero de 2020).
		    <br></br>Si prefiere no compartir su información personal con ningún fin que no sea facilitar una transacción con
		    nosotros, formule una
		    <a href="data-privacy-enquiry-form_es.html">Consulta de privacidad de datos</a>.
		    <br></br>Para excluirse de la venta de su información personal, haga clic aquí: <a href="data-subject-request-form_es.html">NO VENDER MI INFORMACIÓN PERSONAL</a>.
		    <br></br><b><i>Esta Declaración de privacidad no crea, amplía ni modifica ningún derecho de los interesados de la UE,
		        derechos del consumidor de California ni obligaciones de Newell Brands, excepto según lo dispuesto por el RGPD y la
		        Ley de Privacidad del Consumidor de California (CCPA, por sus siglas en inglés). </i></b>
		    </div>

		    <h2 id="8">8.INFORMACIÓN DE CONTACTO DEL RESPONSABLE DEL TRATAMIENTO Y LA PROTECCIÓN DE DATOS DE LA UE Y DEL EEE  </h2>
		    <div>Puede encontrar una lista y datos de contacto de las entidades del Responsable de datos de Newell Brands y más información relacionada con las autoridades de control, en la página siguiente <a href="controllers_es.html">Responsables del tratamiento y autoridades de control de Newell Brands.  </a> 
		        <br></br>Cuando el Responsable del tratamiento de Newell Brands esté ubicado fuera de la UE/EEE, el representante de la UE del Responsable del tratamiento se especifica en la página <a href="controllers_es.html">Responsables del tratamiento y autoridades de control de Newell Brands.</a>
		        <br></br>Puede ponerse en contacto con el Director de Protección de Datos de Newells en:<br>
		    </br><U><a href="data-privacy-enquiry-form_es.html">Consulta de privacidad de datoso</a></U>:<br>
		    A la atención de: Director de protección de datos
		      <br>6655 Peachtree Dunwoody Road
		      <br>Atlanta, Georgia 30328, EE. UU.
		     </div>
		    <br>
		    <div>Para obtener más información sobre el alcance total de sus derechos o para ejercerlos, póngase en contacto con nuestro Director de protección de datos utilizando la información de contacto anterior o formulando una <a href="data-privacy-enquiry-form_es.html">Consulta de privacidad de datoso</a> la autoridad de control principal de Newell Brands, a saber, la Oficina del Delegado de Información de Wycliff House, Water Lane, Wilmslow, Cheshire SK9 6AF, Reino Unido, <a href="https://ico.org.uk/">www.ico.es</a>.</div>

		    <h2 id="9">9.DATOS PERSONALES ENVIADOS POR NIÑOS  </h2>

		    <div>Algunos de los contenidos de nuestros Sitios web pueden dirigirse a niños menores de 13 años. Sin embargo, no recogemos ni solicitamos intencionadamente información personal de menores de 13 años sin consentimiento de los padres, a menos que lo permita la ley. Si nos damos cuenta de que hemos recogido información personal de un niño menor de 13 años sin consentimiento paterno o a menos que la ley lo permita, la eliminaremos de conformidad con la legislación aplicable. Si cree que un niño nos ha proporcionado Datos personales sin consentimiento paterno o de un modo en que la ley no lo permite, póngase en contacto con nosotros como se detalla a continuación en la sección “Póngase en contacto con nosotros”.
		    <br></br><u><i>Consumidores de California: </i></u>no venderemos la información de los consumidores de California que tengan 16 años o más sin autorización previa. Nuestros sitios web y servicios dirigidos a niños comprobarán la edad de un usuario y ofrecerán opciones sobre la venta de información personal relacionada con niños de 16 años o menores. Si un consumidor de California tiene menos de 13 años, un padre o tutor debe dar su consentimiento para la venta de la información personal del menor. Sin embargo, si el consumidor de California tiene entre 13 y 16 años de edad, el menor puede dar su consentimiento para la venta de su información personal. Nuestros sitios web dirigidos a los niños solicitarán la comprobación de la edad y requerirán el consentimiento adecuado.   
		    <br></br><u><i>Residentes de la UE:  </i></u>no recogemos ni tratamos datos personales relacionados con interesados de la UE menores de 16 años sin consentimiento explícito de un padre o tutor.  Nuestros sitios web dirigidos a los niños solicitarán la comprobación de la edad y requerirán el consentimiento según corresponda.    
		    </div>
		    
		    <h2 id="10">10.COOKIES, FAROS WEB Y OTRAS TECNOLOGÍAS DE RASTREO  </h2>
		    <div>Nuestros Sitios web utilizan Cookies. Las cookies son archivos de texto que contienen pequeñas cantidades de información que se descargan en su dispositivo cuando visita un sitio web. Las cookies son útiles porque permiten que un sitio web reconozca el dispositivo de un usuario. Las cookies ayudan con diversas funciones del sitio web, lo que incluye permitir a los usuarios “iniciar sesión” en sitios web (hacer que su navegación de sitios web sea más eficiente), recordar sus preferencias (como el idioma) y, por lo general, mejorar su experiencia en nuestros Sitios web. Las cookies también pueden ayudar a garantizar que los anuncios y el contenido que vea en línea sean más relevantes para usted y sus intereses. También podemos incluir faros web en mensajes de correo electrónico de marketing o nuestros boletines de noticias para determinar si los mensajes se han abierto y se ha hecho clic en los vínculos que estos mensajes contienen.
		    </div><br>
		    <div>Algunos de nuestros socios comerciales establecen faros web y cookies en nuestro sitio web. Además, los botones de redes sociales de terceros pueden registrar cierta información como la dirección IP, el tipo de navegador y el idioma, el tiempo de acceso y las direcciones del sitio web de referencia. Además, si ha iniciado sesión en esos sitios de redes sociales, también puede vincular dicha información recogida con su información de perfil en ese sitio.
		        <br></br>Si desea excluirse de las cookies, visite www.aboutcookies.org o www.allaboutcookies.org para obtener más información. Estos sitios web también le explican cómo configurar su navegador para no aceptar cookies y cómo eliminar cookies de su navegador. Sin embargo, en algunos casos, algunas de las funciones de nuestros Sitios web pueden dejar de funcionar como resultado de esto. Puede encontrar más información sobre nuestro uso de cookies en nuestra
		        <a href="cookie_es.html">Política de cookies</a>.
		    </div>

		    <h2 id="11">11.SEGURIDAD</h2>
		    <div>
		      No existe la seguridad perfecta. Hemos implementado salvaguardas organizativas, técnicas y administrativas adecuadas y comercialmente razonables para proteger los Datos personales. Usted es responsable de mantener el secreto de cualquier credencial que pueda utilizarse para acceder a cualquier cuenta o servicio de Newell. Si sospecha de alguna actividad no autorizada, comuníquenoslo inmediatamente. Puede informar de actividades sospechosas o de otras inquietudes poniéndose en contacto con nosotros en:
		        <br></br><a href="data-privacy-enquiry-form_es.html">Consulta de privacidad de datos</a>
		    </div>

		    <h2 id="12">12.ENLACES A SITIOS WEB DE TERCEROS</h2>
		    <div>
		      Podemos recoger su información de sitios de terceros. Esto incluye cuando inicie sesión a través de un sitio web de terceros como Facebook, Snapchat, Instagram, Linkedin, Twitter, etc. También podemos divulgar información recogida de terceros para personalizar anuncios y gestionar y facilitar mensajes en sitios web de terceros. Tenga en cuenta que al cancelar su suscripción a nuestro correo electrónico de marketing puede haber un retraso en la interrupción de nuestros anuncios dirigidos a usted en sitios web de terceros. 
		    </div>
		    <h2 id="13">13.NO RASTREO </h2>
		    <div>
		      Newell no realiza un rastreo de sus clientes a lo largo del tiempo y de sitios web de terceros para proporcionar publicidad dirigida y, por tanto, no responde a las señales de no rastreo, Do Not Track (DNT, por sus siglas en inglés). Sin embargo, algunos sitios de terceros realizan un seguimiento de sus actividades de navegación cuando le sirven contenido, lo que les permite adaptar lo que le presentan a usted. Si visita dichos sitios, es posible que pueda configurar la señal DNT de su navegador para que terceros (especialmente los anunciantes) sepan que no desea que se le rastree.
		    </div>

		    <h2 id="14">14.USO DE DISPOSITIVOS MÓVILES Y OTROS DISPOSITIVOS</h2>
		    <div>
		      Newell, nuestras filiales y terceros que hemos contratado, pueden recoger y almacenar identificadores únicos que coincidan con su dispositivo móvil con el fin de ofrecer anuncios o contenido personalizados mientras usted utiliza nuestros Sitios web o navega por Internet; para ofrecer servicios y publicidad basados en la ubicación o para identificarle de forma única a través de dispositivos o navegadores. Para personalizar estos anuncios o contenido, nosotros o terceros pueden recoger Datos personales, p. ej., su dirección de correo electrónico o datos recogidos de usted, como su identificador de dispositivo, ubicación o dirección IP. La mayoría de los dispositivos móviles le permiten desactivar los servicios de ubicación. 
		        <br><br>Al ajustar la configuración de privacidad y seguridad de su dispositivo, puede administrar la forma en que su dispositivo móvil y su navegador móvil comparten información, así como la forma en que su navegador móvil gestiona las cookies y otros identificadores de rastreo. Consulte las instrucciones suministradas por su proveedor de servicios móviles o el fabricante de su dispositivo para saber cómo ajustar su configuración.
		    </div>

		    <h2 id="15">15.COMUNICACIONES DE MARKETING POR CORREO ELECTRÓNICO  </h2>
		    <div>
		      Si usted es un cliente registrado o, si lo requiere la legislación aplicable, ha proporcionado su consentimiento o no nos ha indicado de otro modo que no le enviemos mensajes de marketing, podemos enviarle mensajes de marketing por correo electrónico o correo postal para mantenerle informado/a de nuestros productos y servicios. 
		        <br></br>Puede dejar de recibir mensajes de marketing en cualquier momento mediante los siguientes métodos:
		        <ul>
		          <li>A través de los ajustes/preferencias de su cuenta de cliente en “Mi cuenta”;</li>
		          <li>Haciendo clic en el enlace “cancelar suscripción” en cualquier comunicación por correo electrónico que le enviamos</li>
		          <li>Poniéndose en contacto con nosotros en   <a href="data-privacy-enquiry-form_es.html">Consulta de privacidad de datos.</a></li>
		        </ul>
		        Una vez completado uno de estos métodos, cumpliremos su solicitud y actualizaremos su perfil para garantizar que no reciba más mensajes de marketing.
		        <br></br>Procesaremos solicitudes de exclusión/cancelación de suscripción lo antes posible y, en la mayoría de los casos, en un plazo de 10 días. Sin embargo, Newell Brands está constituida por una compleja red de muchas empresas interconectadas y proveedores de servicios, por lo que puede llevar tiempo que todos nuestros sistemas reflejen estas actualizaciones. Le pedimos que comprenda que durante este período puede que aún le enviemos mensajes mientras su solicitud se está procesando.  
		        <br></br>Tenga en cuenta que la interrupción de los mensajes de marketing no detendrá ninguna comunicación de servicio, como actualizaciones de pedidos.
		    </div>

		    <h2 id="16">16.MENSAJES DE TEXTO </h2>
		    <div>
		      Nuestros Sitios web pueden ofrecerle la opción de recibir mensajes de texto y alertas en los números de teléfono móvil que ha compartido con nosotros. Una vez que se haya suscrito, podremos enviarle mensajes de texto sobre su cuenta, para investigar o prevenir el fraude, y para alertarle en caso de urgencia, etc. Es posible que le enviemos mensajes de texto y alertas mediante tecnología de marcado automático. Además, de vez en cuando, podemos realizar ciertas campañas que le brindan la posibilidad de enviar un mensaje de texto a un código corto específico y proporcionarle la opción de recibir mensajes de texto en el futuro. Su solicitud de suscripción nos proporciona su número de teléfono móvil. Si decide suscribirse, acepta que, ocasionalmente, podemos enviar mensajes al dispositivo móvil asociado con ese número sobre nuevos productos, promociones, servicios u ofertas. 
		        <br></br>Puede optar por excluirse de nuestros mensajes de texto y alertas en cualquier momento respondiendo “STOP” al mensaje que recibió. Una vez que se haya excluido, no recibirá mensajes de texto adicionales a través de su teléfono móvil. Tenga en cuenta que si opta por dejar de recibir mensajes de texto y alertas, es posible que no podamos ponernos en contacto con usted con mensajes importantes sobre su cuenta o transacciones con nosotros. Sin embargo, si hay una urgencia o una pregunta sobre la cuenta, haremos todo lo posible para ponernos en contacto con usted de otras maneras, como por correo electrónico o a través de un teléfono fijo. No tiene que suscribirse a mensajes de texto y alertas para utilizar nuestros Sitios web y servicios. Si opta por suscribirse, es posible que se apliquen cargos estándar por mensajes de texto. Para obtener más información sobre nuestros mensajes de texto y alertas, póngase en contacto con nosotros en <a href="data-privacy-enquiry-form_es.html">Consulta de privacidad de datos.</a>
		    </div>

		    <h2 id="17">17.CAMBIOS EN NUESTRA DECLARACIÓN DE PRIVACIDAD </h2>
		    <div>
		      Nos reservamos el derecho, a nuestra entera discreción, de cambiar, modificar, añadir o eliminar partes de esta política de privacidad en cualquier momento. Notificaremos dichos cambios de acuerdo con la legislación de protección de datos aplicable. 
		        <br></br>Cualquier cambio sustancial en esta Política de privacidad se publicará en esta página web. Es su responsabilidad revisar nuestra Política de privacidad cada vez que nos proporcione Datos personales, ya que la Política de privacidad puede haber cambiado desde la última vez que utilizó nuestros Sitios web o servicios.
		    </div>

		    <h2 id="18">18.PÓNGASE EN CONTACTO CON NOSOTROS</h2>
		      <div>
		        Si tiene alguna pregunta sobre esta Política de privacidad o sobre las prácticas de Newells, póngase en contacto con nosotros en:  <a href="data-privacy-enquiry-form_es.html">Consulta de privacidad de datos</a>.
		      </div>

		      <h2 id="19">19.NOVEDADES  </h2>
		      <div>
		        A partir del 15 de noviembre de 2019, actualizamos y homogeneizamos partes de nuestra Declaración de privacidad. Este es un resumen de algunos de los cambios: 
		          <br>
		          <ul>
		            <li>Ahora utilizaremos el término “Sitios web” para describir nuestro sitio web, aplicaciones móviles, etc.</li>
		            <li>Aclaramos los tipos de Datos personales que recogemos sobre usted y las formas en que recogemos esta información.</li>
		            <li>Describimos sus derechos de acceso y control sobre sus Datos personales.  </li>
		            <li>Proporcionamos información de contacto actualizada.</li>
		            <li>Describimos cómo podemos utilizar y compartir sus Datos personales</li>
		            <li>Aclaramos los tipos de Datos personales que recogemos sobre usted y las formas en que recogemos estos Datos personales. Recogemos Datos personales de muchas formas, como sitios de terceros, sitios de redes sociales y redes de publicidad.</li>
		            <li>Describimos cómo compartimos y usamos sus Datos personales.  </li>
		            <li>Describimos nuestro uso de comunicaciones por correo electrónico y mensajes de texto, así como cómo cancelar o controlar estas comunicaciones.</li>
		            <li>Notificamos a los usuarios sobre sus derechos en virtud del RGPD y de la Ley de Privacidad del Consumidor de California, cuando corresponda.</li>
		            <li>Aclaramos que sus Datos personales pueden transferirse a través de fronteras internacionales para su tratamiento y otros usos.</li>
		            <li>Aclaramos cómo compartimos sus Datos personales con nuestros proveedores, distribuidores externos y sitios de referencia.</li>
		            <li>Incluimos información adicional sobre señales de no rastreo (DNT).</li>
		          </ul>

		      </div>';

    $lang['condiciones_txt'] = '<div class="legal">
    <p><b>INFORMACIÓN LEGAL</b></p>
    <p>Sunbeam Latin America, LLC (“SLA”) mantiene este "Sitio Web" para su entretenimiento, información, educación y comunicación. Le invitamos a que navegue en nuestro Sitio Web. Usted puede bajar el material que ahí se muestra para su uso personal no comercial, siempre y cuando respete y se sujete a los derechos de autor y otros avisos sobre propiedad estipulados en los materiales. Sin embargo, no está permitido distribuir, modificar, transmitir, rehusar, reenviar o utilizar el contenido del Sitio Web con propósitos públicos o comerciales, incluyendo textos, imágenes, audio o video, sin la autorización escrita de SLA.</p>
    <p>El acceso y uso del Sitio Web está sujeto también a los siguientes términos y condiciones (Términos y Condiciones) y a las leyes que apliquen. Al accesar y navegar en la Página, usted acepta, sin limitación o restricción alguna (i) los "Términos y Condiciones" contenidas en la siguiente sección, y admite que cualesquiera otros acuerdos entre usted y SLA relacionados específicamente con su Sitio Web quedan enteramente revocados y sin efecto o validez, (ii) que usted es mayor de edad y está en plena capacidad de aceptar quedar sujeto a estos Términos y Condiciones, y (iii) si usted es una compañía u otra entidad, usted está en la capacidad de obligar a dicha compañía o entidad.</p>
    <p>Términos y Condiciones</p>
    <p>1. Usted asumirá que todo lo que usted vea o lea en el Sitio Web tiene derechos de autor o está protegido como marca registrada, a no ser que expresamente se especifique lo contrario, y no podrá ser utilizado, sino como se estipula en estos Términos y Condiciones o en el texto del Sitio Web, sin el consentimiento previo y por escrito de SLA.</p>
    <p>2. Sin embargo,  SLA hará los esfuerzos razonables para incluir una información actualizada y veraz en el Sitio Web. SLA no garantiza, ni se hace responsable por su exactitud. SLA no asumirá responsabilidades por errores u omisiones en el contenido del Sitio Web.</p>
    <p>3. Las imágenes de personas o lugares, textos y elementos gráficos constitutivos del Sitio Web difundidos a través de éste, así como su presentación y montaje, son de titularidad exclusiva de SLA u ostenta los derechos de explotación de éstos a través de acuerdos con terceros. Asimismo, todos los nombres comerciales, marcas o signos distintivos, logotipos, símbolos, marcas mixtas, figurativas o nominativas que aparecen en este Sitio Web pertenecen a Sunbeam Products, Inc., a sus subsidiarias, afiliadas o empresa matriz y otras  (colectivamente “Sunbeam”) o se ostenta las correspondientes licencias de uso sobre los mismos. El uso de estas imágenes por usted u otra persona autorizada por usted, está prohibido, a menos que específicamente lo permitan estos Términos y Condiciones o se provea un consentimiento específico en otro lugar del Sitio Web. Toda reproducción o presentación, incluso parcial, del contenido de este Sitio Web o de uno de sus elementos, así como cualquier uso no autorizado de las imágenes, puede violar leyes de derechos de autor, de marcas registradas, de privacidad y publicidad, quedará prohibida siempre que no se cuente con la autorización expresa y por escrito de Sunbeam.</p>
    <p>4. Nada de lo contenido en el Sitio Web debe ser interpretado como una concesión, o un permiso o derecho para utilizar cualquier Marca Registrada exhibida en el Sitio Web, sin el consentimiento, por escrito, de Sunbeam o del tercero que posea la Marca Registrada exhibida en el Sitio Web. El uso indebido de las marcas exhibidas en el Sitio Web, o cualquier otro contenido del Sitio Web, con excepción de las provistas en estos Términos y Condiciones, está estrictamente prohibido. Se notifica, que Sunbeam o cualquier tercero que para tal efecto ella designe, hará valer agresivamente sus derechos de propiedad intelectual y procurará que se aplique todo el peso de la ley, incluyendo las vías civil y penal.</p>
    <p>5. Queda entendido que Usted utiliza y navega en el Sitio Web bajo su propio riesgo.  SLA y ninguna otra parte involucrada en su creación, producción o montaje, son responsables por cualquier daño directo, incidental, consecuente con, o indirecto, resultante de su simple acceso (entrada) o uso del Sitio Web. Sin exclusión de lo anterior, todo en el Sitio Web se provee COMO ESTÁ, SIN GARANTÍA DE NINGUNA CLASE, NI EXPRESA, NI IMPLÍCITA. SLA NO ASUME RESPONSABILIDAD Y NO PODRÁ SER ACUSADA POR DAÑOS INFORMÁTICOS CAUSADOS POR VIRUS, GUSANOS, TROYANOS O CUALESQUIER OTRO CÓDIGO MALICIOSO QUE PUDIESEN INFECTAR SU COMPUTADORA U OTRO MEDIO INFORMÁTICO COMO CONSECUENCIA O POR CUENTA DE SU ACCESO, USO, O NAVEGACIÓN EN EL SITIO WEB O POR BAJAR MATERIALES, DATOS, TEXTOS, IMÁGENES, VIDEOS O AUDIOS DE LA MISMA.</p>
    <p>6. Este Sitio Web tiene la intención de asistir al lector con información acerca de nuestros productos. SLA,  no es responsable por cualesquiera errores u omisiones en el material, información o recomendación provista en el Sitio Web, ni por cualquier pérdida o daño causados por su credibilidad en la información obtenida en el Sitio Web o por una Página enlazada o ligada desde nuestra página electrónica. Antes de realizar la compra de cualquiera de nuestros productos, aconsejamos a nuestros lectores consultar con un distribuidor autorizado de nuestros productos.</p>
    <p>7. Cualquier comunicación o material que usted transmita al Sitio Web vía E-mail, o de otro modo, incluyendo cualquier información, pregunta, comentario, sugerencia o similar, será tratado como no confidencial y sin propiedad exclusiva.  Lo anterior no incluye datos personales.</p>
    <p>8. SLA no ha revisado todas las páginas con enlace al Sitio Web, y no es responsable por páginas exteriores o cualquier otra página enlazada con este Sitio Web. El enlace con cualquier otra página exterior u otras páginas es a su propio riesgo.</p>
    <p>9. Aunque SLA pudiera, ocasionalmente, monitorear o revisar las discusiones, chats, correos, transmisiones, boletines y similares en el Sitio Web, SLA  no está obligada a hacerlo y no asume ninguna responsabilidad por el contenido que surja de tales sitios, ni por ningún error, difamación, calumnia, omisión, falsedad, obscenidad, pornografía, profanación, daño o inexactitud contenida en alguna información dentro de tales sitios en el Sitio Web. Está prohibido enviar o transmitir cualquier información ilegal o de carácter amenazante, calumnioso, difamatorio, obsceno, escandaloso, incitador, pornográfico, o material profano o cualquier material que pudiera animar o impulsar conductas que pudiesen ser consideradas como delictivas por la ley o leyes de cualquier Estado o País, promover una guerra civil o, de otro modo, violar cualquier ley.  SLA  cooperará totalmente con cualquier autoridad ejecutora de la ley y con la orden judicial que le requiera o exija revelar la identidad del individuo o entidad que esté enviando tales informaciones o materiales en caso de ser del conocimiento de SLA.</p>
    <p>10. SLA puede, en cualquier momento, revisar estos Términos y Condiciones y actualizar esta comunicación. Usted queda sujeto a estas revisiones y por lo tanto, deberá visitar esta página periódicamente con el fin de revisar los Términos y Condiciones actuales a los que usted se obliga por el simple acceso al Sitio Web.</p>
    <p>11. En cumplimiento a lo dispuesto por las Leyes y Regulaciones de Protección al Consumidor, SLA  pone a disposición de los visitantes, clientes y usuarios de su Sitio Web los siguientes datos de contacto para atender cualquier duda o reclamación:</p>
    <p>Sunbeam Latin America, LLC, 5200 Blue Lagoon Dr., Suite 860, Miami, Florida 33126. Teléfono: 786-845-2540 Fax: 786-845-2556. Correo electrónico: PrivacidadOsterLatinoamerica.com.</p>
    <p>SLA  reitera la total disposición de proteger los datos personales de sus usuarios y visitantes. Usted tiene la facultad de corregir su información o solicitar que sea borrada en cualquier momento, a través de los datos de contacto previstos en este inciso.</p>
    <p>12. En cumplimiento a lo dispuesto por las Leyes y Regulaciones de Protección al Consumidor, SLA:</p>
    <p>I.  Utilizará la información personal proporcionada por el consumidor en forma confidencial, por lo que no podrá difundirla o transmitirla a otros proveedores ajenos a la prestación de este servicio del Sitio Web o a alguna  transacción de SLA con el usuario si fuese el caso, salvo autorización expresa del propio consumidor o por requerimiento de autoridad competente;</p>
    <p>II. Utilizará alguno de los elementos técnicos disponibles para brindar seguridad y confidencialidad a la información proporcionada por el consumidor e informará a éste, de las características generales de dichos elementos;</p>
    <p>III.  Proporcionará al consumidor, antes de celebrar una transacción, el domicilio físico, números telefónicos y demás medios a los que pueda acudir el propio consumidor para presentarle sus reclamaciones o solicitarle aclaraciones a SLA sobre la información que se encuentra bajo el punto 11 de estos Términos y Condiciones;</p>
    <p>IV. Evitará las prácticas comerciales engañosas respecto de las características de los productos, por lo que cumplirá con las disposiciones relativas a la información y publicidad de los bienes y servicios que ofrezca, señaladas en las Leyes y Regulaciones de Protección al Consumidor y demás disposiciones que se deriven de ella en cuanto fueren aplicables;</p>
    <p>V.      Dará a conocer al consumidor toda la información sobre los términos, condiciones, costos, cargos adicionales, a que hubiere lugar, formas de pago de los bienes y servicios ofrecidos por el proveedor;</p>
    <p>VI. En caso de venta de productos respetará la decisión del consumidor en cuanto a la cantidad y calidad de los productos que  desea recibir, así como la de recibir avisos comerciales;</p>
    <p>VII.  Se abstendrá de utilizar estrategias de venta o publicitarias que no proporcionen al consumidor información clara y suficiente sobre los servicios ofrecidos, en especial tratándose de prácticas de mercadotecnia dirigidas a la población vulnerable, como los niños, ancianos y enfermos, por lo que incorporará mecanismos que adviertan cuando la información no sea apta para esa población.</p>
    <p>13. La aceptación de los Términos y Condiciones quedará perfeccionada desde la fecha en que el Usuario manifieste su conformidad con los Términos y Condiciones del presente instrumento, mediante la aportación de los datos solicitados cumplimentando el formulario de suscripción incluido en el Sitio Web.</p>
    <p>14. Estos Términos y Condiciones podrán ser modificados en cualquier momento, lo que será comunicado a los Usuarios mediante la publicación respectiva en el Sitio Web. La falta de comunicación expresa por parte del Usuario, manifestando su intención de dar por finalizado el contrato, implicará su aceptación de las nuevas condiciones, que le serán aplicables a partir de la fecha en que deba de producirse la renovación.</p>
    <p>15. SLA podrá negar el acceso al Sitio Web así como los servicios y contenido sin necesidad de preaviso, a aquellos Usuarios que incumplan las condiciones establecidas en el presente instrumento.</p>
    <p>16. La prestación del servicio del Sitio Web tiene una duración indefinida. SLA, no obstante, está autorizada para dar por terminada o suspender la prestación del servicio del Sitio Web y/o de cualquiera de los Servicios en cualquier momento, sin perjuicio de lo que se hubiere dispuesto al respecto en el presente instrumento. Cuando ello sea razonablemente posible y dejando a salvo lo dispuesto en el punto 15 anterior. SLA advertirá previamente la terminación o suspensión de la prestación del servicio del Sitio Web.</p>
    <p>17. La validez, interpretación y efecto de estos Términos y Condiciones será regulada exclusivamente por las leyes de la Florida, Estados Unidos de América.</p>
    <p>18. Las partes se someten a la jurisdicción de los Tribunales del Condado de Dade, Florida. Estados Unidos de America.</p>
    <p>Derechos Reservados Términos y Condiciones Legales.</p>

   </div>';
?>