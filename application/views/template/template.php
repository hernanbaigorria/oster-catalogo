<!DOCTYPE html>
<html>
   <head>
   	  <meta charset="utf-8" />
      <title><?= $title ?></title>
	  <meta name="description" content="<?= $description ?>">
	  <meta name="keywords" content="<?= $keywords ?>">
	  <meta name='viewport' content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' />
	  
	  <meta property="og:title" content="<?= $title ?>" />
	  <meta property="og:type" content="<?= $ogType ?>" />
	  <meta property="og:url" content="<?php echo base_url(uri_string()) ?>" />
	  <meta property="og:image" content="<?= base_url().$image ?>" />
	  <meta property="og:description" content="<?= $description ?>" />
	  
	  <link rel="shortcut icon" type="image/png" href="<?php echo base_url() ?>asset/img/favicon.png?">

	  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.4/css/all.css" integrity="sha384-DyZ88mC6Up2uqS4h/KRgHuoeGwBcD4Ng9SiP4dIRy0EXTlnuz47vAwmeGwVChigm" crossorigin="anonymous">

	  <link href="https://fonts.googleapis.com/css2?family=Barlow+Condensed:wght@300;400;500;600;700;900&display=swap" rel="stylesheet">
	
	  <link rel='stylesheet' href='https://unpkg.com/xzoom/dist/xzoom.css'>

	  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
	  <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.3/assets/owl.carousel.min.css'>
	  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
	  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

	  <link rel="stylesheet" href="<?php echo base_url() ?>asset/css/custom.css?version=<?=time()?>" type="text/css" media="all" />
	  <script>var base_url = "<?php echo base_url() ?>";</script>
	  
	  <?= $_styles ?>

	  <?php
	  	$cookie= array(
	      'name'   => 'ip_cliente',
	      'value'  => $this->input->ip_address(),
	      'expire' => '999999',
	      'secure' => FALSE
	    );

	   $this->input->set_cookie($cookie);
	  ?>

   </head>
   <body>
	  <?= $header ?>        
	  <?= $content ?>
    <?= $footer ?>
	  <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js'></script>

	  <script src='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.3/owl.carousel.min.js'></script>
	  <script src='https://use.fontawesome.com/826a7e3dce.js'></script>
      
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

      <script src='https://unpkg.com/xzoom/dist/xzoom.min.js'></script>
      <script src='https://hammerjs.github.io/dist/hammer.min.js'></script>
      <script src='https://cdnjs.cloudflare.com/ajax/libs/foundation/6.3.1/js/foundation.min.js'></script>
	
	  <script defer type="text/javascript" src="<?php echo base_url() ?>asset/js/main.js?version=<?=time()?>"></script>

	  <?= $_scripts ?>


	  <script type="text/javascript">
	  	$(".form-product").submit(function(event){
	  		event.preventDefault(); //prevent default action 
	  		var post_url = $(this).attr("action"); //get form action url
	  		var request_method = $(this).attr("method"); //get form GET/POST method
	  		var form_data = $(this).serialize(); //Encode form elements for submission
	  		
	  		$.ajax({
	  			url : post_url,
	  			type: request_method,
	  			data : form_data,
	  			beforeSend: function() {
	  		        // setting a timeout
	  		        $('.enviar').addClass('disable');
	  		        $('.gif').show('slow');
	  		        $('.message').hide("slow");
	  		    },
	  		    success: function(response){
  		          	window.location.replace("<?php echo base_url().$this->config->item('language_abbr') ?>/home/gracias/");
	  		    }
	  		});
	  	});

	  	$(".form-newsletter").submit(function(event){
	  		event.preventDefault(); //prevent default action 
	  		var post_url = $(this).attr("action"); //get form action url
	  		var request_method = $(this).attr("method"); //get form GET/POST method
	  		var form_data = $(this).serialize(); //Encode form elements for submission
	  		
	  		$.ajax({
	  			url : post_url,
	  			type: request_method,
	  			data : form_data,
	  			beforeSend: function() {
	  		        // setting a timeout
	  		        $('.enviar').addClass('disable');
	  		        $('.gif').show('slow');
	  		        $('.message').hide("slow");
	  		    },
	  		    success: function(response){
  		          	window.location.replace("<?php echo base_url().$this->config->item('language_abbr') ?>/home/gracias/");
	  		    }
	  		});
	  	});

	  	$(".form-contact").submit(function(event){
	  		event.preventDefault(); //prevent default action 
	  		var post_url = $(this).attr("action"); //get form action url
	  		var request_method = $(this).attr("method"); //get form GET/POST method
	  		var form_data = $(this).serialize(); //Encode form elements for submission
	  		
	  		$.ajax({
	  			url : post_url,
	  			type: request_method,
	  			data : form_data,
	  			beforeSend: function() {
	  		        // setting a timeout
	  		        $('.enviar').addClass('disable');
	  		        $('.gif').show('slow');
	  		        $('.message').hide("slow");
	  		    },
	  		    success: function(response){
  		          	window.location.replace("<?php echo base_url().$this->config->item('language_abbr') ?>/home/gracias/");
	  		    }
	  		});
	  	});
	  	
	  </script>

	  <script type="text/javascript">
	  	$('#HeaderHome').owlCarousel({
	  	  loop: true,
	  	  margin: 10,
	  	  nav: true,
	  	  navText: [
	  	    "<i class='fa fa-caret-left'></i>",
	  	    "<i class='fa fa-caret-right'></i>"
	  	  ],
	  	  autoplay: true,
	  	  autoplayHoverPause: true,
	  	  responsive: {
	  	    0: {
	  	      items: 1
	  	    },
	  	    600: {
	  	      items: 1
	  	    },
	  	    1000: {
	  	      items: 1
	  	    }
	  	  }
	  	})
	  </script>


	  <script type="text/javascript">
	  	$('#LastViewed').owlCarousel({
	  	  loop: true,
	  	  margin: 10,
	  	  nav: true,
	  	  navText: [
	  	    "<i class='fa fa-caret-left'></i>",
	  	    "<i class='fa fa-caret-right'></i>"
	  	  ],
	  	  autoplay: true,
	  	  autoplayHoverPause: true,
	  	  responsive: {
	  	    0: {
	  	      items: 1
	  	    },
	  	    600: {
	  	      items: 1
	  	    },
	  	    1000: {
	  	      items: 4
	  	    }
	  	  }
	  	})
	  </script>

	  <script type="text/javascript">
	  	 $( document ).ready( function () {
	  	     $( '.dropdown-menu a.dropdown-toggle' ).on( 'click', function ( e ) {
	  	         var $el = $( this );
	  	         $el.toggleClass('active-dropdown');
	  	         var $parent = $( this ).offsetParent( ".dropdown-menu" );
	  	         if ( !$( this ).next().hasClass( 'show' ) ) {
	  	             $( this ).parents( '.dropdown-menu' ).first().find( '.show' ).removeClass( "show" );
	  	         }
	  	         var $subMenu = $( this ).next( ".dropdown-menu" );
	  	         $subMenu.toggleClass( 'show' );
	  	         
	  	         $( this ).parent( "li" ).toggleClass( 'show' );

	  	         $( this ).parents( 'li.nav-item.dropdown.show' ).on( 'hidden.bs.dropdown', function ( e ) {
	  	             $( '.dropdown-menu .show' ).removeClass( "show" );
	  	             $el.removeClass('active-dropdown');
	  	         } );
	  	         
	  	          if ( !$parent.parent().hasClass( 'navbar-nav' ) ) {
	  	             $el.next().css( { "top": $el[0].offsetTop, "left": $parent.outerWidth() - 4 } );
	  	         }

	  	         return false;
	  	     } );
	  	 } );
	  </script>

   </body>
</html>