<section class="header-home">
	<div class="carousel-wrap">
	  <div id="HeaderHome" class="owl-carousel">
	  	<?php foreach($slider as $slide): ?>
		    <div class="item slide-cut-content" style="background:url('<?=base_url()?>asset/img/fondo_carrousel.png');">
		    	<div class="container">
		    		<div class="row align-items-center">
		    			<div class="col-12 col-md-7">
		    				<img src="<?=base_url()?>asset/img/uploads/<?=$slide->imagen?>" class="img-fluid">
		    			</div>
		    			<div class="col-12 col-md-5">
		    				<h3><?=$slide->titulo?></h3>
		    				<p><?=$slide->descripcion?></p>
		    				<a href="<?=$slide->link?>" class="btn-slide"><?php echo $this->lang->line('learn_more'); ?></a>
		    			</div>
		    		</div>
		    	</div>
		    </div>
		<?php endforeach; ?>
	  </div>
	</div>
</section>

<section class="sect-compo-2">
	<div class="row m-0">
		<?php foreach($catalogos as $catalogo): ?>
		<div class="col-12 col-md-6 back-box" style="background:url('<?=base_url()?>asset/img/uploads/<?=$catalogo->imagen_home?>');">
			<div class="d-flex align-items-start justify-content-center flex-column back-filter-content">
				<h4><?=$catalogo->nombre?></h4>
				<a href="<?=base_url().'catalogs/'.$catalogo->slug?>" class="btn-generico"><?php echo $this->lang->line('learn_more'); ?></a>
			</div>
		</div>
		<?php endforeach; ?>
	</div>
</section>
<?php if(!empty($ultimos_visitados)): ?>
<section class="sect-products">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h3><?php echo $this->lang->line('last_view'); ?></h3>
				<div id="LastViewed" class="owl-carousel">
					<?php foreach($ultimos_visitados as $producto): ?>
						  <div class="item">
						  	<a href="<?php echo base_url() ?>catalogs/<?=$producto->slug_catalogo?>/<?=$producto->slug_categoria?>/<?=$producto->slug_subcategoria?>/<?=$producto->slug?>/" class="global-div-product">
							  	<div class="img-product" style="background:url('<?=base_url()?>asset/img/uploads/<?=$producto->imagen?>');"></div>
							  	<div class="content-info-product">
							  		<p><?=$producto->titulo?></p>
							  	</div>
							  	<div class="info-card">
							  		<h5><?=$producto->titulo?></h5>
							  		<p>
							  			<?=$producto->subtitulo?>
							  		</ul>
							  		</p>
							  	</div>
						  	</a>
						  </div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	</div>
</section>
<?php endif; ?>
<section class="section-news-products">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h3><?php echo $this->lang->line('new_products'); ?></h3>
			</div>
			<?php foreach($nuevos as $nuevo): ?>
				<div class="col-12 col-md-4">
					<div class="content-products-news d-flex w-100 align-items-center justify-content-center">
						<div class="img-product-new" style="background:url('<?=base_url()?>asset/img/uploads/<?=$nuevo->imagen?>');"></div>
						<div class="content-data">
							<h5>
								<?=$nuevo->titulo?>
							</h5>
							<a href="<?php echo base_url() ?>catalogs/<?=$nuevo->slug_catalogo?>/<?=$nuevo->slug_categoria?>/<?=$nuevo->slug_subcategoria?>/<?=$nuevo->slug?>/" class="btn-generico"><?php echo $this->lang->line('learn_more'); ?></a>
						</div>
						<div class="img-icon-abosolute-new">
							<img src="<?=base_url()?>asset/img/new-producto-icon.png" class="img-fluid">
						</div>
					</div>
				</div>
			<?php endforeach; ?>
			<?php if(false): ?>
			<div class="col-12 col-md-4">
				<div class="content-products-news d-flex w-100 align-items-center justify-content-center">
					<div class="img-product-new" style="background:url('<?=base_url()?>asset/img/producto-01-enmascarado.png');"></div>
					<div class="content-data">
						<h5>
							Oster® A6® Slim™ Purple - 3 Speeds
						</h5>
						<a href="#" class="btn-generico"><?php echo $this->lang->line('learn_more'); ?></a>
					</div>
					<div class="img-icon-abosolute-new">
						<img src="<?=base_url()?>asset/img/new-producto-icon.png" class="img-fluid">
					</div>
				</div>
			</div>
			<div class="col-12 col-md-4">
				<div class="content-products-news d-flex w-100 align-items-center justify-content-center">
					<div class="img-product-new" style="background:url('<?=base_url()?>asset/img/producto-02-enmascarado.png');"></div>
					<div class="content-data">
						<h5>
							Oster® Fast Feed® SINGLE SPEED CLIPPER
						</h5>
						<a href="#" class="btn-generico" style="background:#9BCEEF;"><?php echo $this->lang->line('learn_more'); ?></a>
					</div>
					<div class="img-icon-abosolute-new">
						<img src="<?=base_url()?>asset/img/new-producto-icon.png" class="img-fluid">
					</div>
				</div>
			</div>
			<?php endif; ?>
			<div class="col-12 col-md-4 custom-calendar">
				<div id="event-action-response"></div>
				<div id="calendar"></div>
			</div>
		</div>
	</div>
</section>

<div class="modal-events d-none align-items-center justify-content-center">
	<div class="content-modal">
		<div class="contents-principal">
			<div class="row">
				<div class="col-7">
					<h3 class="title-event">Event FPO</h3>
					<p class="description-event">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat</p>
				</div>
				<div class="col-5">
					<div class="image-events"></div>
				</div>
			</div>
		</div>
		<div class="faja-event">
			<h5>To <?php echo $this->lang->line('learn_more'); ?> visit:</h5>
			<div class="link-modal"></div>
		</div>
		<img src="<?=base_url()?>asset/img/icon-cierre-modal.png" class="img-fluid cierre-modal">
	</div>
</div>

<script type="text/javascript">
	var meses = '<?=$this->lang->line('meses')?>';
	var dias = '<?=$this->lang->line('dias')?>';
	var hoy = '<?=$this->lang->line('hoy')?>';
</script>