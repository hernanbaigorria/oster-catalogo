<?php
function highlight($text='', $word='')
{
  if(strlen($text) > 0 && strlen($word) > 0)
  {
    return (str_ireplace($word, "<span style='background: yellow;'>{$word}</span>", $text));
  }
   return ($text);
}
$words = htmlspecialchars($_GET['s']);
?>
<section class="serach-section">
    <div class="container">
        <div class="row m-0">
            <?php if(empty($productos)): ?>
            <div class="col-12 p-0">
                <h5>NO HUBO RESULTADOS</h5>
            </div>
            <?php else: ?>
                <div class="col-12 p-0">
                    <h4>Search results for “<?=htmlspecialchars($_GET['s'])?>”</h4>
                </div>
                <?php foreach($productos as $producto):?>
                    <a href="<?php echo base_url() ?>catalogs/<?=$producto->slug_catalogo?>/<?=$producto->slug_categoria?>/<?=$producto->slug_subcategoria?>/<?=$producto->slug?>/" class="col-12 box-line-search">
                        <div class="row w-100 align-items-center">
                            <div class="col-2 text-center">
                                <img src="<?=base_url()?>asset/img/uploads/<?=$producto->imagen?>" class="img-fluid">
                            </div>
                            <div class="col-10">
                                <h6><?php echo highlight($producto->titulo, $words)?></h6>
                                <p><?php echo highlight($producto->subtitulo, $words)?></p>
                            </div>
                        </div>
                    </a>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>
</section>

