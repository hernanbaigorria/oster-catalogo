<section class="header-grill-products" style="background:url('<?=base_url()?>asset/img/uploads/<?=$catalogo[0]->imagen_portada?>');">
</section>
<section class="products-detaills">
	<div class="container">
		<div class="row m-0">
			<?php foreach($productos_catalogo as $producto): ?>
			<div class="col-12 col-sm-4" style="margin-bottom: 20px;">
				<a href="<?php echo base_url() ?>catalogs/<?=$producto->slug_catalogo?>/<?=$producto->slug_categoria?>/<?=$producto->slug_subcategoria?>/<?=$producto->slug?>/" class="box d-flex align-items-center justify-content-center">
					<div class="image d-flex align-items-center flex-column w-100">
						<img src="<?php echo base_url() ?>asset/img/uploads/<?=$producto->imagen?>" class="img-fluid">
						<h4><?=$producto->titulo?></h4>
						<div class="info-card">
							<h5><?=$producto->subtitulo?></h5>
							<p>
								<?=$producto->subtitulo?>
							</ul>
							</p>
						</div>

					</div>
				</a>
			</div>
			<?php endforeach; ?>
		</div>
	</div>	
</section>