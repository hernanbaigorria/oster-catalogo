<section class="general-grill">
	<div class="container">
		<div class="row">
			<div class="col-12 p-0">
				<p><?=$catalogo[0]->nombre?> > <strong><?=$act_cat[0]->nombre?></strong></p>
			</div>
		</div>
	</div>
	<section class="section-product-view">
		<div class="container">
			<div class="row">
				<div class="col-12 col-md-6 product-zoom text-center d-block d-sm-none">
					<div class="d-block d-sm-none w-100" style="position:relative;">
						<?php if($producto[0]->nuevo == 1): ?>
							<img src="<?=base_url()?>asset/img/img-new-product.png" class="img-fluid abs-img-new">
						<?php endif; ?>
						<h3><?=$producto[0]->titulo?></h3>
						<h5><?=$producto[0]->subtitulo?></h5>
					</div>
					
				</div>
				<div class="col-12 col-md-6">
					<div class="d-none d-sm-block w-100" style="position:relative;">
						<?php if($producto[0]->nuevo == 1): ?>
							<img src="<?=base_url()?>asset/img/img-new-product.png" class="img-fluid abs-img-new">
						<?php endif; ?>
						<h3><?=$producto[0]->titulo?></h3>
						<h5><?=$producto[0]->subtitulo?></h5>
					</div>
					<ul class="nav nav-tabs" id="myTab" role="tablist">
					  <li class="nav-item">
					    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true"><?=$this->lang->line('title_overview')?></a>
					  </li>
					  <?php if(!empty($producto[0]->brochure)): ?>
						  <li class="nav-item">
						    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Documentation</a>
						  </li>
					  <?php endif; ?>
					</ul>
					<div class="tab-content" id="myTabContent">
					  <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
					  	<p><?=$producto[0]->descripcion?></p>
					  </div>
					  <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
					  	<?php if(!empty($producto[0]->brochure)): ?>
						  	<div class="row align-items-center">
						  		<div class="col-6 text-left p-0">
						  			<h4>Brochure</h4>
						  		</div>
						  		<div class="col-6 text-right p-0">
						  			<a href="<?=base_url()?>asset/img/uploads/<?=$producto[0]->brochure?>" target="_blank">Download</a>
						  		</div>
						  	</div>
					  	<?php endif; ?>
					  	<?php if(!empty($producto[0]->hoja_venta)): ?>
						  	<div class="row align-items-center">
						  		<div class="col-6 text-left p-0">
						  			<h4>Sell Sheet</h4>
						  		</div>
						  		<div class="col-6 text-right p-0">
						  			<a href="<?=base_url()?>asset/img/uploads/<?=$producto[0]->hoja_venta?>" target="_blank">Download</a>
						  		</div>
						  	</div>
					  	<?php endif; ?>
					  </div>
					</div>
					<a href="<?=base_url().$this->config->item('language_abbr')?>/where_to_buy/" class="link-buy"><?=$this->lang->line('menu_buy')?></a>
				</div>
				<div class="col-12 col-md-6 product-zoom text-center d-none d-sm-block">
					<div class="xzoom-container">
					  <img class="xzoom" id="xzoom-default" src="<?php echo base_url() ?>asset/img/uploads/<?=$producto[0]->imagen?>" xoriginal="<?php echo base_url() ?>asset/img/uploads/<?=$producto[0]->imagen?>" />
					  <div class="xzoom-thumbs">
					    <a href="<?php echo base_url() ?>asset/img/uploads/<?=$producto[0]->imagen?>"><img class="xzoom-gallery" width="25" src="<?php echo base_url() ?>asset/img/uploads/<?=$producto[0]->imagen?>"  xpreview="<?php echo base_url() ?>asset/img/uploads/<?=$producto[0]->imagen?>" title="The description goes here"></a>
					  </div>
					</div>
				</div>
			</div>
		</div>
	</section>
</section>