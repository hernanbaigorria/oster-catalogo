<section class="header-grill-products" style="background:url('<?=base_url()?>asset/img/uploads/<?=$catalogo[0]->imagen_portada?>');">
</section>
<section class="products-detaills">
	<div class="container">
		<div class="row m-0">
			<div class="col-12">
				<h4 class="site-map"><?=$catalogo[0]->nombre?> > <strong><?=$act_cat[0]->nombre?></strong></h4>
			</div>
			<?php foreach($productos as $producto): ?>
			<div class="col-12 col-sm-4" style="margin-bottom: 20px;">
				<a href="<?php echo base_url() ?>catalogs/<?=$slug_catalogo?>/<?=$slug_cat?>/<?=$slug_subcat?>/<?=$producto->slug?>/" class="box d-flex align-items-center justify-content-center">
					<div class="image d-flex align-items-center flex-column w-100">
						<img src="<?php echo base_url() ?>asset/img/uploads/<?=$producto->imagen?>" class="img-fluid">
						<h4><?=$producto->titulo?></h4>
						<div class="info-card">
							<h5><?=$producto->titulo?></h5>
							<p>
								<?=$producto->subtitulo?>
							</ul>
							</p>
						</div>

					</div>
				</a>
			</div>
			<?php endforeach; ?>
		</div>
	</div>	
</section>