<section class="header-service">
    <img src="<?=base_url()?>asset/img/<?=$this->lang->line('header_service')?>" class="img-fluid w-100">
</section>
<section class="mapas-titles">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h3><?=$map[0]->country?></h3>
                <p><?=$this->lang->line('header_service_desc')?></p>
            </div>
        </div>
    </div>
</section>
<div class="main-content">  
    <script id="google_map" type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyD4Px-mEuR54Hdl-t6Ifn4Q8H5e7ZRHTMA&libraries=geometry&language=es"></script>
    <script src="<?php echo base_url()?>asset/js/markerclusterer.js"></script>
    <div id='map_canvas'></div>
    <div class="consecionarios-content container" id="">
    <div class="row w-100">
        <?php foreach($localidades as $localidad): ?>
            <div class="col-md-6 list-concesionario-title" data-filter="<?=$localidad->id?>">
                <h3><?=$localidad->localidad?> - <?=$map[0]->country?></h3>
                <div class="row w-100">
                    <?php foreach($this->page_model->get_centro_bylocalidad($localidad->id_localidad)  as $centro): ?>
                        <div class="list-concesionario col-md-6" data-filter="<?=$centro->id?>">
                            <div class="marca" style="display:none;"><?=$centro->id?></div>
                            <div class="latlong" style="display:none;">-35.42450837917413,-60.17809006216123</div>
                            <div class="data">
                                <h5><?=$centro->titulo?></h5>
                                <div class="localidad"><?=$centro->direccion?></div>
                                <p><?=$centro->telefono?></p>
                                <p><strong>Email:</strong><br>
                                    <?=$centro->email?>
                                </p>
                            </div>
                            <div style="clear:both;"></div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
</div>

