<section class="header-service">
	<img src="<?=base_url()?>asset/img/<?=$this->lang->line('header_buy')?>" class="img-fluid w-100">
</section>

<section class="countrys">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h3><?=$this->lang->line('header_service_sub')?></h3>
			</div>
			<?php foreach($paises as $pais): ?>
                <div class="col-12 col-md-4">
                    <a href="<?=base_url()?>where-to-buy/maps?ctry=<?=$pais->pais?>"><?=$pais->country?></a>
                </div>
            <?php endforeach; ?>
		</div>
	</div>
</section>