<section class="product-registration">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-3"></div>
            <div class="col-12 col-md-6">
                <h3><?=$this->lang->line('contact_title')?></h3>
                <p><?=$this->lang->line('contact_sub')?> ( <span>*</span> )</p>
                <form class="form-contact" action="<?php echo base_url().$this->config->item('language_abbr') ?>/contact/save-contact/" method="post">
                    <input type="text" name="name" required placeholder="<?=$this->lang->line('input_name')?>*">
                    <input type="text" name="lastname" required placeholder="<?=$this->lang->line('input_lastname')?>*">
                    <input type="email" name="email" required placeholder="<?=$this->lang->line('input_email')?>*">
                    <select name="country" required>
                        <option value=""><?=$this->lang->line('input_country')?></option>
                        <?php foreach($paises as $pais): ?>
                            <option value="<?=$pais->country?>"><?=$pais->country?></option>
                        <?php endforeach; ?>
                    </select>
                    <input type="text" name="subject" required placeholder="<?=$this->lang->line('input_subject')?>*">
                    <textarea placeholder="<?=$this->lang->line('input_message')?>" name="message" required row="150"></textarea>
                    <div class="custom-terminos text-left" style="margin-top:10px;margin-bottom:30px;">
                        <div class="custom-checkbox">
                          <input type="checkbox" name="terminos"  class="required" id="id-1" required style="opacity:0;height:0;width:0;padding:0;margin:0;">
                          <label for="id-1" style="text-align:left;"><?=$this->lang->line('input_terms')?></label>
                          <label for="id-1" class="checkbox"></label>
                        </div>
                    </div>
                    <div class="subtext">
                        <p><?=$this->lang->line('input_subterms')?></p>
                    </div>
                    <input type="submit" value="<?=$this->lang->line('input_submit')?>">
                </form>
            </div>
            <div class="col-12 col-md-3"></div>
        </div>
    </div>
</section>