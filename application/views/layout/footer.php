<?php $leng = $this->config->item('language_abbr');
        if ($leng == 'ar'){
        	$this->lang->load('web_lang','spanish');
			$shortname = "es";
			$language = "spanish";
		}

		if ($leng == 'cl'){
        	$this->lang->load('web_lang','spanish');
			$shortname = "es";
			$language = "spanish";
		}


		if ($leng == 'br'){
			$this->lang->load('web_lang','portuguese');
			$shortname = "pt";
			$language = "portuguese";
		}

		if ($leng == 'us'){
			$this->lang->load('web_lang','english');
			$shortname = "en";
			$language = "english";
		}

?>
<?php date_default_timezone_set('America/Los_Angeles');
	$catalogos = $this->page_model->get_catalogos($shortname)
?>
<section class="footer-sect-01">
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-5">
				<form action="<?php echo base_url().$this->config->item('language_abbr') ?>/contact/save-newsletter/" method="post" class="form-newsletter w-100 d-flex align-items-center justify-content-center">
					<input type="email" name="email" required placeholder="Email">
					<input type="submit" value="<?=$this->lang->line('newsletter_send')?>">
				</form>
				<p><?=$this->lang->line('newsletter_info')?></p>
			</div>
			<div class="col-12 col-md-4"></div>
			<div class="col-12 col-md-3 text-center">
				<div class="w-100 d-flex align-items-center justify-content-center icons-reds">
					<a href="https://www.instagram.com/osterlatino/" target="_blank"><i class="fab fa-instagram"></i></a>
					<a href="https://www.facebook.com/OsterLatino/" target="_blank"><i class="fab fa-facebook"></i></a>
					<a href="https://www.youtube.com/user/OsterLatino/" target="_blank"><i class="fab fa-youtube"></i></a>
				</div>
				<p><?=$this->lang->line('social_info')?></p>
			</div>
		</div>
	</div>
</section>

<section class="footer-sect-02">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center">
				<a href="#"><?=$this->lang->line('back_top')?> <span style="font-family:Arial;">ʌ</span></a>
			</div>
		</div>
	</div>
</section>

<section class="footer-sect-03">
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-3">
				<h5>Oster</h5>
				<ul>
					<?php foreach($catalogos as $catalogo): ?>
						<li><a href="<?=base_url().'catalogs/'.$catalogo->slug?>"><?=$catalogo->nombre?></a></li>
					<?php endforeach; ?>
				</ul>
			</div>
			<div class="col-12 col-md-3">
				<h5><?=$this->lang->line('title_resource')?></h5>
				<ul>
					<li><a href="<?=base_url()?>product_registration/"><?=$this->lang->line('menu_product')?></a></li>
					<li><a href="<?=base_url()?>service_support/"><?=$this->lang->line('menu_service')?></a></li>
					<li><a href="<?=base_url()?>where_to_buy/"><?=$this->lang->line('menu_buy')?></a></li>
					<li><a href="<?=base_url()?>contact/"><?=$this->lang->line('menu_contact')?></a></li>
				</ul>
			</div>
			<div class="col-12 col-md-3">
				<h5>Legal</h5>
				<ul>
					<li><a href="https://privacy.newellbrands.com/index_es.html" target="_blank"><?=$this->lang->line('privacy')?></a></li>
					<li><a href="<?=base_url()?>cookie/"><?=$this->lang->line('cookie')?></a></li>
					<li><a href="<?=base_url()?>terminos/"><?=$this->lang->line('terms')?></a></li>
					<li><a href="https://www.osterlatinamerica.com/servicio-y-soporte/informacion-legal/terminos-y-condiciones">Términos y Condiciones</a></li>
				</ul>
			</div>
			<div class="col-12 col-md-3"></div>
		</div>
	</div>
</section>

<section class="footer-sect-04">
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-6 d-flex align-items-center">
				<a href="#">
					<img src="<?=base_url()?>asset/img/oster-logo.png" class="img-fluid">
				</a>
				<p>© <?=date(Y)?> Sunbeam Products, Inc. All Rights Reserved.</p>
			</div>
			<div class="col-12 col-md-6"></div>
		</div>
	</div>
</section>