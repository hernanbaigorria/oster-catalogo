<?php $leng = $this->config->item('language_abbr');
        if ($leng == 'ar'){
        	$this->lang->load('web_lang','spanish');
			$shortname = "es";
			$language = "spanish";
		}

		if ($leng == 'cl'){
        	$this->lang->load('web_lang','spanish');
			$shortname = "es";
			$language = "spanish";
		}


		if ($leng == 'br'){
			$this->lang->load('web_lang','portuguese');
			$shortname = "pt";
			$language = "portuguese";
		}

		if ($leng == 'us'){
			$this->lang->load('web_lang','english');
			$shortname = "en";
			$language = "english";
		}

?>

<header>
	<div class="container">
		<nav class="navbar navbar-expand-lg">
		  <a class="navbar-brand" href="<?=base_url()?>"><img src="<?=base_url()?>asset/img/oster-logo.png" class="img-fluid"></a>
		  <form class="form-search d-none d-sm-block" method="GET" action="<?=base_url()?>search/">
		    <div class="pseudo-search">
		      <button class="fa fa-search" type="submit"></button>
		      <input type="text" name="s" placeholder="<?=$this->lang->line('search')?>" autofocus required <?php if(isset($_GET['s'])): echo "value=".htmlspecialchars($_GET['s']).""; endif; ?>>
		    </div>
		  </form>

		  <div class="dino-input-field select custom-select">
		    <ul class="select-options">
		      <li onclick="window.location='<?php echo base_url().'us'.uri_string()?>/'"><?=$this->lang->line('en')?></li>
		      <li onclick="window.location='<?php echo base_url().'ar'.uri_string()?>/'"><?=$this->lang->line('esp')?></li>
		      <li onclick="window.location='<?php echo base_url().'br'.uri_string()?>/'"><?=$this->lang->line('port')?></li>
		    </ul>
		    <span class="option-selected">
		    	<?php if($language == 'spanish'): ?>
		    		<?=$this->lang->line('esp')?>
		    	<?php endif; ?>
		    	<?php if($language == 'portuguese'): ?>
		    		<?=$this->lang->line('port')?>
		    	<?php endif; ?>
		    	<?php if($language == 'english'): ?>
		    		<?=$this->lang->line('en')?>
		    	<?php endif; ?>
		    </span>
		  </div>
		</nav>
	</div>
</header>
<div class="responsive-menu-search w-100 d-flex align-items-center d-sm-none">
	<form class="form-search w-100" method="GET" action="<?=base_url()?>search/">
	  <div class="pseudo-search">
	    <button class="fa fa-search" type="submit"></button>
	    <input type="text" name="s" placeholder="Search" autofocus required <?php if(isset($_GET['s'])): echo "value=".htmlspecialchars($_GET['s']).""; endif; ?>>
	  </div>
	</form>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"><i class="icon-menu-toggle fas fa-bars"></i></span>
	  </button>
</div>
<div class="container">
	<div class="row">
		<div class="col-12">
			<!-- Static navbar -->
            <nav class="navbar navbar-expand-lg">
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
				    <ul class="navbar-nav mr-auto">
				     <?php $catalogos = $this->page_model->get_catalogos($shortname);?>
				     <?php foreach($catalogos as $cat): ?>
				     <?php $categorias = $this->page_model->get_categorias($cat->slug, $shortname); ?>
				     	<li class="nav-item dropdown">
				     	    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				     	      <?=$cat->nombre?>
				     	    </a>
				     	    <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
				     	      <li class="dropdown-submenu">
				     	      	<?php foreach($categorias as $cate): ?>
				     	      		<?php $subcategorias = $this->page_model->get_sub_categeoria($cate->uniq, $shortname); ?>
					     	        <?php if(!empty($subcategorias)): ?>
					     	        <a class="dropdown-item dropdown-toggle" href="#"><?=$cate->nombre?></a>
					     	        <ul class="dropdown-menu">
						     	          <?php foreach($subcategorias as $subcat): ?>
						     	          	<li><a class="dropdown-item" href="<?=base_url().'catalogs/'.$cat->slug.'/'.$cate->slug.'/'.$subcat->slug?>/"><?=$subcat->nombre?></a></li>
						     	          <?php endforeach; ?>
					     	        </ul>
	     	             	          <?php else: ?>
	     	        	     	          <li class="nav-item">
	     	        	     	            <a class="nav-link" href="<?=base_url().'catalogs/'.$cat->slug.'/'.$cate->slug?>/"><?=$cate->nombre?></a>
	     	        	     	          </li>
	     	        	     	      <?php endif; ?>
				     	    	<?php endforeach; ?>
				     	      </li>
				     	    </ul>
				     	</li>
				     <?php endforeach; ?>
				     	<li class="nav-item">
				     	  <a class="nav-link" href="<?=base_url().$this->config->item('language_abbr')?>/service-support/"><?=$this->lang->line('menu_service')?></a>
				     	</li>
				     	<li class="nav-item">
				     	  <a class="nav-link" href="<?=base_url().$this->config->item('language_abbr')?>/product-registration/"><?=$this->lang->line('menu_product')?></a>
				     	</li>
				     	<li class="nav-item">
				     	  <a class="nav-link" href="<?=base_url().$this->config->item('language_abbr')?>/where-to-buy/"><?=$this->lang->line('menu_buy')?></a>
				     	</li>
				    </ul>
				</div>
            </nav>
		</div>
	</div>
</div>