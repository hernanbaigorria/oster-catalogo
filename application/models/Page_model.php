<?php

class page_model extends CI_Model
{


		public function __construct()
        {
			parent::__construct();
			// Your own constructor code
			$this->load->helper('cookie');
        }
        
        public $title;
        public $content;
        public $date;


		public function get_catalogos($abr)
		{	
			$this->db->query("SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));");
			$this->db->select('catalogos.*');
			$this->db->group_by('catalogos.uniq');
			$this->db->where('lenguajes.abr', $abr);
    		$this->db->join('lenguajes', 'lenguajes.id = catalogos.lang');
			$result = $this->db->get('catalogos');
			return $result->result();
		}

		public function get_slider($abr)
		{
			$this->db->select('slider.*');
			$this->db->where('lenguajes.abr', $abr);
    		$this->db->join('lenguajes', 'lenguajes.id = slider.lang');
			$result = $this->db->get('slider');
			return $result->result();
		}

		public function get_paises()
		{	
			$this->db->order_by('country', 'asc');
			$result = $this->db->get('countries');
			return $result->result();
		}

		public function get_centros_bypaises()
		{
			$this->db->query("SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));");
			$this->db->select('centros.*');
			$this->db->select('countries.country as country');
			$this->db->order_by('countries.country', 'asc');
			$this->db->join('countries', 'countries.id = centros.pais');
			$this->db->group_by('centros.pais');
			$result = $this->db->get('centros');
			return $result->result();

		}

		public function get_centros_sup_bypaises()
		{
			$this->db->query("SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));");
			$this->db->select('centros.*');
			$this->db->where('centros.soporte', 1);
			$this->db->select('countries.country as country');
			$this->db->order_by('countries.country', 'asc');
			$this->db->join('countries', 'countries.id = centros.pais');
			$this->db->group_by('centros.pais');
			$result = $this->db->get('centros');
			return $result->result();

		}

		public function get_catalogo_slug($slug, $abr)
		{	
			$this->db->query("SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));");
			$this->db->select('catalogos.*');
			$this->db->where('catalogos.slug', $slug);
			$this->db->where('lenguajes.abr', $abr);
			$this->db->group_by('catalogos.uniq');
			$this->db->join('lenguajes', 'lenguajes.id = catalogos.lang');
			$result = $this->db->get('catalogos');
			return $result->result();

		}

		public function get_categorias($slug, $abr)
		{	
			$this->db->query("SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));");
			$this->db->select('categorias.*');
			$this->db->select('catalogos.slug as slug_catalogo');
			$this->db->where('catalogos.slug', $slug);
			$this->db->where('lenguajes.abr', $abr);
			$this->db->group_by('categorias.uniq');
    		$this->db->join('lenguajes', 'lenguajes.id = categorias.lang');
			$this->db->join('catalogos', 'catalogos.uniq = categorias.id_catalogo');
			$result = $this->db->get('categorias');
			return $result->result();
		}

		public function get_sub_categeoria($id_categoria, $abr)
		{	
			$this->db->query("SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));");
			$this->db->select('subcategorias.*');
			$this->db->select('categorias.nombre as categoria');
			$this->db->select('categorias.slug as slug_categoria');
			$this->db->select('catalogos.slug as slug_catalogo');
			$this->db->where('lenguajes.abr', $abr);
			$this->db->group_by('subcategorias.uniq');
    		$this->db->join('lenguajes', 'lenguajes.id = subcategorias.lang');
			$this->db->join('categorias', 'categorias.uniq = subcategorias.id_categoria');
			$this->db->join('catalogos', 'catalogos.uniq = categorias.id_catalogo');
			$this->db->where('subcategorias.id_categoria', $id_categoria);
			$result = $this->db->get('subcategorias');
			return $result->result();
		}

		public function get_act_categoria($slug_categoria , $slug_subcategoria, $abr)
		{
			$this->db->query("SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));");
			if(!empty($slug_subcategoria)):
				$this->db->select('subcategorias.*');
				$this->db->select('catalogos.imagen_portada as imagen_portada');
				$this->db->select('categorias.nombre as categoria');
				$this->db->where('lenguajes.abr', $abr);
				$this->db->group_by('subcategorias.uniq');
	    		$this->db->join('lenguajes', 'lenguajes.id = subcategorias.lang');
				$this->db->where('categorias.slug', $slug_categoria);
				$this->db->where('subcategorias.slug', $slug_subcategoria);
				$this->db->join('categorias', 'categorias.uniq = subcategorias.id_categoria');
				$this->db->join('catalogos', 'catalogos.uniq = categorias.id_catalogo');
				$result = $this->db->get('subcategorias');
				return $result->result();
			else:
				$this->db->where('lenguajes.abr', $abr);
				$this->db->group_by('categorias.uniq');
	    		$this->db->join('lenguajes', 'lenguajes.id = categorias.lang');
				$this->db->where('categorias.slug', $slug_categoria);
				$result = $this->db->get('categorias');
				return $result->result();
			endif;
		}

		public function get_map($pais)
		{
			$this->db->select('centros.*');
			$this->db->select('countries.country as country');
			$this->db->where('centros.pais', $pais);
			$this->db->join('countries', 'countries.id = centros.pais');
			$result = $this->db->get('centros');
			return $result->result();
		}

		public function get_map_supp($pais)
		{
			$this->db->select('centros.*');
			$this->db->select('countries.country as country');
			$this->db->where('centros.pais', $pais);
			$this->db->where('centros.soporte', 1);
			$this->db->join('countries', 'countries.id = centros.pais');
			$result = $this->db->get('centros');
			return $result->result();
		}

		public function get_localidades_map($pais)
		{	
			$this->db->query("SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));");
			$this->db->select('centros.*');
			$this->db->select('centros.localidad as id_localidad');
			$this->db->select('localidades.localidad as localidad');
			$this->db->where('centros.pais', $pais);
			$this->db->join('countries', 'countries.id = centros.pais');
			$this->db->join('localidades', 'localidades.id = centros.localidad');
			$this->db->group_by('centros.localidad');
			$result = $this->db->get('centros');
			return $result->result();
		}


		public function get_localidades_map_supp($pais)
		{	
			$this->db->query("SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));");
			$this->db->select('centros.*');
			$this->db->select('centros.localidad as id_localidad');
			$this->db->select('localidades.localidad as localidad');
			$this->db->where('centros.pais', $pais);
			$this->db->where('centros.soporte', 1);
			$this->db->join('countries', 'countries.id = centros.pais');
			$this->db->join('localidades', 'localidades.id = centros.localidad');
			$this->db->group_by('centros.localidad');
			$result = $this->db->get('centros');
			return $result->result();
		}

		public function get_centro_bylocalidad($localidad)
		{

			$this->db->select('centros.*');
			$this->db->where('centros.localidad', $localidad);
			$result = $this->db->get('centros');
			return $result->result();
		}

		public function get_productos_all($slug_catalog, $abr)
		{
			$this->db->query("SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));");
			$this->db->select('productos.*');
			$this->db->select('categorias.slug as slug_categoria');
			$this->db->select('subcategorias.slug as slug_subcategoria');
			$this->db->select('catalogos.slug as slug_catalogo');
			$this->db->where('lenguajes.abr', $abr);
			$this->db->where('catalogos.slug', $slug_catalog);
			$this->db->group_by('productos.uniq');
			$this->db->join('lenguajes', 'lenguajes.id = productos.lang');
			$this->db->join('categorias', 'categorias.uniq = productos.id_categoria');
			$this->db->join('subcategorias', 'subcategorias.uniq = productos.id_subcategoria');
			$this->db->join('catalogos', 'catalogos.uniq = categorias.id_catalogo');
			$this->db->where('productos.publicar', 1);
			$result = $this->db->get('productos');
			return $result->result();
		}

		public function get_productos_nuevos($abr)
		{
			$this->db->query("SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));");
			$this->db->select('productos.*');
			$this->db->select('categorias.slug as slug_categoria');
			$this->db->select('subcategorias.slug as slug_subcategoria');
			$this->db->select('catalogos.slug as slug_catalogo');
			$this->db->where('productos.nuevo', 1);
			$this->db->where('lenguajes.abr', $abr);
			$this->db->group_by('productos.uniq');
    		$this->db->join('lenguajes', 'lenguajes.id = productos.lang');
			$this->db->limit(2);
			$this->db->join('categorias', 'categorias.uniq = productos.id_categoria');
			$this->db->join('subcategorias', 'subcategorias.uniq = productos.id_subcategoria');
			$this->db->join('catalogos', 'catalogos.uniq = categorias.id_catalogo');
			$this->db->where('productos.publicar', 1);
			$result = $this->db->get('productos');

			return $result->result();
		}

		public function get_productos($slug_categoria , $slug_subcategoria, $abr)
		{	
			$this->db->query("SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));");
			if(!empty($slug_subcategoria)):
				$this->db->select('productos.*');
				$this->db->where('categorias.slug', $slug_categoria);
				$this->db->where('subcategorias.slug', $slug_subcategoria);
				$this->db->where('lenguajes.abr', $abr);
				$this->db->group_by('productos.uniq');
	    		$this->db->join('lenguajes', 'lenguajes.id = productos.lang');
				$this->db->join('categorias', 'categorias.uniq = productos.id_categoria');
				$this->db->join('subcategorias', 'subcategorias.uniq = productos.id_subcategoria');
				$this->db->where('productos.publicar', 1);
				$result = $this->db->get('productos');
				return $result->result();
			else:
				$this->db->select('productos.*');
				$this->db->where('lenguajes.abr', $abr);
				$this->db->group_by('productos.uniq');
	    		$this->db->join('lenguajes', 'lenguajes.id = productos.lang');
				$this->db->where('categorias.slug', $slug_categoria);
				$this->db->join('categorias', 'categorias.uniq = productos.id_categoria');
				$this->db->where('productos.publicar', 1);
				$result = $this->db->get('productos');
				return $result->result();
			endif;
		}

		public function get_productos_buscador($s, $abr)
		{	
			$this->db->query("SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));");
			$this->db->select('productos.*');
			$this->db->select('categorias.slug as slug_categoria');
			$this->db->select('subcategorias.slug as slug_subcategoria');
			$this->db->select('catalogos.slug as slug_catalogo');
			$this->db->like('productos.titulo', htmlspecialchars($s));
			$this->db->or_like('productos.subtitulo', htmlspecialchars($s));
			$this->db->where('lenguajes.abr', $abr);
			$this->db->group_by('productos.uniq');
			$this->db->join('categorias', 'categorias.uniq = productos.id_categoria');
			$this->db->join('subcategorias', 'subcategorias.uniq = productos.id_subcategoria');
			$this->db->join('catalogos', 'catalogos.uniq = categorias.id_catalogo');
			$this->db->join('lenguajes', 'lenguajes.id = productos.lang');
			$this->db->where('productos.publicar', 1);
			$result = $this->db->get('productos');
			return $result->result();
		}


		public function get_ult_productos($abr)
		{	
			$this->db->query("SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));");
			$this->db->select('productos.*');
			$this->db->select('categorias.slug as slug_categoria');
			$this->db->select('subcategorias.slug as slug_subcategoria');
			$this->db->select('catalogos.slug as slug_catalogo');
			$this->db->where('visitados.cookie', $this->input->ip_address());
			$this->db->where('lenguajes.abr', $abr);
			$this->db->join('productos', 'productos.uniq = visitados.producto');
			$this->db->join('categorias', 'categorias.uniq = productos.id_categoria');
			$this->db->join('subcategorias', 'subcategorias.uniq = productos.id_subcategoria');
			$this->db->join('catalogos', 'catalogos.uniq = categorias.id_catalogo');
    		$this->db->join('lenguajes', 'lenguajes.id = productos.lang');
			$this->db->order_by('visitados.registro', 'desc');
			$this->db->group_by('visitados.producto');
			$result = $this->db->get('visitados');
			return $result->result();
		}	

		public function get_producto($slug_categoria , $slug_subcategoria, $slug_producto, $abr)
		{
			$this->db->query("SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));");
			$this->db->select('productos.*');
			$this->db->group_by('productos.uniq');
			$this->db->where('lenguajes.abr', $abr);
			$this->db->where('categorias.slug', $slug_categoria);
			if(!empty($slug_subcategoria)):
				$this->db->where('subcategorias.slug', $slug_subcategoria);
			endif;
			$this->db->where('productos.slug', $slug_producto);
			$this->db->join('lenguajes', 'lenguajes.id = productos.lang');
			$this->db->join('categorias', 'categorias.uniq = productos.id_categoria');
			if(!empty($slug_subcategoria)):
				$this->db->join('subcategorias', 'subcategorias.uniq = productos.id_subcategoria');
			endif;
			$this->db->where('productos.publicar', 1);
			$result = $this->db->get('productos');
			return $result->result();
		}

		public function get_events($abr)
		{	
			$this->db->query("SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));");
			$this->db->select('events.*');
			$this->db->group_by('events.uniq');
			$this->db->where('lenguajes.abr', $abr);
			$this->db->join('lenguajes', 'lenguajes.id = events.lang');
			$result = $this->db->get('events');
			return $result->result();
		}
		
}

?>