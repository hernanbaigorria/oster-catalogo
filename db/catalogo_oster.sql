-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 30-09-2021 a las 18:44:16
-- Versión del servidor: 5.7.24
-- Versión de PHP: 5.6.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `catalogo_oster`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `catalogos`
--

CREATE TABLE `catalogos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `descripcion` longtext,
  `imagen_portada` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `catalogos`
--

INSERT INTO `catalogos` (`id`, `nombre`, `descripcion`, `imagen_portada`, `slug`) VALUES
(1, 'Caballeros', 'En Oster,\r\nsomos una familia\r\nde expertos que diseña soluciones vanguardistas\r\ny equipos de alto\r\nrendimiento. \r\n\r\nComprometidos\r\ncon el cuidado de las personas creemos en productos\r\nque agregan valor\r\ny aseguran mejores\r\nresultados\r\nprofesionales. ', '1616681688677_tapa_caballeros.png', 'caballeros'),
(2, 'Animales', 'En Oster, somos una familia de expertos que diseña soluciones vanguardistas y equipos de alto rendimiento. Comprometidos con el cuidado de los animales creemos en productos que agregan valor y aseguran mejores resultados profesionales', '1617656803891_tapa_animales.png', 'animales');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `id` int(11) NOT NULL,
  `id_catalogo` int(11) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `portada` varchar(255) DEFAULT NULL,
  `footer` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`id`, `id_catalogo`, `nombre`, `portada`, `footer`, `slug`) VALUES
(1, 1, 'CORTADORAS', '1616774892147_portada_cortadoras_.png', '', 'cortadoras'),
(2, 1, 'CUCHILLAS', '', '', 'cuchillas'),
(3, 1, 'CORTADORAS & CUCHILLAS', '', '', 'cortadoras-cuchillas'),
(4, 1, 'RECORTADORAS & CUCHILLAS', '', '', 'recortadoras-cuchillas'),
(5, 1, 'CORTADORAS & CUCHILLAS', '', '', 'cortadoras-cuchillas'),
(7, 1, 'MANTENIMIENTO  ', '', '', 'mantenimiento'),
(8, 1, 'OTRO ', '1618008479900_portada_otras.png', '', 'otro'),
(9, 2, 'Animales pequeños y medianos', '', '', 'animales-pequenos-y-medianos'),
(10, 2, 'Animales grandes', '', '', 'animales-grandes');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `countries`
--

CREATE TABLE `countries` (
  `id` int(20) NOT NULL,
  `country` varchar(250) NOT NULL,
  `shortname` varchar(2) NOT NULL,
  `language` varchar(255) NOT NULL,
  `ContactData` text NOT NULL,
  `webEmailContact` varchar(255) NOT NULL,
  `supportEmailContact` varchar(255) NOT NULL,
  `salesEmailContact` varchar(255) NOT NULL,
  `facebookUrl` varchar(255) NOT NULL,
  `twitterUrl` varchar(255) NOT NULL,
  `instagramUrl` varchar(255) NOT NULL,
  `gplusUrl` varchar(255) NOT NULL,
  `youtubeUrl` varchar(255) NOT NULL,
  `newsletter` int(1) NOT NULL DEFAULT '1',
  `product_register` int(1) NOT NULL DEFAULT '1',
  `service_centers` int(1) NOT NULL DEFAULT '1',
  `employ` int(1) NOT NULL DEFAULT '1',
  `center_service` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `countries`
--

INSERT INTO `countries` (`id`, `country`, `shortname`, `language`, `ContactData`, `webEmailContact`, `supportEmailContact`, `salesEmailContact`, `facebookUrl`, `twitterUrl`, `instagramUrl`, `gplusUrl`, `youtubeUrl`, `newsletter`, `product_register`, `service_centers`, `employ`, `center_service`) VALUES
(1, 'Ecuador', 'ec', 'spanish', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, ''),
(5, 'Panamá', 'pa', 'spanish', '', '', '', '', '', '', '', '', '', 1, 1, 1, 1, ''),
(7, 'Republica Dominicana', 'do', 'spanish', '', '', '', '', '', '', '', '', '', 1, 1, 1, 1, ''),
(8, 'Costa Rica', 'cr', 'spanish', '', '', '', '', '', '', '', '', '', 1, 1, 1, 1, ''),
(22, 'Honduras', 'hn', 'spanish', '', '', '', '', '', '', '', '', '', 1, 1, 1, 1, ''),
(23, 'El Salvador', 'sv', 'spanish', '', '', '', '', '', '', '', '', '', 1, 1, 1, 1, ''),
(24, 'Guatemala', 'gt', 'spanish', '', '', '', '', '', '', '', '', '', 1, 1, 1, 1, ''),
(25, 'Nicaragua', 'ni', 'spanish', '', '', '', '', '', '', '', '', '', 1, 1, 1, 1, ''),
(26, 'Trinidad y Tobago', 'tt', 'english', '', '', '', '', '', '', '', '', '', 1, 1, 1, 1, ''),
(27, 'Argentina', 'ar', 'spanish', '', '', '', '', '', '', '', '', '', 1, 1, 1, 1, ''),
(28, 'International', 'us', 'english', '', '', '', '', '', '', '', '', '', 1, 1, 1, 1, ''),
(29, 'Paraguay', 'py', 'spanish', '', '', '', '', '', '', '', '', '', 1, 1, 1, 1, ''),
(30, 'Mexico', 'mx', 'spanish', '', '', '', '', '', '', '', '', '', 1, 1, 1, 1, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `id` int(11) NOT NULL,
  `id_categoria` int(11) DEFAULT NULL,
  `id_subcategoria` int(11) DEFAULT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `subtitulo` varchar(255) DEFAULT NULL,
  `descripcion` longtext,
  `imagen` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`id`, `id_categoria`, `id_subcategoria`, `titulo`, `subtitulo`, `descripcion`, `imagen`, `slug`) VALUES
(1, 1, 1, 'TITAN® 2 VELOCIDADES  120V', '76076-310-002', 'La potente cortadora de 2 velocidades \r\nagiliza todos los trabajos. Aclamada por barberos y estilistas, la cortadora Oster® Titan ha demostrado ser resistente \r\nal uso continuo, día tras día, año tras año.<br><br>\r\n \r\n• Potente motor universal de doble velocidad para uso pesado.<br>\r\n• Cuchilla - tamaños 000 y 1.\r\n• Incluye protector de la cuchilla, aceite lubricante, grasa y cepillo de limpieza.<br>\r\n• Carcasa ultra duradera y resistente a la rotura.<br>\r\n• Cable de alimentación de 12 pies.', '1616779712821_producto_01.png', 'titan-2-velocidades-120v'),
(3, 1, 1, 'CLASSIC 76 VELOCIDAD ÚNICA  120V', '76076-010-003', 'Reconocida por su durabilidad, potencia y rendimiento, esta cortadora de velocidad única ha demostrado ser resistente al uso continuo, día tras día, año tras año. Mejor calificada para el estilista/peluquero experimentado que aprecia la capacidad de la cortadora para operar sin cesar y sin esfuerzo.<br><br>\r\n\r\n• Potente motor universal de velocidad única.<br>\r\n• Cuchillas desmontables - tamaños 000 y 1.<br>\r\n• Incluye protector de la cuchilla, aceite lubricante, grasa y cepillo de limpieza.<br>\r\n• Carcasa ultra duradera y resistente a la rotura.<br>\r\n• Cable de alimentación de 9 pies.', '1617025004806_producto_02.png', 'classic-76-velocidad-Única-120v'),
(4, 1, 1, 'TITAN® 2 VELOCIDADES  220V', '76076-410-051', 'La potente cortadora de 2 velocidades \r\nagiliza todos los trabajos. Aclamada por barberos y estilistas, la cortadora Oster® Titan ha demostrado ser resistente \r\nal uso continuo, día tras día, año tras año.<br><br>\r\n \r\n• Potente motor universal de doble velocidad para uso pesado.<br>\r\n• Cuchilla - tamaños 000 y 1.<br>\r\n• Incluye protector de la cuchilla, aceite lubricante, grasa y cepillo de limpieza.<br>\r\n• Carcasa ultra duradera y resistente a la rotura.<br>\r\n• Cable de alimentación de 12 pies.', '1617025102923_producto_03.png', 'titan-2-velocidades-220v'),
(5, 1, 1, 'CLASSIC 97  220V', '76097-440-051', 'La legendaria cortadora de barbero diseñada para uso continuo por barberos y estilistas profesionales. \r\nEs ideal para todo tipo de corte de cabello y soporta los requerimientos diarios de uso sofisticado.<br><br>\r\n\r\n• Potente motor de una sola velocidad de 45 vatios.<br>\r\n• Motor universal de alto rendimiento para corte en húmedo o cabello seco.<br>\r\n• Sistema snap-on original, desmontable para un cambio de cuchilla rápido y conveniente.<br>\r\n• Variedad en diferentes tamaños de cuchillas de 0.2mm - 12.7mm.<br>\r\n• Diseño y fabricacion resistente, construidos para durar.<br>\r\n• Funcionamiento fresco con filtros extraíbles.<br>\r\n• 2100 golpes por minuto<br><br>\r\n\r\nINCLUYE<br>\r\n• 1 cuchilla (Tamaño 0000 / 0.25mm) 076918-016-005.<br>\r\n• Accesorios: protector de cuchilla, cepillo de limpieza, aceite lubricante, grasa.<br>\r\n• Cuchillas opcionales.', '1617025171044_producto_04.png', 'classic-97-220v'),
(6, 2, 2, 'CUCHILLA DESMONTABLE 00000', '76918-606-005', '• Deja el cabello 0.2 mm (1/125\").<br>\r\n• Usos: Afeitado completo de la cabeza y cara.<br>\r\n• Capa resistente al agua y oxidazión.', '1617026955515_product--19.png', 'cuchilla-desmontable-00000'),
(7, 2, 2, 'CUCHILLA DESMONTABLE ESS - 0A', '76918-656-005', '• Deja el cabello 1.2 mm (3/64\").<br>\r\n• Usos: Muy cerca se desvanece, desvanecimiento calvo.<br>\r\n• Revestimiento resistente al óxido y al desgaste.', '1617027008614_product--20.png', 'cuchilla-desmontable-ess-0a'),
(8, 2, 2, 'CUCHILLA DESMONTABLE  0000', '76918-016-005', '• Deja el cabello 0.25 mm (1/100\").<br>\r\n• Usos: Para desvanecimientos muy cercanos al craneo, fades para calvos.', '1617027040491_product--21.png', 'cuchilla-desmontable-0000'),
(9, 2, 2, 'CUCHILLA DESMONTABLE ESS - 1', '76918-646-005', '• Deja el cabello 2.4 mm (3/32\").<br>\r\n• Usos: Cortadora sobre peine, mediano se desvanece, eliminando volumen (todo tipo de cabello).<br>\r\n• Revestimiento resistente al óxido y al desgaste.', '1617027077616_product--22.png', 'cuchilla-desmontable-ess-1'),
(10, 2, 2, 'CUCHILLA DESMONTABLE ESS - 000', '76918-626-005', '• Deja el cabello 0.5 mm (1/50\").<br>\r\n• Usos: Muy cerca se desvanece, desvanecimiento calvo.<br>\r\n• Revestimiento resistente al óxido y al desgaste.', '1617027114191_product--23.png', 'cuchilla-desmontable-ess-000'),
(11, 2, 2, 'CUCHILLA DESMONTABLE ESS - 1A', '76918-706-005', '• Deja el cabello 3.2 mm (1/8\").<br>\r\n• Usos: Cortadora sobre peine, desvanecimiento medio y rematado.<br>\r\n• Revestimiento resistente al óxido y al desgaste.', '1617027150701_product--24.png', 'cuchilla-desmontable-ess-1a'),
(12, 2, 2, 'CUCHILLA DESMONTABLE ESS - 1', '76918-676-005', '• Deja el cabello 4 mm (5/32\").<br>\r\n• Usos: Para cortes suaves, reduce lados y nuca.<br>\r\n• Revestimiento resistente al óxido y al desgaste.', '1617027307819_produtc-25.png', 'cuchilla-desmontable-ess-1'),
(13, 2, 2, 'CUCHILLA DESMONTABLE ESS - 3.75', '76918-806-005', '• Deja el cabello 12.7mm (1/2”).<br>\r\n• Mano libre, establece longitud en los lados y la nuca.<br>\r\n• Revestimiento resistente al óxido y al desgaste.', '1617027350690_produtc-26.png', 'cuchilla-desmontable-ess-375'),
(14, 2, 2, 'CUCHILLA DESMONTABLE  O2 - 2', '76918-686-005', '• Deja el cabello 6.3 mm (1/4\").<br>\r\n• Mano libre, mezcla lados y la nuca.<br>                                                 \r\n• Revestimiento resistente al óxido y al desgaste.', '1617027393675_produtc-27.png', 'cuchilla-desmontable-o2-2'),
(15, 2, 2, 'ORGANIZADOR DE CUCHILLAS IGLOO', '76004-011-000', 'Con bolsillos más grandes para almacenar \r\ntodo lo desmontable Oster® así como la mayoría \r\nde las otras cuchillas de tamaño completo.<br><br>\r\n\r\n• Almacena hasta 11 cuchillas.<br>\r\n• Hecho de resistente polipropileno plástico resistente al impacto para mayor durabilidad.', '1617027442859_produtc-28.png', 'organizador-de-cuchillas-igloo'),
(16, 2, 2, 'CUCHILLA DESMONTABLE ESS - 3.5', '76918-696-005', '• Deja el cabello 9.5 mm (3/8”).<br>\r\n• Mano libre, establece longitud en los lados y la nuca.<br>\r\n• Revestimiento resistente al óxido y al desgaste.', '1617027475463_produtc-30.png', 'cuchilla-desmontable-ess-35'),
(17, 3, 3, 'MXPRO™   220V', '76070-010-051', 'La cortadora mXpro es ideal \r\npara estilistias principiantes \r\no para uso ligero o mediano.<br><br>\r\n\r\n• El potente y ligero motor magnético de hasta 7, 200 SPM.<br>\r\n• Tamaño de cuchilla ajustable 000 a 1.<br>\r\n• La palanca ajustable mueve fácilmente la cuchilla desde el ajuste más corto al más largo.<br>\r\n• 4 accesorios de peine guía ofrecen una amplia variedad de longitudes y estilos.<br>\r\n• Cuerpo contorneado.<br>\r\n• Cable de alimentación de 8 pies.<br>\r\n• Incluye aceite de cuchilla y cepillo de limpieza.', '1617028065458_product--25.png', 'mxpro-220v'),
(18, 3, 3, 'CORTADORA  AJUSTABLE DE  MOTOR PIVOTE  TOPAZ® 120V', '76023-310-001', 'Cortadora de cuchillas ajustables \r\nde lineas elegantes funciona \r\ncorte tras corte de forma \r\nrápida y sin esfuerzo \r\ncon un potente motor \r\nrotativo Whisper Quiet™.<br><br>\r\n\r\nLa cortadora Topaz® \r\nmantiene el nivel de ruido \r\nbajo y la productividad alta.', '1617028182523_product--26.png', 'cortadora-ajustable-de-motor-pivote-topaz-120v'),
(19, 3, 3, 'CUCHILLA DE REMPLAZO  PARA CORTADORAS  MXPRO', '76913-270-000', '• Tamaño 000-1.', '1617028226482_product--27.png', 'cuchilla-de-remplazo-para-cortadoras-mxpro'),
(20, 3, 3, 'CUCHILLA  AJUSTABLE ESS', '76913-806-001', '• Compatible con cortadoras Topaz y Rocker.<br>\r\n• Tamaños 000 a 1.', '1617028249602_product--28.png', 'cuchilla-ajustable-ess'),
(21, 3, 3, 'CORTADORA FAST FEED VELOCIDAD ÚNICA 120V', '76023-510-001', 'Cortadora de cuchillas ajustables funciona corte tras corte de forma \r\nrápida y sin esfuerzo con un potente motor de pivote Whisper Quiet™. \r\nLa cortadora Fast Feed® mantiene el nivel de ruido\r\nbajo y la productividad alta.<br><br>\r\n\r\n• Motor de pivote silencioso 2 veces más potente que una cortadora de motor magnético. <br>\r\n• Cuchilla de acero ajustable de 000 a 1, que corta cabello seco o húmedo.<br>\r\n• Palanca de ajuste que ayuda a que la cuchilla se mueva con facilidad entre los ajustes de longitud.<br>\r\n• Lígera y fácil de manejar.<br><br>\r\n\r\nINCLUYE<br>\r\n• Cuchilla de acero ajustable.<br>\r\n• Peines guía: #0 (1/16) - #2 (1/4) - #3 (3/8) - #4 (1/2)<br>\r\n• Protector de cuchillas, aceite lubricante y cepillo de limpieza.', '1617028272081_product-29.png', 'cortadora-fast-feed-velocidad-Única-120v'),
(22, 3, 3, 'PRO-POWER VELOCIDAD ÚNICA', '76606-950-051', '• Cortador de motor de pivote potente y versátil.<br>\r\n• Ligero y compacto.<br>\r\n• La cuchilla de acero resistente a la corrosión se ajusta para longitudes de corte cortas (Tamaño 000 / 0.5mm) a medias (Tamaño 1 / 2.4mm).<br>\r\n• Corta fácilmente el cabello mojado y seco.<br>\r\n• 3000 golpes por minuto.<br><br>\r\n\r\nINCLUYE<br>\r\n• 4 peines: #1 (1/16\" / 1.5mm) - #2 (1/4\" / 6.3mm) - #3 (3/8\" / 9,5 mm) - #4 (1/2\" / 12,7 mm)<br>\r\n• Protector de cuchillas, aceite lubricante y cepillo de limpieza.', '1617028374657_product-30.png', 'pro-power-velocidad-Única'),
(23, 3, 3, 'CORTADORA FAST FEED PROFESIONAL   ', '220V - EUR (76023-090-051) | 220V - ARG (76023-090-054)', 'Cortadora de cuchillas ajustables funciona corte tras corte de forma \r\nrápida y sin esfuerzo con un potente motor de pivote Whisper Quiet™. \r\nLa cortadora Fast Feed® mantiene el nivel de ruido\r\nbajo y la productividad alta.<br><br>\r\n\r\n• Motor de pivote silencioso 2 veces más potente que una cortadora de motor magnético. <br>\r\n• Cuchilla de acero ajustable de 000 a 1, que corta cabello seco o húmedo.<br>\r\n• Palanca de ajuste que ayuda a que la cuchilla se mueva con facilidad entre los ajustes de longitud.<br>\r\n• Lígera y fácil de manejar.<br><br>\r\n\r\nINCLUYE<br>\r\n• Cuchilla de acero ajustable.<br>\r\n• Peines guía: #0 (1/16) - #2 (1/4) - #3 (3/8) - #4 (1/2)<br>\r\n• Protector de cuchillas, aceite lubricante y cepillo de limpieza.', '1617028447818_product-31.png', 'cortadora-fast-feed-profesional'),
(24, 3, 3, 'CUCHILLA  AJUSTABLE  DE 17 DIENTES', '76913-506-001', '• Alimentacion rápida.<br>\r\n• Adjusta-Groom.<br>\r\n• Tamaño variable de 000 a 1.<br>\r\n• 17 dientes. ', '1617028513114_product-32.png', 'cuchilla-ajustable-de-17-dientes'),
(25, 3, 3, 'CUCHILLA  AJUSTABLE  DE 25 DIENTES', '76913-536-001', '• Alimentacion rápida.<br>\r\n• Adjusta-Groom.<br>\r\n• Tamaño variable de 000 a 1.<br>\r\n• 25 dientes.  ', '1617028543251_product-33.png', 'cuchilla-ajustable-de-25-dientes'),
(26, 3, 3, '616 VELOCIDAD  ÚNICA  COLOR NEGRO  220V', '76616-910-051', '• Motor de Pivote.<br>\r\n• Incluye 2 cuchillas: (tamaños 0000 y 1).', '1617028637602_product-29.png', '616-velocidad-Única-color-negro-220v'),
(27, 3, 3, '616 VELOCIDAD  ÚNICA COLOR PLATA 220V', '76616-707-051', '• Motor de Pivote.<br>\r\n• Incluye 2 cuchillas  (tamaños 0000 y 1).', '1617028667026_product-30.png', '616-velocidad-Única-color-plata-220v'),
(28, 3, 3, '616 VELOCIDAD  ÚNICA COLOR NEGRO, SUAVE AL TACTO 220V', '76616-507-051', '• Motor de Pivote.<br>\r\n• Incluye 2 cuchillas  (tamaños 0000 y 1).', '1617028703466_product-31.png', '616-velocidad-Única-color-negro-suave-al-tacto-220v'),
(29, 3, 3, 'CUCHILLA  DESMONTABLE - 0000', '76914-816-000', '• Deja el cabello 2.5 mm (1/100\").<br>\r\n• Compatible con modelo 616.', '1617029620510_product-32.png', 'cuchilla-desmontable-0000'),
(30, 3, 3, 'CUCHILLA DESMONTABLE DE ACABADO ESS', '76913-986-000', '• Deja el cabello 0.2 mm (1/125\").<br>\r\n• Compatible con los modelos T-Finisher y Taler.', '1617029685113_product-33.png', 'cuchilla-desmontable-de-acabado-ess'),
(31, 3, 3, 'CUCHILLA  DESMONTABLE - 000', '76914-826-000', '• Deja el cabello 0.5 mm (1/50\").<br>\r\n• Compatible con modelo 616.', '1617029797959_product-34.png', 'cuchilla-desmontable-000'),
(32, 3, 3, 'CUCHILLA ANGOSTA  DESMONTABLE O2  DE ACABADO', '76913-566-001', '• Deja el cabello 0.2 mm (1/125\").<br>\r\n• Compatible con los modelos T-Finisher y Taler.', '1617029833456_product-35.png', 'cuchilla-angosta-desmontable-o2-de-acabado'),
(33, 3, 3, 'CUCHILLA  DESMONTABLE - 1', '76914-886-000', '• Deja el cabello 2.4 mm (3/32\").<br>\r\n• Compatible con modelo 616.', '1617029865157_product-36.png', 'cuchilla-desmontable-1'),
(34, 3, 3, 'CUCHILLA  DESMONTABLE PARA  AFEITAR Y ACABADO', '76913-006-001', '• Deja el cabello 0.2 mm (1/125\").<br>\r\n• Compatible con los modelos T-Finisher y Taler.', '1617029891693_product-37.png', 'cuchilla-desmontable-para-afeitar-y-acabado'),
(35, 4, 14, 'RECORTADORA T FINISHER VELOCIDAD ÚNICA 120V', '76059-010-001', '• Motor Pivote.<br>\r\n• Diseño de cuchilla en forma T para un corte detallado.<br>\r\n• Carcasa de alta resistencia al uso.<br>\r\n• Incluye aceite lubricante, cepillo de limpieza y cable de 8 pies de largo.', '1617992656067_producto--16.png', 'recortadora-t-finisher-velocidad-Única-120v'),
(36, 4, 14, 'RECORTADORA T BLADE TALER® VELOCIDAD ÚNICA 120V', '76059-310-001', '• Motor Pivote.<br>\r\n• Cuchilla en forma de T, está cubierta con una capa protectora especialmente formulada resistente al desgaste.<br>\r\n• Carcasa en forma de barril diseñada para máximo control y comodidad.<br>\r\n• Carcasa de alta resistencia al uso.<br>\r\n• Incluye aceite lubricante, cepillo de limpieza y cable de 8 pies de largo.', '1617992994820_producto--17.png', 'recortadora-t-blade-taler-velocidad-Única-120v'),
(37, 4, 14, 'FINISHER  VELOCIDAD  ÚNICA 220V', '78059-840-051', '• Corte extremadamente fino y preciso.<br>\r\n• Motor de pivote.<br>\r\n• T-Blade para corte cercano.<br>\r\n• Diseño ligero y cómodo.<br>\r\n• Cuchilla en forma T.', '1617993042450_producto--18.png', 'finisher-velocidad-Única-220v'),
(38, 4, 14, 'CORTADORA / RECORTADORA O\'BABY™ VELOCIDAD ÚNICA 120V', '76988-310-001', 'O\'Baby™ es una poderosa recortadora empacada en un diseño compacto \r\ndel tamaño de una palma. Recortadora compacta de alto rendimiento, \r\nesta lista para manejar todas sus tareas de recorte y acabado.<br><br>\r\n\r\n• El motor rotativo compacto es pequeño pero potente.<br>\r\n• Cuchilla en forma de T esta cubierta por una capa de titanio para larga duración.<br>\r\n• Recortadora es de el tamaño de la palma de una mano y pesa menos de 4 onzas.<br>\r\n• Incluye 5 peines, protector de cuchilla, aceite lubricante y cepillo de limpieza.<br>\r\n• Cable de alimentación de 8 pies.', '1617993084265_producto--19.png', 'cortadora-recortadora-obaby-velocidad-Única-120v'),
(39, 5, 15, 'CUCHILLA DESMONTABLE ESS DE RECORTES', '76913-986-000', '• Compatible con cortadota T Finisher, Taler o 59-84.', '1618006289915_product--17.png', 'cuchilla-desmontable-ess-de-recortes'),
(40, 5, 15, 'CUCHILLA O2 DE  AFEITADO Y ACABADO', '76913-006-001', '• Compatible con cortadota T Finisher, Taler o 59-84.', '1618006325841_product--18.png', 'cuchilla-o2-de-afeitado-y-acabado'),
(41, 5, 15, 'CUCHILLA O2  ANGOSTA DE ACABADO', '76913-566-001', '• Compatible con cortadota T Finisher, Taler o 59-84.', '1618006352862_product--19.png', 'cuchilla-o2-angosta-de-acabado'),
(42, 5, 15, 'CUCHILLA DESMONTABLE  ESS - T-BLADE', '76913-716-001', '• Compatible con recortadoras O\'Baby™ y Artisan. ', '1618006836306_product--20.png', 'cuchilla-desmontable-ess-t-blade'),
(43, 5, 15, 'CUCHILLA O2  ANGOSTA DE ACABADO', '76913-586-001', '• Compatible con cortadota T Finisher, Taler o 59-84.', '1618006922952_product--21.png', 'cuchilla-o2-angosta-de-acabado'),
(44, 5, 15, 'CUCHILLA DESMONTABLE ESS PARA TEXTURIZADOS', '76913-726-000', '• Compatible con recortadoras O\'Baby™ y Artisan.', '1618006952199_product--22.png', 'cuchilla-desmontable-ess-para-texturizados'),
(45, 6, 16, 'SET DE PEINES UNIVERSAL DE 8 PIEZAS', '76926-800-000', '• Incluye los peines guía de los siguientes tamaños: #0 (1/16\"), #1 (1/8\"), #2 (1/4\"), #3 (3/8\"), #4 (1/2\"), #6 (3/4\"), #7 (7/8\"), #8 (1\").', '1618007773177_producto-24.png', 'set-de-peines-universal-de-8-piezas'),
(46, 6, 16, 'SET DE PEINES UNIVERSAL DE 10 PIEZAS', '76926-900-000', '• Incluye: bolso para almacenar,  #0 (1/16\"), #1 (1/8\"), #2 (1/4\"), #3 (3/8\"), #4 (1/2\"), #5 (5/8\"), #6 (3/4\"), #7 (7/8\"), #8 (1\"), #10 (5/4\").', '1618007798879_producto-25.png', 'set-de-peines-universal-de-10-piezas'),
(47, 7, 17, 'SPRAY KOOL LUBE®', '76300-101-005', 'Oster® Kool Lube® 3 es un lubricante especialmente\r\nformulado para enfriar instantáneamente las \r\ncuchillas de corte proporcionando mayor comodidad\r\npara su cliente. Proporcionar enfriamiento\r\ninstantáneo, lubricación y limpieza.<br><br>\r\n\r\n• No contiene gases que reducen la capa de ozono, sin CFC ni HCFC.<br>\r\n• No inflamable, no conductor a 29,000 voltios.<br>\r\n• Seca rápido.', '1618008094042_product--26.png', 'spray-kool-lube'),
(48, 7, 17, 'LUBRICANTE DE CUCHILLAS', '76300-104-005', 'Aceite lubricante de primera calidad Oster® \r\nBlade Lube™ para cortadoras y cuchillas.', '1618008145031_product--27.png', 'lubricante-de-cuchillas'),
(49, 7, 17, 'SPRAY DESINFECTANTE', '76300-102-005', 'El desinfectante en aerosol Oster® desodoriza \r\nlas superficies y mata a la mayoría de las bacterias\r\nque producen las infecciones Staph y Strep, \r\ny organismos como Pseudomanas aeruginosa y\r\nTrichophyton mentagrophytes en superficies duras.<br><br>\r\n\r\n• Para uso en hospitales, salones de belleza y hogares.<br>\r\n• Mata la mayoría de los gérmenes y bacterias.<br>\r\n• Elimina los olores.<br>\r\n• Previene moho y hongos.<br>\r\n• Aroma a limpio.', '1618008191028_product--28.png', 'spray-desinfectante'),
(50, 7, 17, 'GRASA LUBRICANTE GEAR LUBE', '76300-105-005', 'Mantenga las cortadoras funcionando sin \r\nproblemas con el lubricante Gear Lube Oster®.<br><br>\r\n\r\n• Recomendado para usar cada seis meses para un funcionamiento suave.<br>\r\n• Funciona con los modelos Classic 76, Model 10 y A5.', '1618008231166_product--29.png', 'grasa-lubricante-gear-lube'),
(51, 7, 17, 'LIMPIADOR DE CUCHILLAS', '76300-103-005', 'Para el cuidado y mantenimiento de las cuchillas \r\nde cortadoras, Blade Wash limpia y lubrica \r\npara un funcionamiento suave.<br><br>\r\n\r\n• Elimina los preservativos aplicados de fábrica a las cuchillas nuevas.<br>\r\n• Proporciona lubricación para cuchillas de cortadoras.<br>\r\n• Disponible en un tamaño de 16 oz.', '1618008302128_product--30.png', 'limpiador-de-cuchillas'),
(52, 7, 17, 'SPRAY DESINFECTANTE 5 EN 1®', '76300-107-005', 'Oster® 5 in 1 spray® actúa como virucida, bactericida\r\ny fungicida al mismo tiempo que enfría, lubrica,\r\ndesinfecta y brinda protección contra la oxidación.<br><br>\r\n\r\n• Mientras la cortadora está funcionando, sostenga el spray a aproximadamente 6 pulgadas de distancia de la cuchilla y rocíe la parte inferior y la parte superior de la cuchilla antes de cada corte, mientras que la cabeza de la cortadora apunta hacia abajo.<br>\r\n• El tiempo ideal de aplicación es cuando la cuchilla de la cortadora se siente caliente.<br>\r\n• Para evitar la acumulación de calor, rocíe la cuchilla de la cortadora con 5 en 1 Spray® cuando cambie los accesorios del peine de un tamaño al siguiente.<br>\r\n• Se puede utilizar con cualquier cortadora o recortadora.<br>\r\n• Cuando utilice una cortadora de cuchilla desmontables, rocíe cada aspa utilizada durante el corte.<br>\r\n• Desinfecta, y enfría instantáneamente la cuchilla.<br>\r\n• Proporciona una ligera lubricación que permite que la cuchilla se enfríe y se mantenga afilada por más tiempo.', '1618008343934_product--31.png', 'spray-desinfectante-5-en-1'),
(53, 8, 0, 'MASAJEADOR PROFESIONAL STIM-U-LAX®', '76103-100-000', 'Haga que la visita de su cliente \r\nsea aún más agradable con un suave \r\ny relajante masaje. El masajeador \r\nde alto rendimiento STIM-U-LAX® \r\nutiliza una acción exclusiva\r\nde motor suspendido para combinar \r\nlas ventajas del masaje manual \r\ny mecánico para ofrecer resultados\r\nexcepcionales.<br><br>\r\n\r\n• Proporciona un masaje vigorizante combinando técnica manual y mecánica.<br>\r\n• Motor universal diseñado para alto rendimiento en tiendas concurridas.<br>\r\n• Diseño ergonómico se contorna a la mano para mayor comodidad y facilidad de uso, reduciendo la fatiga de las manos.<br>\r\n• La construcción de cojinetes de bolas 100% produce un rendimiento más eficiente y un mejor rendimiento.<br>\r\n• Funciona con 120 voltios, 60 Hz, CA.', '1618008489582_producto_3333.png', 'masajeador-profesional-stim-u-lax'),
(54, 9, 4, 'TURBO A5', '78005-314-003', '• Cortadora de alto rendimiento de 2 velocidades, va detrás de los trabajos más difíciles con su  motor potente, silencioso y confiable.<br>\r\n• Potente motor rotativo universal.<br>\r\n• Turbo potenciado para rotaciones más altas por minuto y 2 velocidades 3000 SPM bajo 4000 SPM alto.<br>\r\n• Incluye cuchilla 10 CryogenX desmontable.<br>\r\n• Compatible con todas las cuchillas desmontables Oster A5.<br>\r\n• Fabricado en USA de partes extranjeras y nacionales.  ', '1618841090039_producto-animal-25.png', 'turbo-a5'),
(55, 9, 4, 'GOLDEN A5 DOS VELOCIDADES', '<strong>120V</strong><br> 78005-140-302<br>  <strong>127V - BRA</strong><br> 78005-140-017<br>  <strong>220V - BRA</strong><br> 78005-140-057<br>  <strong>220V - EUR</strong><br> 78005-140-351<br>  <strong>220V - ARG</strong><br> 78005-140-354', '• Potente motor rotativo universal de dos velocidades.<br>\r\n• El motor de alta resistencia proporciona suavidad y rendimiento conﬁable.<br>\r\n• Desde 2,100 SPM hasta 2,700 SPM.', '1618841325066_producto-animal-26.png', 'golden-a5-dos-velocidades'),
(56, 9, 4, 'A6 PÚRPURA  DE 3 VELOCIDADES', '<strong>120V</strong><br> 78006-123-302<br>  <strong>127V - 220V - BRA</strong><br> 78006-123-087<br>  <strong>220V - EUR</strong><br> 78006-123-151<br>  <strong>220V - ARG</strong><br> 78006-123-054', '• La tecnología ZINC ALLOY combina una ligera sensación ergonómica con alta resistencia y larga durabilidad.<br>\r\n• El agarre más delgado en nuestra clase de alta resistencia para precisión y control sobre las trazas del recorte.<br>\r\n• Tres Velocidades: Alta: Hasta 4,100 SPM. Mediana: 3,600 SPM Baja: 3,100 SPM.', '1618841555167_producto-animal-28.png', 'a6-pÚrpura-de-3-velocidades'),
(57, 9, 4, 'CORTADORA DELGADA A6 TURQUESA - 3 VELOCIDADES', '<strong>127 V- 220V - BRA</strong><br> 2112463<br>  <strong>220V - CHILE</strong><br> 2112461<br>  <strong>220V - ARG</strong><br> 2112462', '• La tecnología ZINC ALLOY combina una ligera sensación ergonómica con alta resistencia y larga durabilidad.<br>\r\n• El motor de alta eficiencia proporciona una experiencia de funcionamiento más eficiente.<br>\r\n• La placa frontal de la cortadora ayuda a mantener el acceso a los botones de mando; ayuda a minimizar la fatiga de la mano.<br>\r\n• Tres Velocidades: Alta: Hasta 4,100 SPM. Mediana: 3,600 SPM Baja: 3,100 SPM.<br>\r\n• Cable de energía de 12\' con extensión para ahorrar espacio.<br>\r\n• Compatible con todas las cuchillas desmontables A5®.', '1618841643075_producto-animal-29.png', 'cortadora-delgada-a6-turquesa-3-velocidades'),
(58, 9, 4, 'CORTADORA DELGADA A6 HUELLAS - 3 VELOCIDADES', '<strong>127 V- 220V - BRA</strong><br> 2112468<br>  <strong>220V - CHILE</strong><br> 2112466<br>  <strong>220V - ARG</strong><br> 2112467', ' ¡Expresa tu estilo y actitud personal mientras preparas a tu cliente favorito!<br>\r\n• El agarre más delgado en nuestra clase de alta resistencia para precisión y control sobre las trazas del recorte.\r\n• Tres Velocidades: Alta: Hasta 4,100 SPM. Mediana: 3,600 SPM Baja: 3,100 SPM.<br>\r\n• Los aisladores de vibración pendientes de patente están centrados alrededor del motor para ayudar a absorber la vibración y el ruido, dando como resultado una sensación natural para el aseo durante todo el día.<br>\r\n• La placa frontal de la cortadora ayuda a mantener el acceso a los botones de mando; ayuda a minimizar la fatiga de la mano.<br>\r\n• Cable de energía de 12\' con extensión para ahorrar espacio.<br>\r\n• Compatible con todas las cuchillas desmontables A5®.', '1618841734192_producto-animal-30.png', 'cortadora-delgada-a6-huellas-3-velocidades'),
(59, 9, 4, 'CORTADORA DELGADA  A6 DE 3 VELOCIDADES ', '<strong>120V</strong><br> 78006-150-302<br>  <strong>127 V- 220V - BRA</strong><br> 78006-150-087<br>  <strong>220V - EUR</strong><br> 78006-150-151<br>  <strong>220V - ARG</strong><br> 78006-150-054', '• La tecnología ZINC ALLOY combina una ligera sensación ergonómica con alta resistencia y larga durabilidad.<br>\r\n• El agarre más delgado en nuestra clase de alta resistencia para precisión y control sobre las trazas del recorte.<br>\r\n• Tres Velocidades: Alta: hasta 4,100 SPM. Mediana: 3,600 SPM. Baja: 3,100 SPM.', '1618841803890_producto-animal-31.png', 'cortadora-delgada-a6-de-3-velocidades'),
(60, 9, 4, 'PRO 3000i  VELOCIDAD ÚNICA', '<strong>100V-240V</strong><br> 78003-100-000<br> **INCLUYE CUCHILLA #10<br>  <strong>220V - EUR</strong><br> 78003-000-151<br> **CUCHILLA NO INCLUIDA', '• Potente motor rotativo de velocidad única de servicio pesado a 3000 SPM para el aseo versátil de perros, gatos, caballos y ganado.<br>\r\n• Tecnología de batería de Ion Litio proporciona uso continuo y mantiene una carga por más tiempo.<br>\r\n• Motor de una sola velocidad para trabajo pesado.<br>\r\n• Funciona hasta 2 horas con una sola carga.<br>\r\n• Li-Ion proporciona un poder más consistente a lo largo de la carga.<br>\r\n• Li-Ion pierde solo el 3% de su carga al mes cuando no está en uso, en comparación con el 30% de NiMH.<br>\r\n• Compatible con todas las cuchillas desmontables A5®.<br>\r\n• Diseño ligero y cómodo.<br>\r\n• Sistema de batería desmontable para extender los tiempos de funcionamiento, capacidad de carga rápida.<br>\r\n• La base puede cargar la segunda batería por separado o mientras está instalada en la recortadora.', '1618842200924_producto-animal-32.png', 'pro-3000i-velocidad-Única'),
(61, 9, 4, 'BATERÍA LI-ION  DE REPUESTO PARA MODELO  PRO 3000i', '78003-000-300', '• Funciona hasta 2 horas con una sola carga.<br>\r\n• Para usar como batería de respaldo.<br>\r\n• Base para cargar incluida.<br>\r\n• La batería Li-Ion pierde solo el 3% de su carga al mes cuando no está en uso, en comparación con el 30% para NiMH y del 15% al ​​30% para Nicad.<br>\r\n• La batería Li-Ion proporciona una potencia constante en comparación con las baterías Nicad durante el uso.', '1618842314803_producto-animal-33.png', 'baterÍa-li-ion-de-repuesto-para-modelo-pro-3000i'),
(62, 9, 5, 'CUCHILLAS - 50', '78919-006-005', '• Deja el pelo 0.2mm (1/25 ”).<br>\r\n• Ideal para cortes extremadamente cortos.<br>\r\n• Cuchilla de preparación quirúrgica veterinaria.', '1618842422885_producto-animales-34.png', 'cuchillas-50'),
(63, 9, 5, 'CUCHILLAS - 15', '78919-036-005', '• Deja el pelo 1.2mm (3/64\").<br>\r\n• Usar para usar en la cara, patas y cola de los poodles.<br>\r\n• Ideal para las razas Setters, Spaniels y Terriers.', '1618842483910_producto-animales-35.png', 'cuchillas-15'),
(64, 9, 5, 'CUCHILLAS - 40', '78919-016-005', '• Deja el pelo 0.25mm (1/100\").<br>\r\n• Proceso patentado Cryogen-X lleva la cuchilla a -300º F para una mayor dureza.<br>\r\n• Acabado de corte suave.', '1618842525556_producto-animales-36.png', 'cuchillas-40'),
(65, 9, 5, 'CUCHILLAS - 10', '78919-046-005', '• Deja el pelo 1.6mm (1/16\").<br>\r\n• Usar para todo trabajo sanitario.<br>\r\n• Ideal para usar en la cara, patas y cola de los poodles.', '1618842573841_producto-animales-37.png', 'cuchillas-10'),
(66, 9, 5, 'CUCHILLAS - 30', '78919-026-005', '• Deja el cabello 1/50\".<br>\r\n• Esta cuchilla se usa en poodles con piel poco sensible.', '1618842624132_producto-animales-38.png', 'cuchillas-30'),
(67, 9, 5, 'CUCHILLAS - 7 SKIP TOOTH', '78919-056-005', '• Deja el pelo 3.2mm (1/8\").<br>\r\n• Las aspas Skip Tooth penetran el pelo rizado mejor.<br>\r\n• Esta cuchilla es recomendada para el afeitado de perros extremadamente enredados.', '1618842755067_producto-animales-39.png', 'cuchillas-7-skip-tooth'),
(68, 9, 5, 'CUCHILLAS - 5 SKIP TOOTH', '78919-066-005', '• Deja el pelo 6.3mm (1/4\").<br>\r\n• Ideal para remover pelaje enredado o pesado.<br>\r\n• Usar para terminados de aspecto natural en Terriers.', '1618843775695_product-animales--40.png', 'cuchillas-5-skip-tooth'),
(69, 9, 5, 'CUCHILLAS - 7F FULL TOOTH', '78919-166-005', '• Deja el pelo 3.2mm (1/8\").<br>\r\n• Fabricado utilizando proceso de 12 pasos.<br>\r\n• Proceso patentado Cryogen-X lleva la cuchilla a -300º F para una mayor dureza.<br>\r\n• Acabado de corte suave.', '1618843836297_product-animales--41.png', 'cuchillas-7f-full-tooth'),
(70, 9, 5, 'CUCHILLAS - 5/8', '78919-106-005', '• Deja el pelo 0.8mm (1/32\").<br>\r\n• Ideal para cortes cercanos.<br>\r\n• Usar alrededor de las orejas y almohadillas.', '1618843874303_product-animales--42.png', 'cuchillas-58'),
(71, 9, 5, 'CUCHILLAS - 5F FULL TOOTH', '78919-176-005', '• Deja el pelo 6.3mm (1/4\").<br>\r\n• Actúa como un peine cuando se corta a través de la capa.<br>\r\n• Para un acabado suave de aspecto natural.<br>\r\n• Usar cuando se cortan patrones de espalda en Terriers y Spaniels.', '1618843924791_product-animales--43.png', 'cuchillas-5f-full-tooth'),
(72, 9, 5, 'CUCHILLAS - 4 SKIP TOOTH', '78919-136-005', '• Deja el pelo 9.5mm (3/8\").<br>\r\n• Utilizado para penetrar capas gruesas y risadas.', '1618843972101_product-animales--44.png', 'cuchillas-4-skip-tooth'),
(73, 9, 5, 'CUCHILLAS - 4F', '78919-186-005', '• Deja el pelo 9.5mm (3/8\").<br>\r\n• Actúa como un peine cuando se corta a través de la capa.<br>\r\n• Para un acabado suave de aspecto natural.<br>\r\n• Usar cuando se cortan patrones de espalda en Terriers y Spaniels.', '1618844014610_product-animales--45.png', 'cuchillas-4f'),
(74, 9, 5, 'CUCHILLAS - 8 1/2', '78919-146-005', '• Deja el pelo 2.8 mm (7/64\").<br>\r\n• Cuchilla utilizada para capas de cabello duro o enredado.<br>\r\n• Utilizada para cortar abrigos pesados o muy enredados.', '1618844077303_product-animales--46.png', 'cuchillas-8-12'),
(75, 9, 5, 'ARCTIC IGLOO', '76004-011-000', '• Espacios más amplios almacenan todas las cuchillas. desmontables de Oster, así como la mayoría de las cuchillas de tamaño completo.<br>\r\n• Almacena hasta 11 cuchillas.<br>\r\n• Hecho de plástico polipropileno resistente a los impactos para mayor durabilidad.', '1618850511447_producto--25.png', 'arctic-igloo'),
(76, 9, 5, 'CUCHILLAS 3F FULL TOOTH', '78919-206-005', '• Deja el pelo 13mm(1/2\").<br>\r\n• Actúa como un peine cuando se corta a través de la capa.<br>\r\n• Usar con las razas Shih Tzu y Maltese.<br>\r\n• Deja un acabado suave y fácil de mantener entre corte y corte.', '1618850568844_producto--26.png', 'cuchillas-3f-full-tooth'),
(77, 9, 5, 'CUCHILLAS 10 ANCHO', '78919-446-005', '• Deja el pelo 2.4mm (3/32\").<br>\r\n• Borde de aspa siginificativamente más ancho que el de cuchilla 10 regular.<br>\r\n• Para perros grandes con pelaje enredado.<br>\r\n• Permite hacer el trabajo más rápido.', '1618850630011_producto--27.png', 'cuchillas-10-ancho'),
(78, 9, 5, 'CUCHILLAS 3 SKIP TOOTH', '78919-226-005', '• Deja el pelo 13mm (1/2\").<br>\r\n• Utilizado para penetrar capas gruesas y mullidas.', '1618850707403_producto--28.png', 'cuchillas-3-skip-tooth'),
(79, 9, 7, 'RECORTADORA  PARA TERMINADO', '78059-100-000', '• Potente motor de giro silencioso Whisper-Quiet.<br>\r\n• Ideal para recortar alrededor de las orejas, cara, y patas.', '1618851254409_productos-animales-29.png', 'recortadora-para-terminado'),
(80, 9, 7, '59-84 TRIMMER  220V - EUR', '78059-840-051', '• Corte y acabado preciso alrededor de orejas, ojos, piernas y orificios nasales.<br>\r\n• Cuchilla tipo T incluida.', '1618851074825_productos-animales-30.png', '59-84-trimmer-220v-eur'),
(81, 9, 7, 'CUCHILLAS T-BLADE', '76913-006-001', '• Deja el pelo 0.2mm (1/125”).<br>\r\n• Utilizado para afeitado muy cerca, y zonas de difícil acceso.', '1618851272188_productos-animales-31.png', 'cuchillas-t-blade'),
(82, 9, 7, 'CUCHILLAS ANGOSTA', '76913-566-001', '• Deja el pelo 0.2mm (1/125”).<br>\r\n• Utilizado para afeitado muy cerca, zonas de difícil acceso.', '1618851328590_productos-animales-32.png', 'cuchillas-angosta'),
(83, 9, 7, 'CUCHILLAS T-FINISHER', '76913-586-001', '• Deja el pelo 0.2mm (1/125”).<br>\r\n• Utilizado para trabajos de recorte detallados.', '1618851372086_productos-animales-33.png', 'cuchillas-t-finisher'),
(84, 9, 8, 'LIMA DE UÑAS SPIN™', '78129-900-000', 'Incluye:<br>\r\n• Soporte de carga.<br>\r\n• Fuente de energía.<br>\r\n• Bandas de esmerilado de: 60, y 100 granos.<br>\r\n• Piedra para limar de 80 granos.<br>\r\n• Manual de instrucciones.', '1618852694160_producto_unias.png', 'lima-de-uÑas-spin'),
(85, 9, 9, 'JUEGO DE PEINES EN ACERO INOXIDABLE', '78936-100-000', '• Estuche transparente. 10 diferentes longitudes de corte convenientemente codificados por colores.<br>\r\n• Incluye 2 tamaños más grandes 11/2 (38 mm) y 2” (50 mm).<br>\r\n• Especialmente diseñado para adaptarse a las cuchillas Oster tipo A5, esta coleccion de peines ofrece una gama completa para el cuidado de todo tipo de perros y gatos.', '1618854024378_producto-animales-37.png', 'juego-de-peines-en-acero-inoxidable'),
(86, 9, 9, 'JUEGO DE PEINES  DE 8 PIEZAS', '76926-800-000', 'Para uso solo con \r\nCortadoras Pivote.', '1618854081043_producto-animales-38.png', 'juego-de-peines-de-8-piezas'),
(87, 9, 9, 'JUEGO DE PEINES  DE 10 PIEZAS', '76926-900-000', '• Permite cortar el pelo de las mascotas a una longitud uniforme o en capas.<br>\r\n• Diseñado para usar con el tipo Oster A5 u otras cortadoras.<br>\r\n• Incluye: práctica bolsa de almacenamiento, 2 mm, 4 mm, 8 mm, 9.5 mm, 14 mm, 16 mm, 18 mm, 22 mm, 25 mm y 32 mm.', '1618854108268_producto-animales-39.png', 'juego-de-peines-de-10-piezas'),
(88, 9, 10, 'CHAMPÚ MULTI USO  BERRY FRESH 1 GL', '78299-705-200', '<strong>CHAMPÚ MULTI USO  \r\nBERRY FRESH\r\n16 OZ</strong><br>\r\n78299-700-200<br>\r\n\r\n• Vitaminas y humectantes naturales restauran y nutren el pelaje y la piel.<br>\r\n• Aroma fresco y limpio a bayas.<br>\r\n• Seguro para el uso diario.<br>\r\n• Proporción de dilución 10 a 1.<br>', '1618854369096_product-animal-41.png', 'champÚ-multi-uso-berry-fresh-1-gl'),
(89, 9, 10, 'CHAMPÚ VAINILLA  BLANCO 1 GL', '78299-725-200', '<strong>>CHAMPÚ VAINILLA \r\nBLANCO \r\n16 OZ</strong><br>\r\n78299-720-200', '1618854611605_product-animal-42.png', 'champÚ-vainilla-blanco-1-gl'),
(90, 9, 10, 'CHAMPÚ ALOE  SIN LÁGRIMAS 1 GL', '78299-715-200', '<strong>CHAMPÚ ALOE \r\nSIN LÁGRIMAS\r\n16 OZ</strong><br>\r\n78299-710-200<br><br>\r\n\r\n• Fórmula suave, sin lágrimas.<br>\r\n• Sin perfume.<br>\r\n• Con aloe vera natural y vitamina B5.<br>\r\n• Proporción de dilución 12 a 1.', '1618854666724_product-animal-43.png', 'champÚ-aloe-sin-lagrimas-1-gl'),
(91, 9, 10, 'CHAMPÚ CREMA DE NARANJA  EXTRA LIMPIO  1 GL', '78299-735-200', '<strong>CHAMPÚ CREMA\r\nDE NARANJA \r\nEXTRA LIMPIO\r\n16 OZ</strong><br>\r\n78299-730-200<br><br>\r\n\r\n• Elimina el exceso de suciedad.<br>\r\n• Enriquecido con aceites esenciales y vitaminas.<br>\r\n• Fragrancia de sorbete de naranja.<br>\r\n• Proporción de dilución 12 a 1.', '1618854712337_product-animal-44.png', 'champÚ-crema-de-naranja-extra-limpio-1-gl'),
(92, 9, 10, 'CHAMPÚ  PERLA NEGRA 1 GL', '78299-745-200', '<strong>CHAMPÚ \r\nPERLA NEGRA\r\n16 OZ</strong><br>\r\n78299-740-200<br><br>\r\n\r\n• Realza el color natural y el brillo de el pelaje oscuro.<br>\r\n• Contiene ingredientes que nutren la piel.<br>\r\n• Realza el color sin colorantes ni otros ingredientes dañinos.<br>\r\n• Proporción de dilución 10 a 1.', '1618854805710_product-animal-46.png', 'champÚ-perla-negra-1-gl'),
(93, 9, 10, 'ACONDICIONADOR DERMASILK 1 GL', '78299-855-200', '<strong>ACONDICIONADOR\r\nDERMASILK\r\n16 OZ</strong><br>\r\n78299-850-200<br><br>\r\n\r\n• Suaviza y da brillo al pelaje sin dejar residuos.<br>\r\n• Desenreda la melena, la cola y el pelaje.<br>\r\n• Fragrancia de ramo de fresas.<br>\r\n• Proporción de dilución 10 a 1.', '1618854860447_product-animal-47.png', 'acondicionador-dermasilk-1-gl'),
(94, 10, 11, 'SHEARMASTER  VELOCIDAD ÚNICA - 120V', '78153-600-000', '<strong>220V - EUR</strong><br>\r\n78530-600-050<br>\r\n\r\n<strong>220V - ARG</strong><br>\r\n78530-600-054<br><br>\r\n\r\n• Perilla giratoria de tensión para ajustes de cuchillas sin problemas.<br>\r\n• Una velocidad.', '1618855273329_product-48.png', 'shearmaster-velocidad-Única-120v'),
(95, 10, 11, 'SHOWMASTER  VELOCIDAD VARIABLE', '78153-700-001', '• Incluye peine y cuchilla de alta resistencia para cortar, exculpir y ajustar.<br>\r\n• Intercambiable a cabezal Clipmaster.', '1618855354605_product-49.png', 'showmaster-velocidad-variable'),
(96, 10, 11, 'CABEZAL  SHEARMASTER', '78153-313-000', '', '1618855393617_product-50.png', 'cabezal-shearmaster'),
(97, 10, 11, 'PEINE ARIZONA DELGADO 13-DIENTES', '78554-016-003', '• Compatible con todas las piezas de mano.<br>\r\n• Cabezal para esquilar de 3-1/2\".<br>\r\n• Ideal para cortes de forma suave y cercana a la piel.', '1618863273359_productos-animales-52.png', 'peine-arizona-delgado-13-dientes'),
(98, 10, 11, 'PEINE SHOW  GROOMER  3\" DE ANCHO', '78554-226-003', '', '1618863243531_productos-animales-53.png', 'peine-show-groomer-3-de-ancho'),
(99, 10, 11, 'PEINE 20-DIENTES 3\" DE ANCHO', '78554-056-003', '• Para el corte regular de chivos.<br>\r\n• Para uso con cabezales de corte de 3\" y piezas de mano profesionales.<br>\r\n• Compatible con todas las piezas de mano y cabezales de 3\".', '1618863283877_productos-animales-54.png', 'peine-20-dientes-3-de-ancho'),
(100, 10, 11, 'PEINE 24-DIENTES  PARA BLOQUEO', '78554-236-003', '', '1618865778370_productos-animales-55.png', 'peine-24-dientes-para-bloqueo'),
(101, 10, 11, 'PEINE 20-DIENTES PARA  BLOQUEO Y TALLADO', '78554-096-003', '• Para recortar, bloquear y tallar.<br>\r\n• Para uso con cabezales de corte de 3\" y piezas de mano profesionales.<br>\r\n• Compatible con todas las piezas de mano y cabezales de 3\".', '1618865801279_productos-animales-56.png', 'peine-20-dientes-para-bloqueo-y-tallado'),
(102, 10, 11, 'PEINE DE SHOW  TEXAS CATTLE', '78554-296-003', '', '1618865846088_productos-animales-57.png', 'peine-de-show-texas-cattle'),
(103, 10, 11, 'PEINE GOLDEN RAM TIPO CAMPANA 13-DIENTES', '78554-186-003', '', '1618865876891_productos-animales-58.png', 'peine-golden-ram-tipo-campana-13-dientes'),
(104, 10, 11, 'CUCHILLAS  DE 3 PUNTAS  WYOMING SPECIAL', '78555-026-003', '', '1618865904423_productos-animales-59.png', 'cuchillas-de-3-puntas-wyoming-special'),
(105, 10, 11, 'CUCHILLAS  DE 4 PUNTAS  WIDE DIAMOND', '78555-036-003', '', '1618865930704_productos-animales-60.png', 'cuchillas-de-4-puntas-wide-diamond'),
(106, 10, 11, 'PEINE EXTRA ANCHO  DE 3\" HARVEST ALL', '78554-196-003', '', '1618865958287_productos-animales-61.png', 'peine-extra-ancho-de-3-harvest-all'),
(107, 10, 11, 'CUCHILLAS  HEEL DELGADA  DE 4 PUNTAS', '78555-046-003', '', '1618865985377_productos-animales-62.png', 'cuchillas-heel-delgada-de-4-puntas'),
(108, 10, 11, 'PEINE EAGLE 3\" DE ANCHO', '78554-216-003', '', '1618866008863_productos-animales-63.png', 'peine-eagle-3-de-ancho'),
(109, 10, 11, 'CUCHILLAS ANCHA  DE 4 PUNTAS AAA', '78555-056-003', '', '1618866040243_productos-animales-64.png', 'cuchillas-ancha-de-4-puntas-aaa'),
(110, 10, 12, 'CLIPMASTER  VELOCIDAD ÚNICA', '<strong>120V</strong><br> 78150-003-004<br>  <strong>127V - BRA</strong><br> 78150-600-017<br>  <strong>220V - BRA</strong><br> 78510-600-057<br>  <strong>220V - EUR</strong><br> 78510-600-050<br>  <strong>220V - ARG</strong><br> 78510-600-054', '• Diseño duradero inastillable y 30% más ligera que el diseño anterior.<br>\r\n• Agarre con balance para mejor maniobrabilidad.', '1618866109228_productos-animales--65.png', 'clipmaster-velocidad-Única'),
(111, 10, 12, 'CLIPMASTER  VELOCIDAD VARIABLE', '78150-700-000', '• Diseño duradero inastillable y 30% más ligera que el diseño anterior.<br>\r\n• Agarre con balance para mejor maniobrabilidad.<br>\r\n• Velocidad variable: se ajusta de 700 a 3000 SPM.', '1618866973695_productos-animales--66.png', 'clipmaster-velocidad-variable'),
(112, 10, 12, 'CABEZAL DE CLIPMASTER', '78150-513-000', '', '1618867042677_productos-animales--67.png', 'cabezal-de-clipmaster'),
(113, 10, 12, 'CUCHILLA  SUPERIOR PARA  CORTE ESTÁNDAR', '78511-016-001', '', '1618867068845_producto-animal-69.png', 'cuchilla-superior-para-corte-estandar'),
(114, 10, 12, 'CUCHILLA  DE PUNTEO CON  FONDO GRUESO', '78511-046-001', '', '1618867227979_producto-animal-70.png', 'cuchilla-de-punteo-con-fondo-grueso'),
(115, 10, 12, 'CUCHILLA  INFERIOR PARA  CORTE ESTÁNDAR', '78511-026-001', '', '1618867258939_producto-animal-71.png', 'cuchilla-inferior-para-corte-estandar'),
(116, 10, 12, 'SET DE CUCHILLAS  TOPE Y FONDO', '78511-126-001', '', '1618867285438_producto-animal-72.png', 'set-de-cuchillas-tope-y-fondo'),
(117, 10, 12, 'CUCHILLA INFERIOR PARA  USO QUIRÚRGICO', '78511-036-001', '', '1618867310734_producto-animal-73.png', 'cuchilla-inferior-para-uso-quirÚrgico'),
(118, 10, 12, 'CUCHILLA  CON DIENTES  COMPLETOS  FONDO GRUESO  PARA PUNTEO', '78511-140-001', '', '1618867336168_producto-animal-74.png', 'cuchilla-con-dientes-completos-fondo-grueso-para-punteo'),
(119, 10, 13, 'KOOL LUBE', '76300-101-005', '• Especialmente formulado para enfriar instantáneamente las aspas de corte proporcionando más comodidad a tu cliente.<br>\r\n• Libre de gases que deterioran la capa de ozono, CFC o HCFC.<br>\r\n• No inflamable, secado rápido.<br>\r\n• Disponible en tamaño de 14 Oz.  ', '1618867418505_productos-animales-75.png', 'kool-lube'),
(120, 10, 13, 'LUBRICANTE  DE CUCHILLAS (4 OZ)', '76300-104-005', '• Ayuda a mantener el aspa afilada y corriendo suavemente.<br>\r\n• Previene las altas temperaturas.', '1618867455136_productos-animales-76.png', 'lubricante-de-cuchillas-4-oz'),
(121, 10, 13, 'DESINFECTANTE  (16 OZ)', '76300-102-005', '• Para uso en hospitales, salones de belleza y el hogar.<br>\r\n• Mata la mayoría de los gérmenes y bacterias, previene hongos y la humedad.<br>\r\n• Elimina los olores.<br>\r\n• Aroma a limpio. ', '1618867491226_productos-animales-77.png', 'desinfectante-16-oz'),
(122, 10, 13, 'LUBRICANTE  ENGRANAJE (1.25 OZ)', '76300-105-005', '• Uso recomendado es de cada seis meses para un funcionamiento fluido.<br>\r\n• Compatible con los modelos Classic 76, Model 10, y A5. ', '1618867530382_productos-animales-78.png', 'lubricante-engranaje-125-oz'),
(123, 10, 13, 'LAVADOR  DE CUCHILLAS (18 OZ)', '76300-103-005', '• Elimina los preservativos de fábrica en aspas nuevas.<br>\r\n• Proporciona lubricación para las aspas de una cortadora.<br>\r\n• Disponible en tamaño de 16 Oz.', '1618867572917_productos-animales-79.png', 'lavador-de-cuchillas-18-oz'),
(124, 10, 13, 'AEROSOL 5 EN 1  (14 OZ)', '76300-107-005', '• Actúa como viricida, bactericida, y fungicida mientras enfría, lubrica, desinfecta y proporciona protección contra la oxidación.', '1618867613175_productos-animales-80.png', 'aerosol-5-en-1-14-oz');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `provincia`
--

CREATE TABLE `provincia` (
  `id` smallint(2) NOT NULL,
  `provincia` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `provincia`
--

INSERT INTO `provincia` (`id`, `provincia`) VALUES
(1, 'Buenos Aires'),
(2, 'Capital Federal'),
(3, 'Catamarca'),
(4, 'Chaco'),
(5, 'Chubut'),
(6, 'Córdoba'),
(7, 'Corrientes'),
(8, 'Entre Ríos'),
(9, 'Formosa'),
(10, 'Jujuy'),
(11, 'La Pampa'),
(12, 'La Rioja'),
(13, 'Mendoza'),
(14, 'Misiones'),
(15, 'Neuquén'),
(16, 'Río Negro'),
(17, 'Salta'),
(18, 'San Juan'),
(19, 'San Luis'),
(20, 'Santa Cruz'),
(21, 'Santa Fé'),
(22, 'Santiago del Estero'),
(23, 'Tierra del Fuego'),
(24, 'Tucumán');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `subcategorias`
--

CREATE TABLE `subcategorias` (
  `id` int(11) NOT NULL,
  `id_categoria` int(11) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `portada` varchar(255) DEFAULT NULL,
  `footer` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `subcategorias`
--

INSERT INTO `subcategorias` (`id`, `id_categoria`, `nombre`, `portada`, `footer`, `slug`) VALUES
(1, 1, 'Uso Pesado', '1616776233314_portada_cortadoras_.png', '', 'uso-pesado'),
(2, 2, 'Uso Pesado', '1617026098425_portada_02.png', '', 'uso-pesado'),
(3, 3, 'Uso Mediano', '1617027605626_portada_03.png', '', 'uso-mediano'),
(4, 9, 'CORTADORAS USO PESADO', '1618841275089_portada_cortadoras.png', '', 'cortadoras-uso-pesado'),
(5, 9, 'CUCHILLAS CORTADORAS', '1618844165129_portada_cuchillas.png', '1618844165141_footer_cuchillas.png', 'cuchillas-cortadoras'),
(7, 9, 'RECORTADORA Y CUCHILLA', '1618850895830_portada_02.png', '', 'recortadora-y-cuchilla'),
(8, 9, 'LIMAS DE UÑA PROFESIONALES', '1618852685824_portada_03.png', '', 'limas-de-una-profesionales'),
(9, 9, 'PEINES UNIVERSALES Y ACCESORIOS', '1618854013066_portada_04.png', '', 'peines-universales-y-accesorios'),
(10, 9, 'CHAMPÚS Y ACONDICIONADORES', '1618854315862_portada_05.png', '', 'champus-y-acondicionadores'),
(11, 10, 'CORTADORAS, PEINES Y CUCHILLAS', '1618855039358_portada_06.png', '', 'cortadoras-peines-y-cuchillas'),
(12, 10, 'CORTADORAS Y CUCHILLAS', '1618866320537_portada_07.png', '', 'cortadoras-y-cuchillas'),
(13, 10, 'LUBRICANTES Y DESINFECTANTES', '1618867648365_portada_07.png', '', 'lubricantes-y-desinfectantes'),
(14, 4, 'Uso Mediano', '1617992545425_portada_recortadoras.png', '', 'uso-mediano'),
(15, 5, 'Uso Ligero', '1618006135400_portada_cuchillas.png', '', 'uso-ligero'),
(16, 6, 'Herramientas para Cuidado', '1618007560844_portada_accesorios.png', '', 'herramientas-para-cuidado'),
(17, 7, 'Lubricantes y Desinfectantes', '1618007995436_portada_mantenimiento.png', '', 'lubricantes-y-desinfectantes');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_login`
--

CREATE TABLE `user_login` (
  `id` int(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `rol_id` int(11) DEFAULT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `administrator` int(1) NOT NULL,
  `provinciaId` int(20) NOT NULL,
  `vendedor_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `user_login`
--

INSERT INTO `user_login` (`id`, `name`, `lastname`, `rol_id`, `user_name`, `user_password`, `user_email`, `administrator`, `provinciaId`, `vendedor_id`) VALUES
(73, 'Irene ', 'Muñoz ', NULL, 'imuñoz', '93cf22c6407be55ed11323149c01889c', '', 1, 1, 0),
(70, 'Daniel', 'Gomez', 1, 'dgomez', '6f6222eb7a37af115905abf646cae4ce', 'dgomez@peman.com.ar', 0, 4, 1),
(69, 'Diego', 'Parodi', NULL, 'dparodi', '6f6222eb7a37af115905abf646cae4ce', 'dparodi@peman.com.ar', 0, 7, 6),
(68, 'Mariano', 'Bonsignor', NULL, 'mbonsignor', '6f6222eb7a37af115905abf646cae4ce', 'mbonsignor@peman.com.ar', 0, 6, 11),
(61, 'admin', 'admin', NULL, 'admin', '1a9450a90520fdc888c3c40a748c453d', 'NOMODIFICAR@oxford.com', 1, 11, 0),
(67, 'Rodrigo', 'Diaz Pucheta', NULL, 'rdiazpucheta', '6f6222eb7a37af115905abf646cae4ce', 'rdiazpucheta@peman.com.ar', 0, 6, 10),
(66, 'Diego', 'Toniotti', 1, 'dtoniotti', '6f6222eb7a37af115905abf646cae4ce', 'dtoniotti@peman.com.ar', 0, 6, 9),
(64, 'Esteban', 'Muratore', 1, 'emuratore', '6f6222eb7a37af115905abf646cae4ce', 'emuratore@peman.com.ar', 0, 17, 7),
(65, 'Octavio', 'Varela', NULL, 'ovarela', '6f6222eb7a37af115905abf646cae4ce', 'asesornoa@peman.com.ar', 0, 24, 8),
(74, 'Santiago ', 'Vélez Ferrer', 0, 'svferrer', 'cc051804c579dfe620802efba025d9ef', '', 1, 6, 1),
(75, 'Bárbara', 'Wulff', NULL, 'bwulff', '6f6222eb7a37af115905abf646cae4ce', 'barbarawulff@peman.com.ar', 0, 6, 15);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `catalogos`
--
ALTER TABLE `catalogos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `provincia`
--
ALTER TABLE `provincia`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `subcategorias`
--
ALTER TABLE `subcategorias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `user_login`
--
ALTER TABLE `user_login`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `catalogos`
--
ALTER TABLE `catalogos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=125;

--
-- AUTO_INCREMENT de la tabla `subcategorias`
--
ALTER TABLE `subcategorias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de la tabla `user_login`
--
ALTER TABLE `user_login`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
